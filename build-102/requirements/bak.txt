beautifulsoup4==4.6.0
certifi==2019.11.28
chardet==3.0.4
csscompressor==0.9.4
decorator==4.4.1
Django==1.8.17
django-model-utils==3.0.0
django-mptt==0.8.7
django-pipeline==1.6.13
django-redis==4.8.0
django-redis-sessions==0.4.0
django-softhyphen==1.1.0
django-solo==1.1.2
django-suit==0.2.25
django-suit-ckeditor==0.0.2
facebook-sdk==2.0.0
future==0.18.2
futures==3.1.1
html5lib==0.999999999
idna==2.5
ipython==6.1.0
ipython-genutils==0.2.0
jedi==0.16.0
jsmin==2.2.2
libsass==0.19.4
libsasscompiler==0.1.5
oauthlib==3.1.0
olefile==0.46
parso==0.6.2
pexpect==4.8.0
pickleshare==0.7.5
Pillow==4.1.1
prompt-toolkit==1.0.18
psycopg2==2.7.1
ptyprocess==0.6.0
Pygments==2.5.2
PyLinkedinAPI==0.1.5
pysyge==0.2.0
python-twitter==3.3
python3-memcached==1.51
pytils==0.3
pytz==2017.2
redis==2.10.5
requests==2.18.1
requests-oauthlib==1.3.0
simplegeneric==0.8.1
six==1.14.0
traitlets==4.3.3
urllib3==1.21.1
wcwidth==0.1.8
webencodings==0.5.1

#!/bin/bash

#
set -e

# set global variables
SCRIPT_NAME=`echo $0 | sed 's|.*/||'`
SCRIPT_VERSION="2.1.1"

# set default variables
APP_NAME=""
APP_BUILD=""
APP_RELEASE="staging"
APP_DATA="app.data"
APP_UWSGI="app.uwsgi"
APP_PATHS_SRC="src"
APP_PATHS_STATIC="static"
APP_PATHS_MEDIA="media"
EXTRACT="0"
NO_LINK_MEDIA="1"
NO_MIGRATION="0"
NO_REQUIREMENTS="0"
RELOAD_CELERY="0"
RELOAD_MEMCACHE="0"
RELOAD_RABBITMQ="0"
RELOAD_REDIS="0"
RELOAD_UWSGI="1"

# set cmd options
SCRIPT_OPTS=":r:b:n:-:xRMlcmqhv"
typeset -A ARRAY_OPTS
ARRAY_OPTS=(
    [release]=r
    [build]=b
    [name]=n
    [extract]=x
    [nomigrations]=M
    [norequirements]=R
    [nomedia]=l
    [version]=v
    [help]=h
)

script_version() {
    echo "${SCRIPT_NAME} version ${SCRIPT_VERSION}" >&2
}

script_usage() {
    echo -e "\nUsage there
    -r RELEASE\t Application release type production or staging (default: staging)
    -b BUILD\t Build number
    -n NAME\t Application name (example: arriva.ru)
    -x\t\t extract and delete tar archive (file name: build-BUILD.tar.gz)
    -R\t\t do not run install requirements (default: yes)
    -M\t\t do not run migrations (default: yes)
    -l\t\t do not create symlint to media (default: yes)
    -c\t\t reload celery (default: no)
    -m\t\t reload memcache (default: no)
    -s\t\t reload redis (default: no)
    -q\t\t reload rabbitmq (default: no)
    -v\t\t Show version
    -h\t\t This help" >&2
}

do_extract() {
    # create app new build dir
    mkdir -p ${APP_BUILD_DIR}

    # extract and delete tar archive
    #tar -xf ${APP_BUILD_FILE} -C ${APP_BUILD_DIR} &&

    # delete tar archive
    rm -f ${APP_BUILD_DIR}/${APP_BUILD_FILE}
}

do_update() {
    # extract tar archive
    if [[ ${EXTRACT} -eq 1 ]]; then
        do_extract
    fi

    # activate webapp virtualenv
    source env/bin/activate

    # set some envs
    export DJANGO_SETTINGS_MODULE=settings.${APP_RELEASE}

    # goto app directory
    cd ${APP_BUILD_DIR}/${APP_PATHS_SRC}

    # install or upgrade requirements
    if [ ${NO_REQUIREMENTS} -eq 0 ]; then
        pip install --upgrade -r ../requirements/${APP_RELEASE}.txt
    fi

    # set hostname
    sed -i "s/%HOSTNAME%/${HOSTNAME}/" ${APP_CONFIG_FILE}

    # set database password
    sed -i "s/%DBPASSWORD%/${APP_DBPASSWORD}/" ${APP_CONFIG_FILE}

    # set secret key
    sed -i "s/%SECRET_KEY%/${APP_SECRET_KEY}/" ${APP_CONFIG_FILE}

    # migrate application database
    if [ ${NO_MIGRATION} -eq 0 ]; then
        python manage.py migrate --noinput
    fi

    # exit from app directory and deactivate virtualenv
    cd ${APP_HOME} && deactivate nondestructive

    # update links to new build
    ln -snf ${APP_BUILD_DIR}/${APP_PATHS_SRC} ${APP_PATHS_SRC}
    ln -snf ${APP_BUILD_DIR}/${APP_PATHS_STATIC} ${APP_PATHS_STATIC}
    if [[ ${NO_LINK_MEDIA} -eq 1 ]]; then
        ln -snf ${APP_BUILD_DIR}/${APP_PATHS_MEDIA} ${APP_PATHS_MEDIA}
    fi

    # remove previous build
    APP_BUILD_OLD=$(echo ${APP_VERSIONS} | cut -d ':' -f 1)
    APP_BUILD_PREV=$(echo ${APP_VERSIONS} | cut -d ':' -f 2)
    APP_VERSIONS="${APP_BUILD_PREV}:${APP_BUILD}"
    if [ -d "build-${APP_BUILD_OLD}" ]; then
        rm -r "build-${APP_BUILD_OLD}"
    fi
}

do_reload() {
    # flag for uWSGI restart
    if [[ ${RELOAD_UWSGI} -eq 1 ]]; then
        echo "last reload at `date "+%Y/%m/%d %H:%M:%S"`" > "${APP_HOME}/${APP_UWSGI}"
    fi

    # reload celery
    if [[ ${RELOAD_CELERY} -eq 1 ]]; then
        # celeryd restart
        echo "Try to restart celeryd, timeout is 60s, please wait ..."
        timeout 30 sudo service celeryd restart  # timeout return code 124 if time exceeded

        # celerybeat restart
        echo "Try to restart celerybeat, timeout is 30s, please wait ..."
        timeout 30 sudo service celerybeat restart  # timeout return code 124 if time exceeded
    fi

    # reload memcache
    if [[ ${RELOAD_MEMCACHE} -eq 1 ]]; then
        echo "Try to restart memcached, timeout is 10s, please wait ..."
        timeout 10 sudo service memcached restart  # timeout return code 124 if time exceeded
    fi

    # reload redis-server
    if [[ ${RELOAD_REDIS} -eq 1 ]]; then
        echo "Try to restart redis-server, timeout is 10s, please wait ..."
        timeout 10 sudo service redis-server restart  # timeout return code 124 if time exceeded
    fi

    # reload memcache
    if [[ ${RELOAD_RABBITMQ} -eq 1 ]]; then
        echo "Try to restart rabbitmq-server, timeout is 10s, please wait ..."
        timeout 10 sudo service rabbitmq-server restart  # timeout return code 124 if time exceeded
    fi
}


# check options
if [[ "$#" -lt 2 ]]; then
    script_version >&2 && script_usage >&2 &&  exit 1
fi

# parse options
while getopts ${SCRIPT_OPTS} OPTION ; do
    # translate long options to short
    if [[ "x$OPTION" == "x-" ]]; then
        LONG_OPTION=$OPTARG
        LONG_OPTARG=$(echo $LONG_OPTION | grep "=" | cut -d'=' -f2)
        LONG_OPTIND=-1
        [[ "x$LONG_OPTARG" = "x" ]] && LONG_OPTIND=$OPTIND || LONG_OPTION=$(echo $OPTARG | cut -d'=' -f1)
        [[ $LONG_OPTIND -ne -1 ]] && eval LONG_OPTARG="\$$LONG_OPTIND"
        OPTION=${ARRAY_OPTS[$LONG_OPTION]}
        [[ "x$OPTION" = "x" ]] &&  OPTION="?" OPTARG="-$LONG_OPTION"

        if [[ $( echo "${SCRIPT_OPTS}" | grep -c "${OPTION}:" ) -eq 1 ]]; then
            if [[ "x${LONG_OPTARG}" = "x" ]] || [[ "${LONG_OPTARG}" = -* ]]; then 
                OPTION=":" OPTARG="-$LONG_OPTION"
            else
                OPTARG="$LONG_OPTARG";
                if [[ $LONG_OPTIND -ne -1 ]]; then
                    [[ $OPTIND -le $Optnum ]] && OPTIND=$(( $OPTIND+1 ))
                    shift $OPTIND
                    OPTIND=1
                fi
            fi
        fi
    fi

    # options follow by another option instead of argument
    if [[ "x${OPTION}" != "x:" ]] && [[ "x${OPTION}" != "x?" ]] && [[ "${OPTARG}" = -* ]]; then 
        OPTARG="$OPTION" OPTION=":"
    fi

    # manage options
    case "$OPTION" in
        r ) APP_RELEASE=${OPTARG};                                                                              ;;
        b ) APP_BUILD=${OPTARG}; APP_BUILD_DIR="build-${APP_BUILD}"; APP_BUILD_FILE="build-${APP_BUILD}.tar.gz" ;;
        n ) APP_NAME=${OPTARG}; APP_HOME="${HOME}/${APP_NAME}"                                                  ;;
        x ) EXTRACT=1                                                                                           ;;
        R ) NO_REQUIREMENTS=1                                                                                   ;;
        M ) NO_MIGRATION=1                                                                                      ;;
        l ) NO_LINK_MEDIA=0                                                                                     ;;
        c ) RELOAD_CELERY=1                                                                                     ;;
        m ) RELOAD_MEMCACHE=1                                                                                   ;;
        s ) RELOAD_REDIS=1                                                                                      ;;
        q ) RELOAD_RABBITMQ=1                                                                                   ;;
        v ) script_version && exit 0                                                                            ;;
        h ) script_version && script_usage && exit 0                                                            ;;
        : ) echo "${SCRIPT_NAME}: -$OPTARG: option requires an argument" >&2 && script_usage >&2 && exit 98     ;;
        ? ) echo "${SCRIPT_NAME}: -$OPTARG: unknown option" >&2 && script_usage >&2 && exit 99                  ;;
    esac
done
shift $((${OPTIND} - 1))


# main
APP_CONFIG_FILE="settings/${APP_RELEASE}.py"
APP_VERSIONS=$(cat ${APP_HOME}/${APP_DATA} | cut -d ':' -f 1,2)
APP_SECRET_KEY=$(cat ${APP_HOME}/${APP_DATA} | cut -d ':' -f 3)
APP_DBPASSWORD=$(cat ${APP_HOME}/${APP_DATA} | cut -d ':' -f 4)
cd ${APP_HOME}

# update app
do_update

# save new build data to file
echo "${APP_VERSIONS}:${APP_SECRET_KEY}:${APP_DBPASSWORD}" > ${APP_DATA}

# reload uwsgi, ?nginx, celeryd, redis, memcache, rabbitmq
do_reload

# normal exit
exit 0
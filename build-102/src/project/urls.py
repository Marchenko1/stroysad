from django.conf import settings
from django.contrib import admin
from django.conf.urls import patterns, include, url
from admin_honeypot.admin import honeypot_site
from main.views import IndexView
from .sitemaps import site_sitemaps


urlpatterns = patterns('',
    url(r'^$', IndexView.as_view(), name='index'),
    url(r'^404/$', 'django.views.defaults.page_not_found'),
    url(r'^500/$', 'django.views.defaults.server_error'),
    url(r'^dealers/', include('dealers.urls', namespace='dealers')),
    url(r'^delivery/', include('delivery.urls', namespace='delivery')),
    url(r'^training/', include('training.urls', namespace='training')),
    url(r'^design/', include('design.urls', namespace='design')),
    url(r'^about/', include('about.urls', namespace='about')),
    url(r'^contacts/', include('contacts.urls', namespace='contacts')),
    url(r'^blog/', include('blog.urls', namespace='blog')),
    url(r'^privacy-policy/', include('policy.urls', namespace='policy')),
    url(r'^agreement/', include('agreement.urls', namespace='agreement')),
    url(r'^search/', include('search.urls', namespace='search')),
    url(r'^robokassa/', include('robokassa.urls', namespace='robokassa')),
    url(r'^sphinx/', include('libs.sphinx.urls', namespace='sphinx')),

    url(r'^admin/', include(honeypot_site.urls)),
    url(r'^dladmin/ckeditor/', include('ckeditor.admin_urls', namespace='admin_ckeditor')),
    url(r'^dladmin/gallery/', include('gallery.admin_urls', namespace='admin_gallery')),
    url(r'^dladmin/users/', include('users.admin_urls', namespace='admin_users')),
    url(r'^dladmin/ctr/', include('admin_ctr.urls', namespace='admin_ctr')),
    url(r'^dladmin/', include(admin.site.urls)),

    url(r'^away/$', 'libs.away.views.away', name='away'),
    url(r'^rating/', include('rating.urls', namespace='rating')),
    url(r'^ckeditor/', include('ckeditor.urls', namespace='ckeditor')),
    url(r'^blocks/', include('attachable_blocks.urls', namespace='blocks')),
    url(r'^ajaxcache/', include('libs.ajaxcache.urls', namespace='ajaxcache')),
    url(r'^social/', include('social_networks.urls', namespace='social_networks')),
    url(r'^autocomplete/', include('libs.autocomplete.urls', namespace='autocomplete')),
    url(r'^jsi18n/$', 'project.views.cached_javascript_catalog', name='jsi18n'),

    url(r'^sitemap\.xml$', 'django.contrib.sitemaps.views.sitemap', {'sitemaps': site_sitemaps}),
    url(r'^api/', include('shop.api_urls')),
    url(r'', include('shop.urls', namespace='shop')),
)

if settings.DEBUG:
    urlpatterns += [
        # Для доступа к MEDIA-файлам при разработке
        url(
            r'^media/(?P<path>.*)$',
            'django.views.static.serve',
            kwargs={'document_root': settings.MEDIA_ROOT}
        ),
        url(
            r'^static/(?P<path>.*)$',
            'django.views.static.serve',
            kwargs={'document_root': settings.STATIC_ROOT}
        ),
    ]

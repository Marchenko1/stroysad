from __future__ import absolute_import

import datetime
import os

from celery import Celery, Task
from celery.schedules import crontab
from django.conf import settings
from django.core import management
from django.template.loader import render_to_string


os.environ.setdefault('DJANGO_SETTINGS_MODULE', 'settings.production')
app = Celery('project')

# Using a string here means the worker will not have to
# pickle the object when using Windows.
app.config_from_object('django.conf:settings')
app.autodiscover_tasks(lambda: settings.INSTALLED_APPS)

app.conf.update(
    task_serializer='pickle',
    result_serializer='pickle',
    event_serializer='pickle',
    accept_content=['pickle'],
    beat_schedule = {
        'renew_ya_catalog': {
            'task': 'project.celery_proxy.renew_ya_catalog',
            'schedule': 60 * 60 * 24
        },
    },
)

app.autodiscover_tasks()


class CeleryTaskProxy(Task):
    def __init__(self, event = None, *args, **kwargs):
        if event is not None:
            self.name = event.task_name
            self.event = event
        super(self.__class__, self).__init__(*args, **kwargs)

    def __call__(self, *args, **kwargs):
        if self._app is None:
            return self.run(*args, **kwargs)
        else:
            return super(CeleryTaskProxy, self).__call__(*args, **kwargs)

    def run(self, *args, **kwargs):
        return self.event.emit(*args, **kwargs)


class CeleryPeriodicTaskProxy(Task):
    def __init__(self, name, function, *args, **kwargs):
        self.name = name,
        self.function = function

        super(CeleryPeriodicTaskProxy, self).__init__(*args, **kwargs)

    def __call__(self, *args, **kwargs):
        if self._app is None:
            return self.run(*args, **kwargs)
        else:
            return super(CeleryPeriodicTaskProxy, self).__call__(*args, **kwargs)

    def run(self, *args, **kwargs):
        return self.function(*args, **kwargs)


@app.task
def renew_ya_catalog():
    management.call_command('generate_yml', verbosity=0, interactive=False)

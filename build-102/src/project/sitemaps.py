from django.contrib.sitemaps import GenericSitemap
from main.models import MainPageConfig
from shop.models import ShopConfig, ShopCategory, ShopProduct
from dealers.models import DealersConfig
from delivery.models import DeliveryConfig
from training.models import TrainingConfig
from design.models import DesignConfig
from about.models import AboutConfig
from contacts.models import ContactsConfig

mainpage_dict = {
    'queryset': MainPageConfig.objects.all(),
    'date_field': 'updated',
}
shop_category_dict = {
    'queryset': ShopCategory.objects.filter(visible=True),
    'date_field': 'updated',
}
shop_product_dict = {
    'queryset': ShopProduct.objects.filter(visible=True),
    'date_field': 'updated',
}
dealers_dict = {
    'queryset': DealersConfig.objects.all(),
    'date_field': 'updated',
}
delivery_dict = {
    'queryset': DeliveryConfig.objects.all(),
    'date_field': 'updated',
}
training_dict = {
    'queryset': TrainingConfig.objects.all(),
    'date_field': 'updated',
}
design_dict = {
    'queryset': DesignConfig.objects.all(),
    'date_field': 'updated',
}
about_dict = {
    'queryset': AboutConfig.objects.all(),
    'date_field': 'updated',
}
contacts_dict = {
    'queryset': ContactsConfig.objects.all(),
    'date_field': 'last_modified',
}


site_sitemaps = {
    'main': GenericSitemap(mainpage_dict, changefreq='daily', priority=1),
    'shop_category': GenericSitemap(shop_category_dict, changefreq='daily', priority=1),
    'shop_product': GenericSitemap(shop_product_dict, changefreq='daily', priority=1),
    'dealers': GenericSitemap(dealers_dict, changefreq='weekly', priority=0.5),
    'delivery': GenericSitemap(delivery_dict, changefreq='weekly', priority=0.5),
    'training': GenericSitemap(training_dict, changefreq='weekly', priority=0.5),
    'design': GenericSitemap(design_dict, changefreq='weekly', priority=0.5),
    'about': GenericSitemap(about_dict, changefreq='weekly', priority=0.5),
    'contacts': GenericSitemap(contacts_dict, changefreq='weekly', priority=0.5),
}

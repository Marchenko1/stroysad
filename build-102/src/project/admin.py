from django import forms
from django.db import models
from django.utils.translation import ugettext_lazy as _
from google_maps import GoogleCoordsField, GoogleCoordsAdminWidget
from libs.stdimage.fields import StdImageField
from libs.stdimage.widgets import StdImageAdminWidget
from libs.valute_field import ValuteField, ValuteFormField
from libs.widgets import SplitDateTimeWidget, URLWidget, AutosizedTextarea


class ModelAdminInlineMixin:
    formfield_overrides = {
        models.CharField: {
            'widget': forms.TextInput(attrs={
                'class': 'full-width',
            })
        },
        models.EmailField: {
            'widget': forms.EmailInput(attrs={
                'class': 'full-width',
            })
        },
        models.URLField: {
            'widget': URLWidget(attrs={
                'class': 'full-width',
            })
        },
        models.TextField: {
            'widget': AutosizedTextarea(attrs={
                'class': 'full-width',
                'rows': 2,
            })
        },
        models.DateTimeField: {
            'widget': SplitDateTimeWidget
        },
        StdImageField: {
            'widget': StdImageAdminWidget
        },
        ValuteField: {
            'form_class': ValuteFormField
        },
        GoogleCoordsField: {
            'widget': GoogleCoordsAdminWidget
        },
    }


class ModelAdminMixin(ModelAdminInlineMixin):
    add_form_template = 'suit/change_form.html'
    change_form_template = 'suit/change_form.html'

    actions_on_top = True
    actions_on_bottom = True

    def suit_cell_attributes(self, obj, column):
        """ Классы для ячеек списка """
        if column == 'view':
            return {
                'class': 'mini-column'
            }
        else:
            return {}

    def get_suit_form_tabs(self, request, add=False):
        """ Получение вкладок для модели админки Suit """
        return getattr(self, 'suit_form_tabs', ())

    def render_change_form(self, request, context, add=False, change=False, form_url='', obj=None):
        """ Получаем вкладки Suit и передаем их в шаблон """
        suit_tabs = self.get_suit_form_tabs(request, add)
        context['suit_tabs'] = suit_tabs
        return super().render_change_form(request, context, add, change, form_url, obj)

    def view(self, obj):
        """ Ссылка просмотра на сайте для отображения в списке сущностей """
        if hasattr(obj, 'get_absolute_url'):
            admin_url = obj.get_absolute_url()
            if admin_url:
                return ('<a href="%s" target="_blank" title="%s">'
                        '   <span class="icon-eye-open icon-alpha75"></span>'
                        '</a>') % (admin_url, _('View on site'))
        return '<span>-//-</span>'
    view.short_description = '#'
    view.allow_tags = True

    @property
    def media(self):
        return super().media + forms.Media(
            js=(
                'admin/js/customize.js',
            ),
        )

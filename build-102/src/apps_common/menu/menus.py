from .base import Menu, MenuItem
from blog.models import BlogPost


def main():

    posts = BlogPost.objects.filter(visible=True)

    menu = Menu()
    menu.append(
        # MenuItem(
        #     title='Магазин оборудования',
        #     url='shop:index',
        # ),
        MenuItem(
            title='Дилерам и оптовикам',
            url='dealers:index',
        ),
        MenuItem(
            title='Доставка и оплата',
            url='delivery:index',
        ),
        MenuItem(
            title='Обучение',
            url='training:index',
        ),
        MenuItem(
            title='Проектирование',
            url='design:index',
        ),
        MenuItem(
            title='О компании',
            url='about:index',
        ),
        MenuItem(
            title='Контакты',
            url='contacts:index',
        ),
    )

    if posts:
        menu.append(
            MenuItem(
                title='Блог',
                url='blog:index',
            ),
        )

    return menu


def bottom():

    posts = BlogPost.objects.filter(visible=True)

    menu = Menu()
    menu.append(
        # MenuItem(
        #     title='Магазин оборудования',
        #     url='shop:index',
        # ),
        MenuItem(
            title='Дилерам и оптовикам',
            url='dealers:index',
        ),
        MenuItem(
            title='Доставка и оплата',
            url='delivery:index',
        ),
        MenuItem(
            title='Обучение',
            url='training:index',
        ),
        MenuItem(
            title='Проектирование',
            url='design:index',
        ),
        MenuItem(
            title='О компании',
            url='about:index',
        ),
        MenuItem(
            title='Контакты',
            url='contacts:index',
        ),
    )

    if posts:
        menu.append(
            MenuItem(
                title='Блог',
                url='blog:index',
            ),
        )

    return menu
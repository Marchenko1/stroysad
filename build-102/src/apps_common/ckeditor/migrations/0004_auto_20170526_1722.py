# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models
import libs.stdimage.fields
import ckeditor.models
import libs.storages.media_storage


class Migration(migrations.Migration):

    dependencies = [
        ('ckeditor', '0003_auto_20160906_0934'),
    ]

    operations = [
        migrations.AlterField(
            model_name='pagephoto',
            name='photo',
            field=libs.stdimage.fields.StdImageField(upload_to=ckeditor.models.split_by_dirs, aspects='normal', min_dimensions=(1024, 768), variations={'wide': {'size': (800, 450)}, 'normal': {'size': (512, 288)}, 'mobile': {'size': (224, 126)}}, blank=True, verbose_name='image', storage=libs.storages.media_storage.MediaStorage('page_photos')),
        ),
    ]

# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models
import libs.stdimage.fields
import libs.storages.media_storage
import ckeditor.models


class Migration(migrations.Migration):

    dependencies = [
        ('ckeditor', '0001_initial'),
    ]

    operations = [
        migrations.CreateModel(
            name='PageFile',
            fields=[
                ('id', models.AutoField(primary_key=True, auto_created=True, serialize=False, verbose_name='ID')),
                ('app_name', models.CharField(max_length=30, blank=True, verbose_name='application')),
                ('model_name', models.CharField(max_length=30, blank=True, verbose_name='model')),
                ('instance_id', models.IntegerField(verbose_name='entry id', db_index=True, default=0)),
                ('file', models.FileField(blank=True, upload_to=ckeditor.models.split_by_dirs, storage=libs.storages.media_storage.MediaStorage('page_files'), verbose_name='file')),
            ],
            options={
                'verbose_name': 'page file',
                'verbose_name_plural': 'page files',
                'default_permissions': (),
            },
        ),
        migrations.AlterModelOptions(
            name='pagephoto',
            options={'verbose_name': 'page photo', 'verbose_name_plural': 'page photos', 'default_permissions': ()},
        ),
        migrations.AlterModelOptions(
            name='simplephoto',
            options={'verbose_name': 'simple photo', 'verbose_name_plural': 'simple photos', 'default_permissions': ()},
        ),
        migrations.AddField(
            model_name='pagephoto',
            name='photo_crop',
            field=models.CharField(max_length=32, blank=True, editable=False, verbose_name='crop'),
        ),
        migrations.AlterField(
            model_name='pagephoto',
            name='photo',
            field=libs.stdimage.fields.StdImageField(aspects='on_page', variations={'on_page': {'size': (600, 340)}, 'admin_thumbnail': {'size': (150, 85)}}, verbose_name='image', blank=True, upload_to=ckeditor.models.split_by_dirs, storage=libs.storages.media_storage.MediaStorage('page_photos'), min_dimensions=(1024, 768)),
        ),
        migrations.AlterField(
            model_name='simplephoto',
            name='photo',
            field=libs.stdimage.fields.StdImageField(aspects=(), variations={'mobile': {'max_width': 512, 'crop': False, 'size': (0, 0)}}, verbose_name='image', blank=True, max_source_dimensions=(3072, 3072), upload_to=ckeditor.models.split_by_dirs, storage=libs.storages.media_storage.MediaStorage('simple_photos')),
        ),
    ]

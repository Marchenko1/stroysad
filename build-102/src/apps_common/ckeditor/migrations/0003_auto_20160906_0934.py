# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models
import libs.stdimage.fields
import ckeditor.models
import libs.storages.media_storage


class Migration(migrations.Migration):

    dependencies = [
        ('ckeditor', '0002_auto_20160906_0908'),
    ]

    operations = [
        migrations.AlterField(
            model_name='pagephoto',
            name='photo',
            field=libs.stdimage.fields.StdImageField(aspects='normal', upload_to=ckeditor.models.split_by_dirs, min_dimensions=(1024, 768), variations={'normal': {'crop': False, 'size': (0, 0), 'max_width': 800}, 'wide': {'crop': False, 'quality': 95, 'size': (0, 0), 'max_width': 1440}, 'mobile': {'crop': False, 'size': (0, 0), 'max_width': 480}}, verbose_name='image', storage=libs.storages.media_storage.MediaStorage('page_photos'), blank=True),
        ),
    ]

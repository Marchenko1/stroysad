# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('seo', '0002_auto_20160617_1326'),
    ]

    operations = [
        migrations.AlterModelOptions(
            name='seoconfig',
            options={'verbose_name': 'Defaults', 'default_permissions': ('change',)},
        ),
        migrations.AddField(
            model_name='seodata',
            name='noindex',
            field=models.BooleanField(verbose_name='noindex', help_text='the text on the page will not be indexed', default=False),
        ),
        migrations.AlterField(
            model_name='seoconfig',
            name='description',
            field=models.TextField(verbose_name='meta description', max_length=255, blank=True),
        ),
        migrations.AlterField(
            model_name='seoconfig',
            name='keywords',
            field=models.TextField(verbose_name='meta keywords', max_length=255, blank=True),
        ),
        migrations.AlterField(
            model_name='seoconfig',
            name='title',
            field=models.CharField(verbose_name='meta title', max_length=128, blank=True),
        ),
    ]

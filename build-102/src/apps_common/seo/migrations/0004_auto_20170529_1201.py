# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('seo', '0003_auto_20160906_1132'),
    ]

    operations = [
        migrations.AlterModelOptions(
            name='counter',
            options={'verbose_name': 'counter', 'ordering': ('sort_order',), 'verbose_name_plural': 'counters'},
        ),
        migrations.RemoveField(
            model_name='seodata',
            name='header',
        ),
        migrations.RemoveField(
            model_name='seodata',
            name='text',
        ),
        migrations.AddField(
            model_name='counter',
            name='sort_order',
            field=models.IntegerField(verbose_name='order', default=0),
        ),
        migrations.AddField(
            model_name='redirect',
            name='last_usage',
            field=models.DateField(editable=False, verbose_name='last usage', null=True),
        ),
        migrations.AlterField(
            model_name='counter',
            name='position',
            field=models.CharField(verbose_name='position', choices=[('head', 'Inside <head>'), ('body_top', 'Start of <body>'), ('body_bottom', 'End of <body>')], db_index=True, max_length=12),
        ),
        migrations.AlterField(
            model_name='seodata',
            name='description',
            field=models.TextField(blank=True, max_length=255, verbose_name='meta description'),
        ),
        migrations.AlterField(
            model_name='seodata',
            name='keywords',
            field=models.TextField(blank=True, max_length=255, verbose_name='meta keywords'),
        ),
        migrations.AlterField(
            model_name='seodata',
            name='noindex',
            field=models.BooleanField(help_text='text on the page will not be indexed', default=False, verbose_name='noindex'),
        ),
        migrations.AlterField(
            model_name='seodata',
            name='og_title',
            field=models.CharField(blank=True, max_length=255, verbose_name='header'),
        ),
        migrations.AlterField(
            model_name='seodata',
            name='title',
            field=models.CharField(blank=True, max_length=128, verbose_name='meta title'),
        ),
    ]

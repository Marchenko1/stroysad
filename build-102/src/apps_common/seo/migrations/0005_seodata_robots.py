# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('seo', '0004_auto_20170529_1201'),
    ]

    operations = [
        migrations.AddField(
            model_name='seodata',
            name='robots',
            field=models.TextField(max_length=255, verbose_name='robots', blank=True),
        ),
    ]

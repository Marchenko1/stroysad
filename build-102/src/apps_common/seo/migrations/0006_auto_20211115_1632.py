# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('seo', '0005_seodata_robots'),
    ]

    operations = [
        migrations.AddField(
            model_name='seodata',
            name='explicitly_index',
            field=models.BooleanField(help_text='При активном флаге в шапке сайта будет явно указан мета-тэг: <meta name="robots" content="index, follow"/>', verbose_name='Индексировать явно', default=False),
        ),
    ]

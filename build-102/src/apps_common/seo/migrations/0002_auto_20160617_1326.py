# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models
import django.utils.timezone
import libs.storages.media_storage


class Migration(migrations.Migration):

    dependencies = [
        ('seo', '0001_initial'),
    ]

    operations = [
        migrations.CreateModel(
            name='Robots',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, primary_key=True, auto_created=True)),
                ('text', models.TextField(blank=True, verbose_name='text')),
            ],
            options={
                'managed': False,
                'default_permissions': (),
                'verbose_name_plural': 'robots.txt',
                'verbose_name': 'file',
            },
        ),
        migrations.CreateModel(
            name='Redirect',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, primary_key=True, auto_created=True)),
                ('old_path', models.CharField(help_text="This should be an absolute path, excluding the domain name. Example: '/events/search/'.", max_length=200, unique=True, verbose_name='redirect from')),
                ('new_path', models.CharField(help_text="This can be either an absolute path (as above) or a full URL starting with 'http://'.", max_length=200, blank=True, verbose_name='redirect to')),
                ('permanent', models.BooleanField(default=True, verbose_name='permanent')),
                ('note', models.TextField(max_length=255, blank=True, verbose_name='note')),
                ('created', models.DateField(editable=False, default=django.utils.timezone.now, verbose_name='created')),
            ],
            options={
                'verbose_name_plural': 'redirects',
                'verbose_name': 'redirect',
                'ordering': ('old_path',),
            },
        ),
        migrations.AlterModelOptions(
            name='seoconfig',
            options={'verbose_name': 'Defaults'},
        ),
        migrations.AlterModelOptions(
            name='seodata',
            options={'default_permissions': ('change',), 'verbose_name_plural': 'SEO data', 'verbose_name': 'SEO data'},
        ),
        migrations.AddField(
            model_name='seodata',
            name='canonical',
            field=models.URLField(blank=True, verbose_name='canonical URL'),
        ),
        migrations.AlterField(
            model_name='seoconfig',
            name='description',
            field=models.TextField(max_length=255, blank=True, verbose_name='site description'),
        ),
        migrations.AlterField(
            model_name='seodata',
            name='description',
            field=models.TextField(max_length=255, blank=True, verbose_name='description'),
        ),
        migrations.AlterField(
            model_name='seodata',
            name='og_image',
            field=models.ImageField(upload_to='', storage=libs.storages.media_storage.MediaStorage('seo'), blank=True, verbose_name='image'),
        ),
    ]

def get_paginator_meta(paginator):
    """
        Получение метаданных из постраничной навигации для SEO.

        <link rel="canonical">
        <link rel="next">,
        <link rel="prev">

        Пример:
            # views.py:
                paginator = Paginator(...)

                seo = Seo()
                seo.set(get_paginator_meta(paginator))
    """
    meta = {
        'canonical': paginator.request.path,
    }

    if paginator.previous_page_number:
        meta['prev'] = paginator.link_to(paginator.previous_page_number, anchor=False)
        meta['noindex'] = True
    else:
        meta['explicitly_index'] = True

    if paginator.next_page_number:
        meta['next'] = paginator.link_to(paginator.next_page_number, anchor=False)

    return meta

from django.http.response import Http404
from django.views.generic.base import View
from django.utils.timezone import now, timedelta
from django.core.exceptions import ValidationError
from django.utils.translation import ugettext_lazy as _
from libs.cookies import set_cookie
from libs.views_ajax import AjaxViewMixin
from .models import RatingVote


class VoteView(AjaxViewMixin, View):
    def post(self, request):
        rating = request.POST.get('rating')
        try:
            rating = int(rating)
        except (TypeError, ValueError):
            raise Http404

        # проверка, что уже голосовал
        voted = request.COOKIES.get('voted')
        if voted is not None:
            return self.json_response({
                'error': _('Already voted!')
            })

        # запрет голосовать более 2-х раз с одного IP в час
        client_ip = request.META['REMOTE_ADDR']
        date_period = now() - timedelta(hours=1)
        last_votes = RatingVote.objects.filter(ip=client_ip, date__gte=date_period)
        if last_votes.count() >= 2:
            return self.json_response({
                'error': _('Already voted!')
            })

        vote = RatingVote(
            ip=client_ip,
            rating=rating,
        )
        try:
            vote.full_clean()
        except ValidationError:
            raise Http404
        else:
            vote.save()

        response = self.json_response()
        set_cookie(response, 'voted', rating, expires=30)
        return response

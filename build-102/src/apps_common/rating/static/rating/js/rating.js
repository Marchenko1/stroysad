(function($) {

    var query;
    $(document).on('click', '.rating .stars li', function() {
        var $star = $(this);
        var $list = $star.closest('.stars');

        var rating = $list.children().toArray().indexOf(this) + 1;
        if ((rating < 1) || (rating > 5)) return false;

        if (query) query.abort();
        query = $.ajax({
            url: window.js_storage.ajax_vote,
            type: 'POST',
            data: {
                rating: rating
            },
            dataType: 'json',
            success: function() {
                $list.removeClass().addClass('stars');
                $list.addClass('voted-' + rating);

                // если кука не поставилась через бэкенд - значит спамят.
                // Ставим куку, чтобы казалось, что всё ок.
                var cookie = $.cookie('voted');
                if (cookie === undefined) {
                    $.cookie('voted', rating, {
                        path: '/',
                        expires: 30
                    });
                }
            }
        });
    });

})(jQuery);

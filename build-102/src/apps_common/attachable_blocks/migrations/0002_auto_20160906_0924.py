# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('contenttypes', '0002_remove_content_type_name'),
        ('attachable_blocks', '0001_initial'),
    ]

    operations = [
        migrations.AlterModelOptions(
            name='attachableblock',
            options={'verbose_name_plural': 'attachable blocks', 'ordering': ('label',), 'default_permissions': (), 'verbose_name': 'attachable block'},
        ),
        migrations.AddField(
            model_name='attachablereference',
            name='ajax',
            field=models.BooleanField(default=False, help_text='load block through AJAX', verbose_name='AJAX'),
        ),
        migrations.AddField(
            model_name='attachablereference',
            name='block_ct',
            field=models.ForeignKey(null=True, to='contenttypes.ContentType', related_name='+'),
        ),
        migrations.AlterField(
            model_name='attachableblock',
            name='visible',
            field=models.BooleanField(default=True, verbose_name='visible'),
        ),
        migrations.AlterUniqueTogether(
            name='attachablereference',
            unique_together=set([]),
        ),
    ]

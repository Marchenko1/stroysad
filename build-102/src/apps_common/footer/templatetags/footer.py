from django.template import Library, loader, RequestContext
from social_networks.models import SocialLinks
from config.models import Config
from contacts.models import ContactsConfig, Address

register = Library()


@register.simple_tag(takes_context=True)
def footer(context, template='footer/footer.html'):
    """ Футер """
    request = context.get('request')
    if not request:
        return ''

    return loader.render_to_string(template, {
        'config': Config.get_solo(),
        'socials': SocialLinks.get_solo(),
        'addresses': Address.objects.all(),
    }, context_instance=RequestContext(request))

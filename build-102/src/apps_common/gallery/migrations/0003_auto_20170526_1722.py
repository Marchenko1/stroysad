# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('gallery', '0002_auto_20160905_1657'),
    ]

    operations = [
        migrations.AlterField(
            model_name='galleryitembase',
            name='self_type',
            field=models.ForeignKey(to='contenttypes.ContentType', editable=False, related_name='+'),
        ),
    ]

# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models
import django.utils.timezone


class Migration(migrations.Migration):

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='Log',
            fields=[
                ('id', models.AutoField(primary_key=True, auto_created=True, verbose_name='ID', serialize=False)),
                ('inv_id', models.PositiveIntegerField(blank=True, null=True, verbose_name='InvId')),
                ('step', models.PositiveSmallIntegerField(choices=[(1, 'Result'), (2, 'Success Page'), (3, 'Fail page')], verbose_name='step')),
                ('status', models.PositiveSmallIntegerField(choices=[(1, 'Message'), (2, 'Warning'), (3, 'Success'), (4, 'Error')], verbose_name='status')),
                ('message', models.CharField(max_length=255, verbose_name='message')),
                ('request', models.TextField(verbose_name='request')),
                ('created', models.DateTimeField(editable=False, verbose_name='create date', default=django.utils.timezone.now)),
            ],
            options={
                'ordering': ('-created',),
                'verbose_name': 'log message',
                'verbose_name_plural': 'log messages',
            },
        ),
    ]

# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('social_networks', '0003_sociallinks_social_youtube'),
    ]

    operations = [
        migrations.AddField(
            model_name='sociallinks',
            name='social_tg',
            field=models.URLField(max_length=255, blank=True, verbose_name='tg'),
        ),
    ]

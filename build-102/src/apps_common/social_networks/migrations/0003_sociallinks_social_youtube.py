# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('social_networks', '0002_auto_20160913_1325'),
    ]

    operations = [
        migrations.AddField(
            model_name='sociallinks',
            name='social_youtube',
            field=models.URLField(blank=True, verbose_name='youtube', max_length=255),
        ),
    ]

# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('social_networks', '0001_initial'),
    ]

    operations = [
        migrations.AddField(
            model_name='sociallinks',
            name='social_linkedin',
            field=models.URLField(blank=True, verbose_name='linkedIn', max_length=255),
        ),
        migrations.AddField(
            model_name='sociallinks',
            name='social_ok',
            field=models.URLField(blank=True, verbose_name='odniklassniki', max_length=255),
        ),
        migrations.AddField(
            model_name='sociallinks',
            name='social_vk',
            field=models.URLField(blank=True, verbose_name='VKontakte', max_length=255),
        ),
    ]

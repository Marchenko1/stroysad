from django.template import Library, loader, RequestContext
from shop.models import ShopCategory, DiscountConfig
from config.models import Config
from search.forms import SearchForm

register = Library()


@register.simple_tag(takes_context=True)
def header(context, template='header/header.html'):
    """ Шапка """
    request = context.get('request')
    current_root = context.get('current_root') or ''
    if not request:
        return ''

    return loader.render_to_string(template, {
        'config': Config.get_solo(),
        'form': SearchForm(request.GET),
        'current_root': current_root,
        'root_categories': ShopCategory.objects.root_categories(),
        'discount_config': DiscountConfig.get_solo(),
    }, context_instance=RequestContext(request))


import datetime

from django.conf import settings
from django.contrib.sessions.middleware import SessionMiddleware
from oauth2_provider.models import AccessToken, Application, RefreshToken
from oauth2_provider.settings import oauth2_settings
import logging

logger = logging.getLogger(__name__)


class UserPageTokenMiddleware(SessionMiddleware):
    def get_user_token(self, user, request):
        from oauthlib.oauth2.rfc6749.tokens import random_token_generator
        """
        Функция генерирующая токен
        :return:
        """
        app_id = getattr(settings, "OAUTH_API_ID", "default_app_name")
        expire_seconds = oauth2_settings.user_settings['ACCESS_TOKEN_EXPIRE_SECONDS']
        scopes = oauth2_settings.user_settings['SCOPES']

        live_token_time = datetime.datetime.now() + datetime.timedelta(seconds = expire_seconds)
        application = Application.objects.get(id = app_id)
        access_token = AccessToken.objects.create(user = user, application = application, expires = live_token_time, token = random_token_generator(request), scope = scopes)

        refresh_token = RefreshToken.objects.create(user = user, token = random_token_generator(request), access_token = access_token, application = application)

        token = {'access_token': access_token.token, 'token_type': 'Bearer', 'expires_in': expire_seconds, 'refresh_token': refresh_token.token, 'scope': scopes}

        return token

    def process_request(self, request):
        print('{time} Выполняется запрос [{user}]: {path}'.format(time = datetime.datetime.today(), path = request.path, user=str(request.user)))
        if request.user.is_authenticated:
            try:
                token = AccessToken.objects.filter(user = request.user).last()

                if token is None or not token.is_valid():
                    token = self.get_user_token(request.user, request)
                    token = token['access_token']
                else:
                    token = token.token
                setattr(request.user, 'token', token)
            except Exception as ex:
                logger.error('Ошибка: {0}'.format(str(ex)))

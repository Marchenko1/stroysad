operators_dict = {
    "=": "",
    ">": "__gt",
    ">=": "__gte",
    "<": "__lt",
    "<=": "__lte",
    "like": "__contains",
    "ilike": "__icontains",
    "in": "__in",
    "null": "__isnull"
}
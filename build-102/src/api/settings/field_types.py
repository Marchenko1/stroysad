from django.db.models import CharField, IntegerField, PositiveIntegerField, FloatField, BooleanField, DateTimeField, \
    AutoField

from django.conf import settings

FIELD_TYPE_MATCHES = [
    {'type': 'str', 'type_class': CharField},
    {'type': 'int', 'type_class': AutoField},
    {'type': 'int', 'type_class': IntegerField},
    {'type': 'pos_int', 'type_class': PositiveIntegerField},
    {'type': 'float', 'type_class': FloatField},
    {'type': 'bool', 'type_class': BooleanField},
    {'type': 'datetime', 'type_class': DateTimeField},
]

project_models = list(filter(lambda item: settings.API_ENTITIES[item]['model'] is not None, settings.API_ENTITIES))

for class_object in project_models:
    if hasattr(class_object, '_meta') and hasattr(class_object._meta, 'model_name') and class_object._meta.model_name.find('abstract') == -1:
        FIELD_TYPE_MATCHES.append({'type': 'entity', 'type_class': class_object, 'entity_name': class_object._meta.model_name})
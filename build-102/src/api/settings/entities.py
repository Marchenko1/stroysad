from django.db.models.signals import pre_save, post_save, pre_delete
from api.lib.classes.events.EventManager import EventManager
from api.lib.classes.events.entity import EntityEvent
from django.conf import settings

try:
    from django.contrib.contenttypes.models import ContentType

    # Получаем все модели в проекте и задаем их как сущности для обращения к API
    default_apps = ['auth', 'admin', 'contenttypes', 'sessions', 'oauth2_provider']
    for model in [content_type_model.model_class() for content_type_model in ContentType.objects.all()]:
        if model is None:
            continue
        if model._meta.model_name not in settings.API_ENTITIES and model._meta.app_label not in default_apps:
            api_handler = getattr(model, 'api_handler') if hasattr(model, 'api_handler') else None
            settings.API_ENTITIES[model._meta.model_name] = { 'model': model, 'api_handler': api_handler }
            # Подписываем на сигнал django "после записи объекта" все сущности модели при инициализации проекта
            EventManager.subscribe(model, EntityEvent.pre_save, pre_save)
            EventManager.subscribe(model, EntityEvent.post_save, post_save)
            EventManager.subscribe(model, EntityEvent.pre_delete, pre_delete)


except Exception as ex:
    # Первичный мигрейт ругается
    pass
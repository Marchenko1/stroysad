from api.lib.classes.events import SendingHttpEvent
from api.lib.classes.events.EventManager import EventManager
from api.lib.classes.events.http.http_event_history import HttpEventResponseRetainer
from api.lib.classes.events.entity import EntityEvent
from api.lib.classes.events.template_generators.base_generator import EventTemplateGenerator

# CRUD
from api.lib.interfaces.events.icrudevent import ICRUDEvent

# HTTP отправка пакетов в сторонние сервисы
EventManager.subscribe(event_or_broker=SendingHttpEvent('SendHttpRequest', 'on-need-send-http-request'))
# Фиксатор запросов HTTP (запрос и ответ хранятся в истории)
EventManager.subscribe(event_or_broker=HttpEventResponseRetainer('onHttpEventResponse', 'on-http-event-response'))
# Генератор шаблонов уведомлений
EventManager.subscribe(event_or_broker=EventTemplateGenerator('needGenerateTemplateAndSendEventResult', 'on-need-generate-template-and-send-event-result'))
EventManager.subscribe(event_or_broker=ICRUDEvent('CRUD', 'on-crud-event'))

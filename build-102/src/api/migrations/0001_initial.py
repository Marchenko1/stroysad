# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models
import uuid
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('contenttypes', '0002_remove_content_type_name'),
    ]

    operations = [
        migrations.CreateModel(
            name='Exchange',
            fields=[
                ('id', models.UUIDField(default=uuid.uuid4, db_index=True, editable=False, primary_key=True, serialize=False)),
                ('is_active', models.BooleanField(verbose_name='Активен', default=False)),
                ('is_export', models.BooleanField(verbose_name='Это выгрузка', default=True)),
                ('date', models.DateTimeField(verbose_name='Дата начала обмена', auto_now_add=True)),
                ('exchange_id', models.UUIDField(default=uuid.uuid4, editable=False)),
                ('state', models.PositiveSmallIntegerField(verbose_name='Состояние', choices=[(0, 'Инициализация параметров'), (1, 'Формирование пакетов'), (2, 'Начало обмена'), (3, 'Выполнение'), (4, 'Завершен'), (5, 'Нет ответа')], default=0)),
                ('attempts', models.PositiveIntegerField(verbose_name='Попыток обмена', default=0)),
            ],
            options={
                'permissions': (('can_view_exchange', 'Просмотр'), ('can_add_exchange', 'Добавление'), ('can_edit_exchange', 'Изменение'), ('can_remove_exchange', 'Удаление')),
                'verbose_name': 'Обмен',
                'verbose_name_plural': 'Обмены',
            },
        ),
        migrations.CreateModel(
            name='ExchangeLog',
            fields=[
                ('id', models.UUIDField(default=uuid.uuid4, db_index=True, editable=False, primary_key=True, serialize=False)),
                ('date', models.DateTimeField(verbose_name='Дата/время сообщения', auto_now=True)),
                ('message', models.TextField(verbose_name='Сообщение')),
                ('exchange', models.ForeignKey(verbose_name='Обмен', to='api.Exchange')),
            ],
            options={
                'permissions': (('can_view_exchange_message', 'Просмотр'), ('can_add_exchange_message', 'Добавление'), ('can_edit_exchange_message', 'Изменение'), ('can_remove_exchange_message', 'Удаление')),
                'verbose_name': 'Сообщение обмена',
                'verbose_name_plural': 'Сообщения обмена',
            },
        ),
        migrations.CreateModel(
            name='ExchangeObject',
            fields=[
                ('id', models.AutoField(serialize=False, verbose_name='ID', primary_key=True, auto_created=True)),
                ('obj_id', models.UUIDField(verbose_name='Внутренний UUID', null=True, blank=True, db_index=True)),
                ('obj_type', models.ForeignKey(on_delete=django.db.models.deletion.DO_NOTHING, to='contenttypes.ContentType', null=True, blank=True)),
            ],
        ),
        migrations.CreateModel(
            name='HttpEventHistory',
            fields=[
                ('id', models.AutoField(serialize=False, verbose_name='ID', primary_key=True, auto_created=True)),
                ('date', models.DateTimeField(verbose_name='Дата отправки', auto_now=True)),
                ('http_code', models.SmallIntegerField(verbose_name='HTTP код', default=0)),
                ('result', models.TextField(verbose_name='Ответ', default='')),
                ('request', models.TextField(verbose_name='Параметры запроса', default='')),
                ('is_json', models.BooleanField(verbose_name='Ответ в JSON', default=False)),
                ('attempt', models.IntegerField(verbose_name='Количество попыток отправки', default=0)),
            ],
            options={
                'verbose_name': 'История отправки HTTP запросов',
                'verbose_name_plural': 'История отправки HTTP запросов',
            },
        ),
        migrations.CreateModel(
            name='Mail',
            fields=[
                ('id', models.AutoField(serialize=False, verbose_name='ID', primary_key=True, auto_created=True)),
            ],
        ),
        migrations.CreateModel(
            name='MailTemplate',
            fields=[
                ('id', models.AutoField(serialize=False, verbose_name='ID', primary_key=True, auto_created=True)),
            ],
        ),
        migrations.CreateModel(
            name='Option',
            fields=[
                ('id', models.AutoField(serialize=False, verbose_name='ID', primary_key=True, auto_created=True)),
                ('name', models.CharField(verbose_name='Параметр', null=True, max_length=1024)),
                ('value', models.TextField(verbose_name='Значение', null=True)),
                ('params', models.TextField(verbose_name='Дополнительные параметры', null=True, blank=True)),
            ],
        ),
        migrations.CreateModel(
            name='Sms',
            fields=[
                ('id', models.AutoField(serialize=False, verbose_name='ID', primary_key=True, auto_created=True)),
            ],
        ),
        migrations.CreateModel(
            name='SmsTemplate',
            fields=[
                ('id', models.AutoField(serialize=False, verbose_name='ID', primary_key=True, auto_created=True)),
            ],
        ),
    ]

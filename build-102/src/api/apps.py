from django.apps import AppConfig
from api.lib.util.api_entities import set_api_entities
from django.conf import settings

class ApiConfig(AppConfig):
    name = 'api'

    def ready(self):
        from api.settings import entities
        from api.lib.classes.entity.event import ApiEvent
        set_api_entities([
            {'name': 'events', 'cls': ApiEvent}
        ])


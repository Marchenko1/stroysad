# Create your views here.
from django.http import HttpResponse
from api.lib.util.api import call_api, call_exchange_api
from api.lib.views import ApiFileView, ApiView, ApiPingView


class APIFileView(ApiFileView):
    def get(self, request, id, method_name):
        return call_api('attachment', method_name, request=request)

    def post(self, request, method_name):
        method = None
        if 'profile/image' in request.path:
            method = self.image_save
        if method is not None:
            return method(request, method_name)
        else:
            return HttpResponse('ok')


class APIView(ApiView):
    def post(self, request, entity_name = None, method_name = None):
        if entity_name is None and method_name is None:
            if request.path == '/api/ping/':
                return HttpResponse('ok')
        return call_api(entity_name, method_name, request = request)


class APIExchangeView(ApiView):
    def post(self, request, entity_name = None, method_name = None, stage = None):
        return call_exchange_api(entity_name, method_name, stage, request = request)


class APIPingView(ApiPingView):
    def post(self, request):
        return HttpResponse('ok')

from django.urls import path
from api.views import APIView, APIFileView, APIExchangeView

urlpatterns = [
    path('files/<str:id>/<str:method_name>/', APIFileView.as_view(), name='api-file-action'),
    path('files/<str:method_name>/', APIFileView.as_view(), name='api-root-files'),
    path('ping/', APIView.as_view()),
    path('<str:entity_name>/<str:method_name>/', APIView.as_view()),
    path('<str:entity_name>/<str:method_name>/<str:stage>/', APIExchangeView.as_view()),
]
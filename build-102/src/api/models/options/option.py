from django.db import models
from django.contrib.postgres.fields import HStoreField


class Option(models.Model):
    name = models.CharField("Параметр", max_length = 1024, null = True)
    value = models.TextField("Значение", null = True)
    params = models.TextField("Дополнительные параметры", null = True, blank = True)

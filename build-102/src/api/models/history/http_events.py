from django.db import models
import json


class HttpEventHistory(models.Model):
    date = models.DateTimeField("Дата отправки", auto_now = True)
    http_code = models.SmallIntegerField("HTTP код", default = 0)
    result = models.TextField("Ответ", default = "")
    request = models.TextField("Параметры запроса", default = "")
    is_json = models.BooleanField("Ответ в JSON", default=False)
    attempt = models.IntegerField("Количество попыток отправки", default=0)

    def __str__(self):
        try:
            return str(self.date) + ' [' + json.loads(self.request)['event'] + '] - ' + str(self.http_code)
        except Exception as ex:
            return str(self.date) + ' ' + self.http_code

    class Meta:
        verbose_name = "История отправки HTTP запросов"
        verbose_name_plural = "История отправки HTTP запросов"
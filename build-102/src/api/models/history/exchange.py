import uuid

from django.contrib.contenttypes.fields import GenericForeignKey
from django.contrib.contenttypes.models import ContentType
from django.db import models
from django.contrib.postgres.fields import ArrayField, HStoreField

from api.lib.classes.exchange.api_handler.api_exchange import ApiExchange
from api.lib.interfaces.events.ievent import IEvent


class Exchange(models.Model):
    id = models.UUIDField(default = uuid.uuid4, db_index = True, primary_key = True, editable = False)
    is_active = models.BooleanField("Активен", default = False)
    is_export = models.BooleanField("Это выгрузка", default = True)
    date = models.DateTimeField("Дата начала обмена", auto_now_add = True)
    exchange_id = models.UUIDField(default = uuid.uuid4, editable = False)

    fragments = ArrayField(
        HStoreField(),
    ),

    state = models.PositiveSmallIntegerField("Состояние", choices=(
        (0, 'Инициализация параметров'),
        (1, 'Формирование пакетов'),
        (2, 'Начало обмена'),
        (3, 'Выполнение'),
        (4, 'Завершен'),
        (5, 'Нет ответа'),
    ), default=0)

    attempts = models.PositiveIntegerField("Попыток обмена", default=0)

    """ API """
    api_handler = ApiExchange

    class ExchangeCRUD(IEvent):
        def do(self, options):
            if options['type'] == 'pre_delete':
                self.pre_delete(options)

        def pre_delete(self, options):
            print(options)
            print(options)
            print(options)
            print(options)

    crud_events = ExchangeCRUD

    class Meta:
        verbose_name = "Обмен"
        verbose_name_plural = "Обмены"

        permissions = (('can_view_exchange', 'Просмотр'), ('can_add_exchange', 'Добавление'), ('can_edit_exchange', 'Изменение'), ('can_remove_exchange', 'Удаление'),)


class ExchangeLog(models.Model):
    id = models.UUIDField(default = uuid.uuid4, db_index = True, primary_key = True, editable = False)
    date = models.DateTimeField("Дата/время сообщения", auto_now = True)
    message = models.TextField("Сообщение")
    exchange = models.ForeignKey(Exchange, verbose_name = "Обмен", on_delete = models.CASCADE)

    class Meta:
        verbose_name = "Сообщение обмена"
        verbose_name_plural = "Сообщения обмена"

        permissions = (('can_view_exchange_message', 'Просмотр'), ('can_add_exchange_message', 'Добавление'), ('can_edit_exchange_message', 'Изменение'), ('can_remove_exchange_message', 'Удаление'),)


class ExchangeObject(models.Model):
    obj_type = models.ForeignKey(ContentType, on_delete = models.DO_NOTHING, null = True, blank = True)
    obj_id = models.UUIDField('Внутренний UUID', db_index = True, null = True, blank = True)
    obj = GenericForeignKey('obj_type', 'obj_id')
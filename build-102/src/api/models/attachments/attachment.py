import os

from django.conf import settings
from django.db import  models
from django.db.models import Q
from django.core.files import File

from api.lib.classes.entity.CRUD.attachment import AttachmentCRUD
from api.lib.classes.entity.file import ApiFile
from api.lib.interfaces.events.ievent import IEvent
from api.lib.util.user import isset_group
from api.models.abstracts import AbstractCommonModel, AbstractIdModel, ChangesWatcher

from django.contrib.contenttypes.fields import GenericForeignKey
from django.contrib.contenttypes.models import ContentType


def upload_dir(instance, filename):
    return '{0}/{1}/{2}'.format(instance.owner_type.model, instance.owner_id, filename)


class Attachment(AbstractCommonModel, ChangesWatcher):
    owner_type = models.ForeignKey(ContentType, on_delete=models.DO_NOTHING, null=True, blank=False)
    owner_id = models.UUIDField('Внутренний UUID', db_index=True, null=True, blank=False)
    owner = GenericForeignKey('owner_type', 'owner_id')
    type = models.CharField(verbose_name="Тип вложения", max_length=100, blank=False, null=True)
    ext = models.CharField(verbose_name="Расширение", max_length=100, blank=False, null=True)
    file = models.FileField('Вложение', upload_to = upload_dir, null = True)
    is_main = models.BooleanField("Основной файл", default=False)
    name = models.CharField("Название", max_length=1024, null=True, blank=True)
    active = models.BooleanField("Активность", default = False)
    # Класс обработчик пользовательских функций
    api_handler = ApiFile

    @classmethod
    def have_access(cls, mode: str, user = None, request = None, id = None, uri = None):
        # Результат всех проверок прав доступа
        is_valid = False
        # Проверка на базовые права к модели
        permissions_is_valid = False
        # Проверка на группы доступа, которые имеют права на файлы по специфичным причинам
        group_is_valid = False

        if mode == 'r':
            if id is None and uri is None:
                raise Exception('Не указан идентификатор файла, к которому запрашивается доступ')

            if user is None and request is None:
                raise Exception('Должен быть указан Пользователь или HTTP Request на запрос к файлам')

        user = user if user is not None else request.user
        groups = user.groups.all()
        group_is_root = isset_group([g.name for g in groups], settings.ACCESS_TO_PRIVATE_FILES)

        # Чтение
        if mode == 'r':
            attachment = cls.objects.filter(Q(id=id) | Q(owner_id=user.id))
            permissions_is_valid = user.has_perm('api.can_view_attachment')
            is_valid = group_is_root or permissions_is_valid
        # Запись (добавление)
        elif mode == 'wa':
            permissions_is_valid = user.has_perm('api.can_add_attachment')
            is_valid = group_is_root or permissions_is_valid
        # Запись (изменение)
        elif mode == 'we':
            permissions_is_valid = user.has_perm('api.can_edit_attachment')
            is_valid = group_is_root or permissions_is_valid
        # Удаление
        elif mode == 'wr':
            permissions_is_valid = user.has_perm('api.can_remove_attachment')
            is_valid = group_is_root or permissions_is_valid

        return is_valid

    def extension(self):
        name, extension = os.path.splitext(self.file.name)
        return extension.replace('.', '')

    class Meta(AbstractCommonModel.Meta):
        abstract = False
        verbose_name = "Вложение"
        verbose_name_plural = "Вложения"

        permissions = (('can_view_attachment', 'Просмотр'), ('can_add_attachment', 'Добавление'),
                       ('can_edit_attachment', 'Изменение'), ('can_remove_attachment', 'Удаление'),)

    @classmethod
    def add(cls, user, owner, name, active, file_type, ext, file_path):

        try:
            if Attachment.have_access('wa', user = user):
                isset_files = Attachment.objects.filter(owner_id = owner.id, type=file_type)

                for isset_file in isset_files:
                    isset_file.delete()

                new_attach = Attachment(
                    owner = owner,
                    type = file_type,
                    ext = ext,
                    name = name,
                    active = active
                )
                new_attach.file = File(open(file_path, 'rb'), name=name)

                setattr(new_attach, '_create_author', user)
                setattr(new_attach, '_update_author', user)

                new_attach.save()
                return True

        except Exception as ex:
            print(str(ex))

        return False

    # @classmethod
    # def delete(cls, user, owner, name, active = None, file_type = None, ext = None):
    #     if Attachment.have_access('wr', user = user):
    #         Q_f = Q(owner = owner, name = name)
    #
    #         if active is not None:
    #             Q_f &= Q(active = active)
    #         if file_type is not None:
    #             Q_f &= Q(type = file_type)
    #         if ext is not None:
    #             Q_f &= Q(ext = ext)
    #
    #         attachs = cls.objects.filter(Q_f)
    #
    #         if attachs.count() > 0:
    #             attachs.delete()

setattr(Attachment, 'crud_events', AttachmentCRUD)
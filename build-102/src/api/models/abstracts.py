from django.contrib.contenttypes.fields import GenericForeignKey
from django.contrib.contenttypes.models import ContentType
from django.db import models
import uuid


class AbstractIdModel(models.Model):
    id = models.UUIDField(default = uuid.uuid4, db_index = True, primary_key = True, editable = False)
    class Meta:
        abstract = True

class ChangesWatcher(models.Model):
    create_author_type = models.ForeignKey(ContentType, on_delete=models.DO_NOTHING, null=True, blank=True, related_name='api_abstract_create_author_%(class)ss')
    create_author_id = models.UUIDField('Внутренний UUID', db_index=True, null=True, blank=True)
    create_author = GenericForeignKey('create_author_type', 'create_author_id')

    update_author_type = models.ForeignKey(ContentType, on_delete=models.DO_NOTHING, null=True, blank=True, related_name='api_abstract_update_author_%(class)ss')
    update_author_id = models.UUIDField('Внутренний UUID', db_index=True, null=True, blank=True)
    update_author = GenericForeignKey('update_author_type', 'update_author_id')

    class Meta:
        abstract = True

class AbstractCommonModel(AbstractIdModel):
    xml_id = models.CharField("Внешний идентификатор", max_length=1024, null=True, blank = True, unique=True)
    code = models.CharField("Код", max_length=100, null=True, blank = True, unique=True)
    ordering = models.IntegerField("Сортировка", default=500)

    # Кастомные поля базового класса для методов API
    need_clear_if_m2m = True

    class Meta:
        abstract = True
        ordering = ['ordering']

    def save(self, *args, **kwargs):
        if hasattr(self, '_create_author'):
            self.create_author = self._create_author
            if 'update_fields' in kwargs and 'create_author_id' not in kwargs['update_fields']:
                kwargs['update_fields'].append('create_author_type')
                kwargs['update_fields'].append('create_author_id')
        if hasattr(self, '_update_author'):
            self.update_author = self._update_author
            if 'update_fields' in kwargs and 'update_author_id' not in kwargs['update_fields']:
                kwargs['update_fields'].append('update_author_type')
                kwargs['update_fields'].append('update_author_id')
        super(__class__, self).save(*args, **kwargs)

    @staticmethod
    def modifier_before_save():
        return None

    @classmethod
    def get_m2m_fields(cls):
        return cls._meta.many_to_many

    @classmethod
    def get_search_fields(cls):
        search_fields = []
        for field in cls._meta.fields:
            if field.name == 'xml_id':
                search_fields.append(field.name)
        return search_fields



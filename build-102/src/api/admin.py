from django.contrib import admin

# Register your models here.
# from api.models import Attachment
from api.models.history.exchange import Exchange, ExchangeLog, ExchangeObject
from api.models.history.http_events import HttpEventHistory

admin.site.register(HttpEventHistory)
# admin.site.register(Attachment)
admin.site.register(Exchange)
admin.site.register(ExchangeLog)
admin.site.register(ExchangeObject)


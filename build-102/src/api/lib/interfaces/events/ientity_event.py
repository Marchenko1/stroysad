from api.lib.interfaces.events.ievent import IEvent


class IEntityEvent(IEvent):
    '''
    Интерфейс обработчика событий сущности:
        * запись
        * удаление
        * кастомное событие
    '''


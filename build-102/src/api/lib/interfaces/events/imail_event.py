from api.lib.interfaces.events.ievent import IEvent


class IMailEvent(IEvent):
    '''
    Интерфейс обработчика почтовых событий
    '''

    def collect_recipients(self):
        '''
        Получение списка адресов электронных ящиков получателей
        :return: list - массив адресов
        '''


    def do(self):
        '''
        Отправка писем
        :return: bool - результат отправки
        '''

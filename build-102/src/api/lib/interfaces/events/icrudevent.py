from django.db import models
from django.db.models.base import ModelBase

from api.lib.interfaces.events.ievent import IEvent

class ICRUDEvent(IEvent):
    '''
    Класс для выполнения действий модели при CRUD
    '''

    def emit(self, options = None):
        '''
        Выполняет указанные CRUD процедуры модели
        :param options: параметры события
        :return: None
        '''
        if options is not None:
            if 'sender' in options and 'type' in options:
                from django.conf import settings
                if options['sender'] in settings.API_ENTITIES:
                    model = settings.API_ENTITIES[options['sender']]['model']
                    if model is not None:
                        if isinstance(model, ModelBase):
                            if hasattr(model, 'crud_events'):
                                crud_events_class = model.crud_events(
                                    template_variables = self.template_variables,
                                    verbose_name = self.verbose_name,
                                    template_generate_is_possible = self.template_generate_is_possible,
                                    name = self.name,
                                    task_name = self.task_name,
                                    handler = self.handler
                                )

                                if options['type'] == 'post_save' and hasattr(crud_events_class, 'post_save'):
                                    crud_events_class.post_save(model, options)



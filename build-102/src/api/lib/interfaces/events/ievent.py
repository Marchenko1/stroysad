from api.lib.classes.events.decorators import event_retainer
from api.lib.decorators import api_handler

'''
Базовый интерфейс события
'''
class IEvent:
    def __init__(self, name, task_name, handler = None, **extra):
        self.template_variables = extra.setdefault('template_variables', {})
        self.verbose_name = extra.setdefault('verbose_name', name)
        self.template_generate_is_possible = extra.setdefault('template_generate_is_possible', False)
        self.name = name
        self.task_name = task_name
        self.handler = handler

    '''
        Декоратор, который отслеживает результаты выполнения события и в случае необходимости вызывает собственные обработчики (например, генерация и отправка уведомления по шаблону, генерация самого шаблона и т.д.)
    '''

    def emit(self, options = None):
        self.options = options
        '''
        Обработка события
        :return: bool
        '''

        result = None

        if hasattr(self, 'do'):
            result = self.do(self.options)
            if self.template_generate_is_possible:
                from api.lib.classes.events.EventManager import EventManager
                EventManager.emit('needGenerateTemplateAndSendEventResult', {'args': self.options, 'event_name': self.name, 'result': result}, is_async = False, celery_task_name = 'on-need-generate-template-and-send-event-result')

        return result
from api.lib.interfaces.events.ievent import IEvent


class IExchangeEvent(IEvent):
    '''
    Интерфейс обработчика событий обмена с внешними информационными системами
    '''
    def do(self, options = None):
        '''
        Обработка события обмена
        :return: bool - результат обработки
        '''

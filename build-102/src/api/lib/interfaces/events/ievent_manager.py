class IEventManager:
    '''
    Интерфейс базового менеджера событий
    '''

    @staticmethod
    def emit(event_name, options = None, is_async = True, celery_task_name = None):
        '''
        Вызов обработки события
        :return: bool - успешность выполнения
        '''

    @staticmethod
    def subscribe(model, func, signal = None, event = None):
        '''
        Подписка на событие
        :return: bool - успешность подписки
        '''
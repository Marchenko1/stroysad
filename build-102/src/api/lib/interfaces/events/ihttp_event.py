from api.lib.interfaces.events.ievent import IEvent


class IHttpEvent(IEvent):
    '''
    Интерфейс обработчика событий HTTP
    '''

    def do(self):
        '''
        Обработка HTTP события.
        Основное назначение - отправка или прием данных по HTTP
        :return: bool - результат обработки или данные полученные через HTTP
        '''

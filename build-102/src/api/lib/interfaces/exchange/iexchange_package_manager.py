

class IExchangePackageManager:
    """ Менеджер пакетов, который фрагментирует отправляемые данные """
    def __init__(self, options):
        # Общий размер фрагментов в байтах
        self.size = 0
        # Фрагменты с пакетами внутри по self.options['par_size'] Мб каждый максимум
        self.fragments = []
        # Параметры обмена
        self.options = options
        # Текущий фрагмент с которым работает пакетный менеджер
        self.current_fragment = None

    def size_verbose(self):
        """ Общий размер фрагмента в формате 1б, 1Кб, 1Мб """

    def remove_fragment_folder(self, fragment_information):
        """ Удаляет папку фрагмента за ненадобностью, т.к. дальнейшая работа только с zip """

    def slice_fragment_zip(self, fragment_information):
        """ Разделяет zip архив, когда фрагмент превышает максимальный размер к отправке """

    def add(self, unhandled_package, exchange_files):
        """ Добавляет пакеты во фрагмент до того момента, как фрагмент не заполнится до нужного размера """
        pass
import os
from uuid import uuid4


class IExchangeFragment:
    """ Фрагмент с пакетами данных, отпрваляемых по частям """
    def __init__(self, options):
        self.size = 0
        self.options = options
        self.packages = []
        self.id = str(uuid4())
        self.folder_path = os.path.join(self.options["exchange_files_dir"], "{0}".format(self.id))
        self.zip_path = os.path.join(self.options["exchange_files_dir"], "{0}.zip".format(self.id))
        self.zip_name = self.id + '.zip'
        self.arcname = self.id

    def adding_is_possible(self, package):
        """
            Возможно ли добавление нового пакета во фрагмент.
            Здесь нужно проверить размер текущего фрагмента с добавляемым
        """

    def is_fullfilled(self, package):
        """ Проверка на переполненность по установленному размеру одного пакета или фрагмента """

    def _save_package(self, package):
        """ Сохранение пакета на диск """

    def add(self, package):
        """ Добавление пакета во фрагмент """

    def information(self):
        """ Информация о фрагменте для обмена """

    def save(self):
        """ Сохранение фрагмента на диске """

from uuid import uuid4


class IExchangePackage:
    """ Пакет обмена """
    def __init__(self, unhandled_package, exchange_files, fragment):
        self.fragment = fragment
        self.package = unhandled_package.get()
        self.files = exchange_files
        self.package['files'] = self.files.information()
        unhandled_package_size = unhandled_package.size()
        files_size = exchange_files.size()
        self.size = unhandled_package_size + files_size
        self.id = str(uuid4())

    def is_fullfilled(self):
        """ Превышает ли основной dict допустимый размер """

    def _slice(self):
        """ Разбиение пакета на части """

    def _save(self):
        """ Сохранение пакета при полной обработке """

    def information(self):
        """ Информация о пакете для обмена включая файлы """

    def save(self):
        """ Сохранение пакета или разбиение на части если потребуется, после чего в обоих случаях вызывается _save(self) для записи """

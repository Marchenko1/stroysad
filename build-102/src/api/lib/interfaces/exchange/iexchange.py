from datetime import datetime
from uuid import UUID


from django.db.models import FileField, ImageField, ForeignKey
from django.db.models.base import ModelBase
from api.lib.classes.exchange._wrappers.exchange_packages import UnhandledExchangePackages
# from api.models import Attachment

class IExchange:
    """
        Класс для работы с обменом данными
    """
    def __init__(self, **params):
        """ Инициализация """

    def _pre_initialize_options(self, _options: dict):
        """
        Инициализация параметров обмена по умолчанию
        :param _options: dict с параметрами обмена
        :return: options['result'] - dict с результатом выполнения обмена
        """

    def _post_initialize_options(self, _options: dict):
        """
        Инициализация параметров обмена по умолчанию
        :param _options: dict с параметрами обмена
        :return: options['result'] - dict с результатом выполнения обмена
        """

    def _pickle_options(self):
        """ Сериализация параметров обмена в pickle """

    def _save_options(self):
        """ Сохранение параметров обмена в файл для реинициализации в случае ошибки """

    def log(self, message: str):
        """ Функция для логирования хода обмена """

    def state(self, state_index):
        """ Изменение состояния обмена """


class IExchangeManager:
    """
        Интерфейс менеджера обмена
    """
    def __init__(self, params, exchange):
        self.fragments = []
        self.options = params
        self.unhandled_packages = []
        self.package_manager = None
        self.exchange = exchange

    def log(self, message: str):
        print('{0}: {1}'.format(datetime.today(), message))
        self.exchange.log('{0}: {1}'.format(datetime.today(), message))

    def log_1c(self, message: str):
        print('{0} [1C] -> : {1}'.format(datetime.today(), message))
        self.exchange.log('{0} [1C] -> : {1}'.format(datetime.today(), message))

    def state(self, state: int):
        self.exchange.state(state)

    def _prepare_params_from_packages(self):
        from django.conf import settings
        """ Проверка пакетов на принадлежность модели и инициализация основных свойств для обмена """
        self.unhandled_packages = self.options['list'] if 'list' in self.options else []
        if len(self.unhandled_packages) > 0:
            if isinstance(type(self.unhandled_packages[0]), ModelBase):
                self.options['is_model_packages'] = True
                self.options['api_model'] = self.unhandled_packages[0]._meta.model
                self.options['api_object'] = self.unhandled_packages[0]._meta.model_name
            elif type(self.unhandled_packages[0]) is str or type(self.unhandled_packages[0]) is UUID:
                self.options['is_ids_packages'] = True

        if not self.options['is_model_packages']:
            api_object = settings.API_ENTITIES.get(self.options.get('api_object', '-'), None)
            self.options['api_model'] = api_object['model'] if api_object is not None else None
            if len(self.unhandled_packages) > 0:
                if 'files' in self.unhandled_packages[0]:
                    self.options['file_fields'].append('files')

        if self.options['api_model'] is not None:
            self.options['model_fields'] = self.options['api_model']._meta.fields
            for field in self.options['model_fields']:
                pass
                #TODO:
                # if type(field) is ForeignKey and field.related_model is Attachment:
                #     self.options['file_fields'].append(field.name)

            if self.options['is_ids_packages']:
                from api.lib.util.api import call_api
                objects = call_api(self.options['api_object'], 'list', params = {
                    'user': self.options['user'],
                    'with_files': True,
                    'per_page': 1000000,
                    'filter': [
                        {'key': 'id', 'operator': 'in', 'value': self.unhandled_packages}
                    ]
                })
                if objects['status'] == 'success':
                    self.unhandled_packages = objects['data']
                else:
                    raise Exception('Обмен приостановлен. При получении данных произошла критическая ошибка')

        self.unhandled_packages = UnhandledExchangePackages(self.unhandled_packages)

        if len(self.options['file_fields']) > 0:
            self.options['isset_files'] = True

        self.state(1)

    def do(self):
        """ Выполнение обмена данными """


class IExchangeExportManager(IExchangeManager):
    def __init__(self, *args, **kwargs):
        super(IExchangeExportManager, self).__init__(*args, **kwargs)
        self.folder = 'export'
    """
        Интерфейс выгрузки данных
    """

    def _start_exchange_session(self):
        """ Начало обмена, инициализация в 1С """

    def _end_exchange_session(self):
        """ Конец обмена, удаление временных файлов """

    def prepare_packages(self):
        """
        Разбиение пакетов выгрузки на части для пошаговой отправки
        :return: None
        """

    def save_packages(self):
        """ Сохранение пакетов на диске и дополнительное разбиение на фрагменты пакетов, которые не уместились """


class IExchangeImportManager(IExchangeManager):
    def __init__(self, *args, **kwargs):
        super(IExchangeImportManager, self).__init__(*args, **kwargs)
        self.folder = 'import'
    """
        Интерфейс загрузки данных
    """

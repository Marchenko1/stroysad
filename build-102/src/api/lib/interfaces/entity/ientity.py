from api.lib.decorators import api_handler

class IEntity:
    '''
    Объект к которому можно обращаться через API
    '''
    def __init__(self, model, params, errors, warnings):
        self.model = model
        self.params = params
        self.errors = errors
        self.warnings = warnings
        self.result = {'status': 'success'}

    @api_handler
    def list(self, *args, **kwargs):
        response = kwargs['params']['default_result']
        result = kwargs['params']['default_result'].to_dict()
        if result['status'] == 'success':
            if 'with_files' in kwargs['params']:
                from api.lib.util.api import call_api
                items_list = result['data']
                ids = [item['id'] for item in items_list]

                images_call_api_params = {
                    'user': kwargs['params']['user'],
                    'owner_id': ids,
                    'original_response': True
                }

                images = call_api(self.params['entity_name'], 'files', params = images_call_api_params)
                if images['status'] == 'success':
                    for item in items_list:
                        item['files'] = list(filter(lambda i: str(i.owner_id) == item['id'], images['data']))

        self.result = result
        return self

    @api_handler
    def files(self, *args, **kwargs):
        from api.lib.util.api import call_api
        user = self.params['request'].user if 'request' in self.params and self.params['request'] is not None else self.params['user']
        attachments_call_api_params = {
            'user': user if 'user' not in self.params else self.params['user'],
            'filter': [
                {'key': 'owner_id', 'operator': '=' if type(self.params['owner_id']) is not list else 'in', 'value': self.params['owner_id']}
            ],
        }
        if 'original_response' in self.params:
            attachments_call_api_params['original_response'] = self.params

        attachments = call_api('attachment', 'list', params = attachments_call_api_params)

        if attachments['status'] == 'success':
            attachments = attachments['data']
        else:
            attachments = []

        self.result['data'] = attachments
        return self

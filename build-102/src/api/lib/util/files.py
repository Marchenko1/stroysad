def upload_dir(instance, filename):
    if hasattr(instance, 'owner_type'):
        return '{0}/{1}/{2}'.format(instance.owner_type.model, instance.owner_id, filename)
    else:
        return '{0}/{1}/{2}'.format(instance._meta.model_name, instance.id, filename)

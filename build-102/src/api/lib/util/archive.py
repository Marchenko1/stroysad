import os

import copy

def write_recursively(zip, path = None, arcname = None, hierarchy = None):
    files = os.scandir(path)

    if hierarchy is None:
        hierarchy = ''
    else:
        if hierarchy == '':
            hierarchy = arcname
        else:
            hierarchy = os.path.join(hierarchy, arcname)

    for file in files:
        is_dir = os.path.isdir(file.path)
        if is_dir:

            write_recursively(zip = zip, path = file.path, arcname = file.name, hierarchy = copy.deepcopy(hierarchy))
        else:
            print(file.path)
            print(os.path.join(hierarchy, file.name))
            print('----------------------------------------------\r\n')
            zip.write(file.path, arcname = os.path.join(hierarchy, file.name))

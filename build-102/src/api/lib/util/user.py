def isset_group(group_names: list, groups: list):
    return len(set(group_names).intersection(set(groups))) > 0
from django.contrib.auth import get_user_model
from django.contrib.auth.models import Permission
from django.contrib.contenttypes.models import ContentType
from django.db import transaction
# from config.custom_options import SERVICE_USER_WITH_FULL_RIGHTS

def get_service_user():
    '''
    :return: Получает или создает сервисного пользователя, например для выполнения периодических заданий
    '''
    User = get_user_model()

    user = User.objects.filter().order_by('id').first()
    # if user is None:
    #     try:
    #         user = User(phone=SERVICE_USER_WITH_FULL_RIGHTS, username = SERVICE_USER_WITH_FULL_RIGHTS)
    #         user.is_superuser = True
    #         user.is_staff = True
    #         user.save()
    #         user.set_password(SERVICE_USER_WITH_FULL_RIGHTS)
    #     except Exception as ex:
    #         print(str(ex))
    return user


@transaction.atomic
def init_rights(api_entities: list):
    '''
    Инициализация прав доступа для сущности API. Создает или удаляет права доступа в зависимости от указанных в списке классов сущностей
    У всех сущностей для которых ограничен доступ нужно делать сдетичный метод "rights"
    :param api_entities: список сущностей API для которых нужно проинициализировать права доступа
    :return: None
    '''

    try:
        test = ContentType.objects.first()

        right_prefix = 'api_entity_can_'

        for api_entity in api_entities:
            content_type = ContentType.objects.filter(app_label='api_custom', model=api_entity.__name__).last()
            if content_type is None:
                content_type = ContentType(app_label='api_custom', model=api_entity.__name__)
                content_type.save()
            if hasattr(api_entity, 'rights'):
                rights = api_entity.rights()
                for method_name in rights:
                    # Обходим все права, добавляем уникальный префикс API и проверяем уже существующие, чтобы отсеить список на добавление
                    rights_list = [{'name': right_prefix + right["name"], 'verbose': right['verbose']} for right in rights[method_name]]
                    codename_list = set(right_prefix + right["name"] for right in rights[method_name])
                    isset_rights = set(perm['codename'] for perm in Permission.objects.filter(codename__in=codename_list).values('codename'))
                    need_save_rights = codename_list.difference(isset_rights)
                    for right_item in list(filter(lambda right_item: right_item['name'] in need_save_rights, rights_list)):
                        right = Permission(codename=right_item['name'], name=right_item['verbose'], content_type=content_type)
                        right.save()
    except Exception as ex:
        return








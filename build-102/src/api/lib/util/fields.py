approved_fields = []
def exclude_forbidden_fields(model, all_approved_fields):
    approved_fields = []
    if hasattr(model, 'forbidden_fields'):
        forbidden_fields = model.forbidden_fields()
        for field in all_approved_fields:
            if not field['name'] in forbidden_fields:
                for forbiden_field in forbidden_fields:
                    if not field['name'].startswith(forbiden_field):
                        approved_fields.append(field)
    else:
        approved_fields = [field for field in all_approved_fields]
    return approved_fields

def approved_fields_to_names_list(approved_fields: list):
    approved_fields = [field['name'] for field in approved_fields]
    return approved_fields

def approved_fields_to_meta(approved_fields: list):
    meta_fields = []
    sub_entities = {}
    for field in approved_fields:
        sub_entity_index = field['name'].find('__')
        if sub_entity_index != -1:

            sub_entity_name = field['name'][:sub_entity_index]
            sub_entity_field_name = field['name'][sub_entity_index + 2:]
            sub_entity_model_name = field['model']['name']
            sub_entity_model_name_verbose = field['model']['verbose']
            del field['model']

            if not sub_entities.get(sub_entity_model_name):
                sub_entities[sub_entity_model_name] = {'type': 'm2m', 'verbose': sub_entity_model_name_verbose, 'fields': []}
                new_field = dict()
                new_field.update(field)
                new_field['name'] = sub_entity_field_name
                sub_entities[sub_entity_model_name]['fields'].append(new_field)
            else:
                new_field = dict()
                new_field.update(field)
                new_field['name'] = sub_entity_field_name
                sub_entities[sub_entity_model_name]['fields'].append(new_field)
        else:
            meta_fields.append(field)
    if len(sub_entities.keys()) > 0:
        meta_fields.append(sub_entities)

    return meta_fields

def get_search_fields_from_model(entity_model):
    search_fields = []

    if hasattr(entity_model, 'get_search_fields'):
        search_fields = entity_model.get_search_fields()
    # else:
    #     search_fields.append('xml_id')

    return search_fields
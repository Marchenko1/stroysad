from api.lib.classes.events.EventManager import EventManager


def delete(model):
    isset = True
    while isset == True:

        entities = model.objects.all()[:5000]

        for entity in entities:
            entity.delete()

        count = model.objects.all().count()
        print("model: {} - count: {}".format(model._meta.model_name, count))
        isset = count > 0

def send_1C_http_request(url, options, is_async = False, is_exchange = False):
    return EventManager.emit('SendHttpRequest', options = {
        'event': url,
        'packages': options,
        'is_exchange': is_exchange
    }, is_async = is_async)
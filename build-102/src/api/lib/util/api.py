from api.lib.classes.exceptions import verbose_errors
from api.lib.classes.handler import ApiHandler
from api.lib.classes.response import APIResponse


def call_api(entity_name, method_name, request=None, params=None):
    api_handler = ApiHandler(entity_name=entity_name, method_name=method_name, request=request, params=params)
    api_handler.log.append('Инициализация обмена данными')
    if api_handler.is_possible():
        result = api_handler.execute()
        if request is not None:
            result = result.to_http()
        else:
            result = result.to_dict()
        return result
    else:
        result = APIResponse({'status': 'error', 'errors': verbose_errors(api_handler.errors)})
        if request is not None:
            result = result.to_http()
        else:
            result = result.to_dict()

    return result


def call_exchange_api(entity_name, entity_obj_name, method_name, request=None, params=None):
    if params is None:
        params = {'entity_name': entity_obj_name}
    else:
        params['entity_name'] = entity_obj_name

    api_handler = ApiHandler(entity_name = entity_name, method_name = method_name, request = request, params = params)
    if api_handler.is_possible():
        result = api_handler.execute()
        if request is not None:
            result = result.to_http()
        else:
            result = result.to_dict()
        return result
    else:
        result = APIResponse({ 'status': 'error', 'errors': verbose_errors(api_handler.errors) })
        if request is not None:
            result = result.to_http()
        else:
            result = result.to_dict()

    return result


import itertools
import uuid
from datetime import datetime, date

from django.db.models import ForeignKey

from api.lib.util.fields import get_search_fields_from_model

def merge_values(approved_fields, values, model):
    grouped_results = itertools.groupby(values, key = lambda value: value['id'])
    merged_values = []

    fields = model._meta.fields
    foreign_fields_filter = lambda field: type(field) is ForeignKey
    foreign_fields = [f.name for f in list(filter(foreign_fields_filter, fields))]

    for k, g in grouped_results:
        groups = list(g)
        merged_value = { }
        sub_entities_ids = { }
        for group in groups:
            group_sub_entities = { }
            empty_sub_entities = { }
            for key, val in group.items():
                val = str(val) if type(val) is uuid.UUID else val if not type(val) is datetime and not type(val) is date else val.strftime("%Y-%m-%d") if type(val) is date else val.strftime("%Y-%m-%d %H:%M:%S")
                if val == '1-01-01':
                    val = None
                if not merged_value.get(key) and key.find('__') == -1:
                    merged_value[key] = val
                elif key.find('__') != -1:
                    key_entity = key[:key.find('__')]
                    key_entity_field = key[key.find('__') + 2:]
                    if empty_sub_entities.get(key_entity):
                        continue

                    if key_entity_field == 'id':
                        if not val in sub_entities_ids:
                            if val is not None:
                                sub_entities_ids[val] = 'Y'
                            else:
                                # Метка на пустую связь
                                empty_sub_entities[key_entity] = 'E'
                                group_sub_entities[key_entity] = None
                                continue
                        else:
                            group_sub_entities[key_entity] = 'N'
                            continue
                    else:
                        if key_entity in group_sub_entities and group_sub_entities[key_entity] == 'N':
                            continue

                    if group_sub_entities.get(key_entity) and isinstance(group_sub_entities[key_entity], dict):
                            group_sub_entities[key_entity][key_entity_field] = val
                    else:
                        group_sub_entities[key_entity] = { key_entity_field: val }

            for sub_entity_name in group_sub_entities.keys():
                if not merged_value.get(sub_entity_name) and group_sub_entities[sub_entity_name] != 'N':
                    if group_sub_entities[sub_entity_name] is not None or sub_entity_name in foreign_fields:
                        merged_value[sub_entity_name] = [group_sub_entities[sub_entity_name]] if sub_entity_name not in foreign_fields else group_sub_entities[sub_entity_name]
                    else:
                        merged_value[sub_entity_name] = []

                elif group_sub_entities[sub_entity_name] != 'N':
                    if group_sub_entities[sub_entity_name] is not None or sub_entity_name in foreign_fields:
                        if sub_entity_name not in foreign_fields:
                            merged_value[sub_entity_name].append(group_sub_entities[sub_entity_name])
                        else:
                            merged_value[sub_entity_name] = group_sub_entities[sub_entity_name]
                    else:
                        merged_value[sub_entity_name] = []

        merged_values.append(merged_value)

    return merged_values

def add_m2m_into_entity(entity, field_value):
    ids = None

    return ids

def search_entities(entity_model, values: list):
    search_fields = get_search_fields_from_model(entity_model)

    searched_entities = []
    i = 0

    new_values = []
    for entity_data in values:
        entity = search_entity(entity_model, entity_data)

        if entity is not None:
            searched_entities.append(entity)
        else:
            new_values.append(entity_data)

    return searched_entities, new_values

def search_entity(entity_model, entity_data: dict):
    search_fields = get_search_fields_from_model(entity_model)

    entity = None
    if 'id' in entity_data:
        entity = entity_model.objects.filter(id=entity_data['id']).last()

    for search_field in search_fields:
        if entity is None:
            if search_field in entity_data:
                filter_params = {search_field: entity_data[search_field]}
                entity = entity_model.objects.filter(**filter_params).last()
        else:
            break

    return entity
from datetime import datetime, date
from uuid import UUID

class JsonConverter:
    approved_fields = None
    model = None

    @classmethod
    def convert_dict(cls, element):
        for field in element:
            if type(element[field]) is UUID or type(element[field]) is datetime or type(element[field]) is date:
                val = str(element[field])
                if val == '1-01-01':
                    val = None
                element[field] = val
        return element

    @classmethod
    def to_json(cls, elements, approved_fields, model):
        '''
        :param elements: Элемент или список элементов к конвертации в JSON
        :return: Массив (list) элементов (dict) готовых к конвертации в json
        '''
        cls.approved_fields = approved_fields
        cls.model = model

        if type(elements) is dict:
            # На входе один элемент
            elements = cls.convert_dict(elements)
        elif type(elements) is list:
            for entity in elements:
                cls.convert_dict(entity)
        else:
            elements = cls.convert_object(elements)
        return elements

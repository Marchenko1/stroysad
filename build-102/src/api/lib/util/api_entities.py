from django.conf import settings


def set_api_entities(custom_entities: list):
    from api.lib.util.rights import init_rights

    init_rights([entity['cls'] for entity in custom_entities])

    for entity in custom_entities:
        settings.API_ENTITIES[entity['name']] = {'model': None, 'api_handler': entity['cls']}

    print('Инициализация сущностей API:')
    for entity in settings.API_ENTITIES:
        print('Название сущности: "{0}", '.format(entity) +
            'Модель: "{0}", '.format(settings.API_ENTITIES[entity]["model"].__name__ if settings.API_ENTITIES[entity]["model"] is not None else "-") +
              'Обработчик: "{0}"'.format(settings.API_ENTITIES[entity]["api_handler"].__name__ if settings.API_ENTITIES[entity]["api_handler"] is not None else "-"))
        
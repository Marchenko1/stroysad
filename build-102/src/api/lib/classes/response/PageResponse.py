import json

from django.contrib.auth import get_user_model
from django.shortcuts import render, redirect
import logging

from django.utils.http import urlencode

logger = logging.getLogger(__name__)
User = get_user_model()


class PageResponse:
    @classmethod
    def render(cls, request, template, context = None, js_context = None):
        properties = {
            'page_properties': {},
            'js_properties': {}
        }

        if context is not None:
            properties['page_properties'] = context

        if js_context is None or type(js_context) is not dict:
            js_context = {}

        if 'user_photo_url' not in js_context:
            js_context['user_photo_url'] = User.get_photo_url(request.user)

        if request.user.is_authenticated:
            js_context['token'] = getattr(request.user, 'token', None)

        try:
            properties['js_properties'] = json.dumps(js_context, ensure_ascii = False)
        except Exception as ex:
            properties['js_properties'] = {}
            logger.error(ex)

        return render(request, template, properties)

    @classmethod
    def redirect(cls, request, path, url_params = None):
        return redirect('{0}?{1}'.format(path, urlencode(url_params)) if url_params is not None else path)
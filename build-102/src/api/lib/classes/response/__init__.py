'''
    Класс обработчик выдаваемого результата
'''
from django.http import JsonResponse
from .PageResponse import PageResponse

class APIResponse:
    def __init__(self, result: dict):
        self.result = result

    def to_http(self):
        try:
            return JsonResponse(self.result) if type(self.result) is dict else self.result
        except Exception as ex:
            return JsonResponse({'status': self.result['status'], 'errors': [str(ex)], 'warning': {
                'description': 'Не удалось выдать результат запроса в формате JSON',
                'code': 5,
                'result_message': str(self.result)
            }})

    def to_dict(self):
        return self.result
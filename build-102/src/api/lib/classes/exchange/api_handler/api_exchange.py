import logging

from django.db.models import Q
from api.lib.decorators import api_handler
from api.lib.interfaces.entity.ientity import IEntity


logger = logging.getLogger(__name__)


class ApiExchange(IEntity):
    """
    Класс API сущности User
    """
    def __exchange(self):
        entity_name = self.params['entity_name']
        exchange_id = self.params['exchange_id']
        datafile = self.params['datafile'] if 'datafile' in self.params else None

        from api.lib.classes.exchange import Exchange

        exchange = Exchange(
            is_export = False,
            api_object = entity_name,
            user = self.params['user'],
            params = {
                'stage': self.params['method_name'],
                'datafile': datafile,
                'id': exchange_id
            }
        )

        result = exchange.do()

        return result

    @api_handler
    def fragment(self, *args, **kwargs):
        result = self.__exchange()
        if result == False:
            self.result['status'] = 'error'
        return self

    @api_handler
    def do(self, *args, **kwargs):
        result = self.__exchange()
        if type(result) is not dict or (type(result) is dict and 'status' in result and result['status'] != 'success') or (type(result) is dict and 'data' in result and 'results' not in result['data']):
            self.result['status'] = 'error'
        else:
            self.result['data'] = result['data']
        return self

    @api_handler
    def get_active(self, *args, **kwargs):
        from api.models import Exchange as ExchangeModel
        self.result['data'] = [
            {
                'exchange_id': str(active_exchange.exchange_id),
                'date': str(active_exchange.date),
                'state': active_exchange.state
            } for active_exchange in ExchangeModel.objects.filter(is_active = True)]
        return self

    @api_handler
    def test(self, *args, **kwargs):
        try:
            from api.lib.classes.exchange import Exchange
            from api.lib.util.api import call_api
        except Exception as ex:
            logger.error(str(ex))

        return self

    @api_handler
    def get_log(self, *args, **kwargs):
        from api.models import ExchangeLog
        exchange_id = self.params['exchange_id']
        last_log_id = None
        if 'last_log_id' in self.params:
            last_log_id = self.params['last_log_id']
        last_date = None
        if 'last_date' in self.params:
            last_date = self.params['last_date']

        filter_Q = Q(date__gt = last_date, exchange__exchange_id = exchange_id) if last_date is not None else Q(exchange__exchange_id = exchange_id)

        logs = ExchangeLog.objects.filter(filter_Q).values('id', 'message', 'date')

        if last_log_id is not None:
            logs = logs.exclude(id = last_log_id)

        logs = logs.order_by('date')

        last_log_id_stepped = False

        self.result['data'] = [{'id': log['id'], 'message': log['message'], 'date': str(log['date'])} for log in logs] if logs.count() > 0 else []

        return self

import sys

class UnhandledExchangePackages:
    """ Обертка для пакетов обмена """
    def __init__(self, packages: list):
        """
        :param packages: пакеты обмена в виде [ {} | ModelBase(object) ]
        """
        self.unhandled_packages = [UnhandledExchangePackage(package) for package in packages]

    def __iter__(self):
        return iter(self.unhandled_packages)

    def __getitem__(self, item):
        return self.unhandled_packages[item]

    def __len__(self):
        return len(self.unhandled_packages)

    def size(self):
        return sys.getsizeof(self.unhandled_packages)

    def pop(self):
        return self.unhandled_packages.pop()


class UnhandledExchangePackage:
    """ Обертка для пакета обмена. Служит для возврата запрашиваемого атрибута """
    def __init__(self, unhandled_package):
        self.unhandled_package = unhandled_package

    def __getitem__(self, item):
        if type(self.unhandled_package) is dict:
            return self.unhandled_package[item]
        else:
            return getattr(self.unhandled_package, item, None)

    def set(self, key, value):
        self.unhandled_package[key] = value

    def size(self):
        return sys.getsizeof(self.unhandled_package)

    def type(self):
        return type(self.unhandled_package)

    def get(self):
        return self.unhandled_package

    def pop(self, attr):
        return self.unhandled_package.pop(attr)

from functools import reduce
from uuid import UUID

from django.db.models.base import ModelBase

from django.conf import settings

class ExchangeFiles:
    """ Обертка для файлов обмена """
    def __init__(self, attachments: list):
        """
        :param files: файлы обмена в виде [ {} | Field(object) ]
        """

        if len(attachments) > 0 and (type(attachments[0]) is str or type(attachments[0]) is UUID) and '/' not in attachments[0]:
            from api.models import Attachment
            attachments = Attachment.objects.filter(id__in=attachments)

        self.attachments = [ExchangeFile(attachment) for attachment in attachments]

    def __getitem__(self, item):
        return self.attachments[item]

    def __iter__(self):
        return iter(self.attachments)

    def information(self):
        from api.lib.util.api import call_api
        return call_api('attachment', 'list', params = {
            'user': settings.SERVICE_USER_WITH_FULL_RIGHTS,
            'filter': [{'key': 'id', 'operator': 'in', 'value': [file.id for file in self.attachments]}]
        })['data']

    def size(self):
        all_size = 0
        for attachment in self.attachments:
            all_size += attachment.size()
        return all_size


class ExchangeFile:
    """ Обертка для файла обмена. Служит для возврата запрашиваемого атрибута и обработки файта к передаче или загрузке """
    def __init__(self, file):
        self.attachment = file

    def __getitem__(self, item):
        if type(self.attachment) is dict:
            return self.attachment[item]
        else:
            return getattr(self.attachment, item, None)


    def __getattr__(self, item):
        return getattr(self.attachment, item)

    def read(self, length = None):
        return self.file.file.file.read() if length is None else self.file.file.file.read(length)


    def information(self):
        return {
            'id': str(self.attachment.id)
        }

    def size(self):
        all_size = 0
        if type(self.attachment) is not dict:
            all_size += self.attachment.file.size
        return all_size





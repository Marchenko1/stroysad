import json
import os
import shutil
import zipfile

from api.lib.interfaces.exchange.iexchange import IExchangeImportManager
from django.core.files.base import ContentFile
from django.core.files.storage import default_storage
import binascii


class ExchangeImportManager(IExchangeImportManager):
    def do(self):
        try:
            result = False
            if self.exchange.history.is_active == False:
                self.log("=================== Начало обмена данными с 1С ===================")
                self.exchange.activate()

            if self.options['stage'] == 'fragment':
                self.log("Сохранение фрагмента файла на диск: {0} ...".format(self.options['datafile'].name))
                if not os.path.exists(self.options['exchange_root_dir']):
                    os.mkdir(self.options['exchange_root_dir'])
                if not os.path.exists(self.options['exchange_type_folder']):
                    os.mkdir(self.options['exchange_type_folder'])
                if not os.path.exists(self.options['exchange_folder']):
                    os.mkdir(self.options['exchange_folder'])

                b64_string = self.options['datafile'].read()
                file = ContentFile(binascii.a2b_base64(b64_string))
                filepath = os.path.join(self.options['exchange_folder'], self.options['datafile'].name)
                default_storage.save(filepath, file)
                self.log("Файл успешно сохранен: {0}.".format(self.options['datafile'].name))
                return True
            elif self.options['stage'] == 'do':
                try:
                    self.log('Процесс синхронизации данных...')
                    self.log('Обработка переданных файлов обмена...')
                    result = self._join_exchange_files()
                    if result:
                        self.log('Распаковка архива...')
                        self._unpack_exchange_files()
                        self.log('Архив распакован. Обработка...')
                        result = self._handle()
                        self.log('Обработка пакетов прошла успешно. Очищение временных файлов...')
                        self._clear_folders()
                        return result
                    else:
                        self.log('При обработке файлов обмена произошла ошибка')
                except Exception as ex1:
                    self.log("Ошибка: {0}".format(str(ex1)))
                finally:
                    self.log('===================== Завершение ======================')
                    self.exchange.deactivate()
                    self.state(4)

        except Exception as ex:
            self.log("Ошибка: {0}".format(str(ex)))
            self.log('===================== Завершение ======================')
            self.exchange.deactivate()

        return False

    def _join_exchange_files(self):
        result = False

        try:
            files = sorted(list(os.scandir(self.options['exchange_folder'])), key = lambda el: el.name)
            archive_path = os.path.join(self.options['exchange_folder'], 'exchange_archive.zip')
            if len(files) > 1:
                self.log('Объединение ...')
                with open(archive_path, 'wb') as fp:
                    for file in files:
                        if not os.path.isdir(file.path):
                            fp.write(open(file.path, 'rb').read())
                            os.remove(file.path)
                self.log('Объединение успешно выполнено ...')
            else:
                os.rename(files[0].path, archive_path)
            result = True

        except Exception as ex:
            self.log(str(ex))

        return result

    def _unpack_exchange_files(self):
        zip_path = os.path.join(self.options['exchange_folder'], 'exchange_archive.zip')
        fantasy_zip = zipfile.ZipFile(zip_path)
        fantasy_zip.extractall(os.path.join(self.options['exchange_folder']))
        fantasy_zip.close()
        os.remove(zip_path)

    def _handle(self):
        result = False
        with open(os.path.join(self.options['exchange_folder'], 'data.json'), 'r') as fp:
            data = json.load(fp)
            from api.lib.util.api import call_api
            result = call_api(self.options['api_object'], 'update', params = {
                'user': self.options['user'],
                'list': data,
                'files_folder': self.options['exchange_folder']
            })
        return result

    def _clear_folders(self):
        shutil.rmtree(self.options['exchange_folder'])





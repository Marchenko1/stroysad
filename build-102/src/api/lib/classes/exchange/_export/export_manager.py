import json
import os
import time
import zipfile
from datetime import datetime
from functools import reduce

import jsonlines

from api.lib.interfaces.exchange.iexchange import IExchangeExportManager
from api.lib.util.archive import write_recursively
from api.lib.util.functions import send_1C_http_request
# from api.models import Attachment
from .._wrappers.file_packages import ExchangeFiles
from .._packages.exchange_package_manager import ExchangePackageManager
from ...events.EventManager import EventManager

try:
    import zlib

    compression = zipfile.ZIP_DEFLATED
except:
    compression = zipfile.ZIP_STORED


class ExchangeExportManager(IExchangeExportManager):
    def send_http_message(self, url, options = None):
        result = send_1C_http_request(url, options if options is not None else {}, is_exchange = True)

        if 'log' in result['data']:
            for message in result['data']['log']:
                self.log_1c(message)

        if 'errors' in result:
            for error in result['errors']:
                self.log_1c(error)

        if 'data' in result and 'errors' in result['data']:
            for error in result['data']['errors']:
                self.log_1c(error)

        return result

    def do(self):
        self.log('================ Начало обмена ================')
        steps = 4
        steps_completed = 0
        try:
            if self.exchange.history.state == 0:
                self.log('Инициализация параметров обмена')
                self._prepare_params_from_packages()
            if self.exchange.history.state == 1:
                steps_completed += 1
                self.log('Подготовка пакетов...')
                if len(self.unhandled_packages) > 0:
                    self._prepare_packages()
                    self.log('Пакеты сформированы, всего пакетов: {0}'.format(str(len(self.package_manager.fragments))))
                    self.log('Сохранение пакетов на диск и фрагментация при необходимости по {0} Мб'.format(self.options["part_size"]))
                    all_packages_saved = self._save_packages()
                    if all_packages_saved:
                        self.log('Все пакеты успешно записаны')
                        self.log('Общий размер пакетов {0}'.format(str(self.package_manager.size_verbose())))
                    else:
                        if all_packages_saved is False:
                            self.log('Не все пакеты записаны')
                        elif all_packages_saved is None:
                            self.log('Ни одного пакета не было записано')
                            self.log('Завершение обмена')
                            return
                else:
                    self.log("Нет пакетов к выгрузке")
                    self.log('================ Завершение обмена ================')
                    self.exchange.deactivate()
                    self.exchange.state(4)
                    return
            if self.exchange.history.state == 2:
                steps_completed += 1
                self.log("Начало сессии обмена данными с 1С")
                self._start_exchange_session()
            if self.exchange.history.state == 3:
                steps_completed += 1
                self.log("..... Выгрузка пакетов в 1С .....")
                self._send_fragments()
                self._unpack_fragments()
                self._delete_temp_fragment_files()
                self.log("..... Выгрузка пакетов окончена .....")
                success = self._do_exchange()
                if success:
                    self._end_exchange_session()

            if self.exchange.history.state == 4:
                steps_completed += 1
                self.log("================ Обмен завершен успешно ================")
                self.exchange.deactivate()
            if self.exchange.history.state == 5:
                self.log("Нет ответа от сервера 1С")
                self.log('================ Завершение обмена ================')
        except Exception as ex:
            self.log(str(ex))

        if steps_completed != steps:
            self.log("================ Обмен завершился с ошибкой ================")
            self.exchange.deactivate()

    def _prepare_packages(self):
        self.package_manager = ExchangePackageManager(self.options)
        self.log("Извлекаем информацию о файлах, привязанных к объектам")
        owner_ids = [unhandled_package['id'] for unhandled_package in self.unhandled_packages]


        percent = 0
        iterator = 0
        count = len(self.unhandled_packages)
        isset_500_packages = False
        while self.unhandled_packages:
            start_time = time.time()
            unhandled_package = self.unhandled_packages.pop()
            unhandled_package.set('entity', self.options['api_object'])
            exchange_files = []
            if self.options['isset_files']:
                for file_field in self.options['file_fields']:
                    if type(unhandled_package[file_field]) is list:
                        for f in unhandled_package[file_field]:
                            if str(f.id) not in exchange_files:
                                exchange_files.append(str(f.id))
                    elif unhandled_package[file_field] is not None and unhandled_package[file_field] not in exchange_files:
                        exchange_files.append(unhandled_package[file_field])
            # if self.options['api_model'] is not None:
            #     attachments = list(Attachment.objects.filter(owner_type__model = self.options['api_model']._meta.model_name, owner_id__in = owner_ids).exclude(id__in=exchange_files).values('id'))
            #     for attach in attachments:
            #         exchange_files.append(str(attach['id']))

            self.log("Всего найдено присоединенных файлов: {0}".format(len(exchange_files)))

            exchange_files = ExchangeFiles(exchange_files)

            self.package_manager.add(unhandled_package, exchange_files)

            iterator += 1
            percent = round(iterator / count * 100, 2)
            if iterator % 1000 == 0:
                isset_500_packages = True
                percent_verbosed = '{0}%'.format(round(iterator / count * 100, 2))
                self.log("Сформировано {0} пакетов из {1}. Процент выполнения: {2}".format(iterator, count, percent_verbosed))

        if not isset_500_packages:
            percent_verbosed = '{0}%'.format(round(iterator / count * 100, 2))
            self.log("Сформировано {0} пакетов из {1}. Процент выполнения: {2}".format(iterator, count, percent_verbosed))

    def _save_packages(self):
        if not os.path.exists(self.options['exchange_root_dir']):
            os.mkdir(self.options['exchange_root_dir'], 0o770)
        if not os.path.exists(self.options['exchange_type_folder']):
            os.mkdir(self.options['exchange_type_folder'], 0o770)
        if not os.path.exists(self.options['exchange_folder']):
            os.mkdir(self.options['exchange_folder'], 0o770)
        if not os.path.exists(self.options['exchange_files_dir']):
            os.mkdir(self.options['exchange_files_dir'], 0o770)

        fragments_count = len(self.package_manager.fragments)
        saved = 0

        self.options['main_jsonl'] = jsonlines.open(self.options['main_jsonl_path'], mode = 'w')

        with open(self.options['description'], 'w') as fp:
            json.dump({
                'id': self.options['id'],
                'api_object': self.options['api_object']
            }, fp)

        while self.package_manager.fragments:
            fragment = self.package_manager.fragments.pop()
            if fragment.save():
                self.options['main_jsonl'].write(fragment.information())

                with zipfile.ZipFile(fragment.zip_path, mode = 'w', compression = compression, compresslevel = 0) as fragment_archive:
                    write_recursively(zip = fragment_archive, path = fragment.folder_path, arcname = fragment.arcname)

                saved += 1
                self.package_manager.size += os.path.getsize(fragment.zip_path)

        self.options['main_jsonl'].close()

        fragments_reader = jsonlines.open(self.options['main_jsonl_path'], mode = 'r')
        for fragment in fragments_reader:
            self.fragments.append(fragment)
            self.package_manager.remove_fragment_folder(fragment)
            if fragment['in_parts']:
                self.package_manager.slice_fragment_zip(fragment)
        fragments_reader.close()

        self.state(2)

        return saved == fragments_count if saved != 0 else None

    def _start_exchange_session(self):
        result = self.send_http_message('exchange/{0}/start'.format(self.options["id"]),
                                      {
                                          'files': {
                                              'main': open(self.options['main_jsonl_path'], 'rb'),
                                              'description': open(self.options['description'], 'rb')
                                          }
                                      })

        if result['status'] == 'success':
            self.state(3)

    def _send_fragments(self):
        for fragment in self.fragments:
            def send_fragment(zip_path):
                fragment_result = self.send_http_message('exchange/{0}/fragment'.format(self.options["id"]), {'json': fragment, 'files': {'fragment': open(zip_path, 'rb')}})

            fragment_path = fragment['zip_path']
            if fragment['in_parts'] == False:
                send_fragment(fragment_path)
            else:
                # Отправляем каждую часть фрагмента
                for index in range(0, fragment['parts']):
                    send_fragment("{0}.{1}".format(fragment_path, (index + 1)))

    def _unpack_fragments(self):
        for fragment in self.fragments:
            result = self.send_http_message('exchange/{0}/unpack'.format(self.options["id"]), {
                'fragment': fragment
            })

    def _delete_temp_fragment_files(self):
        self.log("Удаление временных файлов в 1С...")
        result = self.send_http_message('exchange/{0}/clear_temp'.format(self.options["id"]))
        if result['status'] == 'success':
            self.log("Временные файлы 1С удалены")

    def _do_exchange(self):
        self.log("Обработка переданных данных")

        fragments_counter = len(self.fragments)
        success_fragments_counter = 0
        counter = 0
        for fragment in self.fragments:
            self.log("Обработка фрагмента {0}...".format(fragment['id']))
            result = self.send_http_message('exchange/{0}/do'.format(self.options["id"]), {
                'fragment': fragment
            })

            print(result)

            counter += 1
            if result['status'] == 'success':
                success_fragments_counter += 1
                continue_text = ' Обработка следующего фрагмента...' if counter != fragments_counter else ''
                self.log("Фрагмент {0} обработан успешно. {}".format(fragment['id'], continue_text))
            else:
                self.log("Фрагмент {0} не был обработан.".format(fragment['id']))

        self.log("Обработано [{0} из {1}]".format(success_fragments_counter, fragments_counter))

        if fragments_counter == success_fragments_counter:
            self.log("Все фрагменты обмена обработаны успешно.")
        elif success_fragments_counter != 0:
            self.log("Не все фрагменты обмена были успешно отработаны")
        else:
            self.log("Ни один фрагмент не был успешно отработан")

        return fragments_counter == success_fragments_counter

    def _end_exchange_session(self):
        self.state(4)
        self.exchange.deactivate()

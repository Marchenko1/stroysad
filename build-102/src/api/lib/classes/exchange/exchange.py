from django.core.files.base import ContentFile
import pickle

import os
from uuid import uuid4

from django.db.models.base import ModelBase

from api.lib.interfaces.exchange import IExchange


from ._export import ExchangeExportManager
from ._import import ExchangeImportManager
from django.conf import settings

from ...util.rights import get_service_user

class Exchange(IExchange):
    def __init__(self, **params):
        params.update(params['params'])
        del params['params']
        self._pre_initialize_options(params)
        self.exchange_manager = ExchangeExportManager(self.options, self) if self.options['is_export'] else ExchangeImportManager(self.options, self)
        self._post_initialize_options(params)

        from api.models.history.exchange import Exchange as ExchangeModel
        isset_exchange = ExchangeModel.objects.filter(exchange_id = self.options['id']).last()
        if isset_exchange is None:
            self.history = ExchangeModel(exchange_id = self.options['id'])
            self.history.save()
        else:
            self.history = isset_exchange

    def _pre_initialize_options(self, _options: dict):
        _options.setdefault('id', str(uuid4()))
        _options.setdefault('is_export', False)
        _options.setdefault('is_model_packages', False)
        _options.setdefault('is_ids_packages', False)
        _options.setdefault('user', None)
        _options.setdefault('part_size', settings.API_PART_SIZE)
        _options.setdefault('file_fields', [])
        _options.setdefault('isset_files', False)
        _options.setdefault('params', {})
        _options.setdefault('api_object', None)
        _options.setdefault('api_model', None)
        _options.setdefault('model_fields', [])
        _options.setdefault('exchange_folder_root', settings.API_EXCHANGE_ROOT)
        _options.setdefault('files_root_dir', settings.SENDFILE_ROOT)
        _options.setdefault('result', {'status': 'success'})

        self.options = _options

    def _post_initialize_options(self, _options: dict):
        _options.setdefault('exchange_root_dir', self.options['exchange_folder_root'])
        _options.setdefault('exchange_type_folder', os.path.join(_options.get('exchange_root_dir'), self.exchange_manager.folder))
        _options.setdefault('exchange_folder', os.path.join(_options.get('exchange_root_dir'), self.exchange_manager.folder, self.options['id']))
        _options.setdefault('main_jsonl', None)
        _options.setdefault('main_jsonl_path', os.path.join(self.options['exchange_folder'], 'main.jsonl'))
        _options.setdefault('description', os.path.join(self.options['exchange_folder'], 'description.json'))
        _options.setdefault('exchange_files_dir', os.path.join(self.options['exchange_folder'], 'fragments'))
        _options.setdefault('options_path', os.path.join(self.options['exchange_folder'], 'options.pickle'))

    def log(self, message: str):
        from api.models.history.exchange import ExchangeLog
        new_message = ExchangeLog(exchange_id = self.history.id, message=message)
        new_message.save()

    def state(self, state: int):
        self.history.state = state
        self.history.save(update_fields=['state'])

    def activate(self):
        self.history.is_active = True
        self.history.save(update_fields = ['is_active'])

    def deactivate(self):
        self.history.is_active = False
        self.history.save(update_fields=['is_active'])

    def do(self):
        return self.exchange_manager.do()

    @staticmethod
    def register(id_or_object, model_or_model_name = None):
        from api.models.history.exchange import ExchangeObject
        model = model_or_model_name
        obj = None
        if model_or_model_name is not None:
            from config.settings import API_ENTITIES
            if type(model_or_model_name) is str:
                model = API_ENTITIES[model_or_model_name]['model'] if model_or_model_name in API_ENTITIES and API_ENTITIES[model_or_model_name]['model'] is not None else None

            if type(id_or_object) is str or not isinstance(type(id_or_object), ModelBase):
                try:
                    obj = model.objects.get(id = id_or_object)
                except Exception as ex:
                    print(str(ex))
            elif isinstance(type(id_or_object), ModelBase):
                obj = id_or_object
        else:
            obj = id_or_object

        if obj is not None:
            if ExchangeObject.objects.filter(obj_id=obj.id).last() is None:
                new_exchange_object = ExchangeObject(obj = obj)
                new_exchange_object.save()

    @staticmethod
    def periodic():
        from api.models.history.exchange import ExchangeObject
        user = get_service_user()

        exchange_objects = ExchangeObject.objects.all()

        api_model_names = []

        for exchange_object in exchange_objects:
            if exchange_object.obj_type.model not in api_model_names:
                api_model_names.append(exchange_object.obj_type.model)

        if len(api_model_names) > 0:
            print("Отправка измененных пакетов")
            for api_model_name in api_model_names:
                object_ids = [str(item.obj_id) for item in list(filter(lambda item: item.obj_type.model == api_model_name, exchange_objects))]

                exchange = Exchange(
                    is_export = True,
                    api_object = api_model_name,
                    user = user,
                    params = {
                        'list': object_ids,
                    }
                )

                exchange.do()

                if exchange.history.state == 4 and exchange.history.is_active is False:
                    objects = [item for item in list(filter(lambda item: str(item.obj_id) in object_ids, exchange_objects))]
                    for obj in objects:
                        obj.delete()

            print("Отправка измененных пакетов выполнена")

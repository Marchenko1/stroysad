import os

from api.lib.interfaces.exchange.iexchange_fragment import IExchangeFragment


class ExchangeFragment(IExchangeFragment):
    def adding_is_possible(self, package):
        fragment_size = (self.size + package.size) / 1024 / 1024
        if fragment_size < self.options['part_size']:
            return True
        return False

    def add(self, package):
        self.packages.append(package)
        self.size += package.size

    def is_fullfilled(self, package = None):
        if package.size if package is not None else self.size / 1024 / 1024 > self.options['part_size']:
            return True
        else:
            return False

    def _save_package(self, package):
        return package.save()

    def information(self):
        return {
            'id': self.id,
            'api_object': self.options['api_object'],
            'exchange_id': self.options['id'],
            'zip_path': self.zip_path,
            'zip_name': self.zip_name,
            'folder_path': self.folder_path,
            'in_parts': self.is_fullfilled(),
            'packages_count': len(self.packages)
        }

    def save(self):
        main_jsonl = self.options['main_jsonl']
        files = self.options['exchange_files_dir']
        self.options['fragment_dir'] = os.path.join(files, self.id)

        if not os.path.exists(self.options['fragment_dir']):
            os.mkdir(self.options['fragment_dir'], 0o770)

        saved = 0
        packages_len = len(self.packages)

        for package in self.packages:
            if self._save_package(package):
                saved += 1

        return saved == packages_len

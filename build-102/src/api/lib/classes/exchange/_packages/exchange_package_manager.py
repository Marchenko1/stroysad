import math
import os
import shutil


from api.lib.classes.exchange._packages.exchange_fragment import ExchangeFragment
from api.lib.classes.exchange._packages.exchange_package import ExchangePackage
from api.lib.interfaces.exchange.iexchange_package_manager import IExchangePackageManager


class ExchangePackageManager(IExchangePackageManager):
    def __init__(self, options):
        super(ExchangePackageManager, self).__init__(options)
        self.current_fragment = ExchangeFragment(options)

    def size_verbose(self):
        size = int(self.size / 1024)
        if size == 0:
            return '{0} б'.format(self.size)
        size = int(size / 1024)
        if size == 0:
            return '{0} Кб'.format(str(int(self.size / 1024)))
        size = int(size / 1024)
        if size == 0:
            return '{0} Мб'.format(str(int(self.size / 1024 / 1024)))
        else:
            return '{0} Гб'.format(str(int(self.size)))

    def remove_fragment_folder(self, fragment_information):
        if os.path.exists(fragment_information['folder_path']):
            shutil.rmtree(fragment_information['folder_path'])

    def slice_fragment_zip(self, fragment_information):
        archive_size = os.path.getsize(fragment_information['zip_path'])
        archive_name = fragment_information['zip_name']
        one_part_size = self.options['part_size'] * 1024 * 1024
        parts_count = math.ceil(archive_size / one_part_size)
        fragment_information['parts'] = parts_count
        iterator = 1

        with open(fragment_information['zip_path'], 'rb') as zip_file_fp:
            while parts_count > 0:
                with open("{0}.{1}".format(fragment_information['zip_path'], iterator), 'wb') as zip_part_fp:
                    part_bytes = zip_file_fp.read(one_part_size)
                    zip_part_fp.write(part_bytes)
                iterator += 1
                parts_count -= 1

        os.remove(fragment_information['zip_path'])

    def add(self, package, exchange_files):
        exchange_package = ExchangePackage(package, exchange_files, self.current_fragment)
        if self.current_fragment.adding_is_possible(exchange_package):
            if len(self.fragments) == 0:
                self.fragments.append(self.current_fragment)
            self.current_fragment.add(exchange_package)
        else:
            self.current_fragment.add(exchange_package)
            self.fragments.append(self.current_fragment)
            self.current_fragment = ExchangeFragment(self.options)


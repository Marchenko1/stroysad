import json
import math
import os

from api.lib.interfaces.exchange.iexchange_package import IExchangePackage


class ExchangePackage(IExchangePackage):
    def information(self):
        return {
            'id': self.id
        }

    def is_fullfilled(self):
        package_size = self.size - self.files.size()
        return package_size / 1024 / 1024 < self.fragment.options['part_size']

    def slice_package(self, package_or_file_list):
        fragments = []
        for package_or_file in package_or_file_list:
            is_package = type(package_or_file) is dict
            if is_package:
                # Конвертируем пакет dict в json и в bytearray
                package_in_bytes = bytearray(json.dumps(package_or_file, ensure_ascii = False).encode('utf-8'))
            else:
                package_in_bytes = None
            # Максимальный размер одного пакета
            fragment_size = self.fragment.options['part_size'] * 1024 * 1024
            # Размер пакета без файлов
            package_size = (self.size - self.files.size()) if is_package else package_or_file.size()

            # Количество фрагментов на которое нужно разделить переполненный пакет
            fragments_count = math.ceil((package_size / 1024 / 1024) / self.fragment.options['part_size'])

            package_dir = os.path.join(self.fragment.options['exchange_folder'], self.fragment.options['fragment_dir'])

            if not os.path.exists(package_dir):
                os.mkdir(package_dir, 0o770)

            package_dir = os.path.join(package_dir, self.id)

            if not os.path.exists(package_dir):
                os.mkdir(package_dir, 0o770)

            if not is_package:
                package_dir = os.path.join(package_dir, 'files')

                if not os.path.exists(package_dir):
                    os.mkdir(package_dir, 0o770)

            if is_package:
                name = '{0}.json'.format(self.id)
            else:
                name = package_or_file.name

            fragment_name = os.path.join(package_dir, name)

            fragment = {
                'filename': fragment_name, 'bytes': bytes(package_in_bytes) if is_package else package_or_file.read()
            }

            fragments.append(fragment)
        saved = 0
        fragments_len = len(fragments)
        for fragment in fragments:
            with open(fragment['filename'], 'wb') as fragment_fp:
                fragment_fp.write(fragment['bytes'])
            saved += 1

        return saved == fragments_len

    def save(self):
        packing_packages = [self.package]
        for file in self.files:
            packing_packages.append(file)

        return self.slice_package(packing_packages)

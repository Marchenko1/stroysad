from rest_framework.request import Request

from api.lib.classes.exceptions import APICallException, verbose_errors
from api.lib.classes.methods import ReadMethod, WriteMethod, ReadMetaMethod
from api.lib.classes.request import APIRequest
from api.lib.classes.response import APIResponse



class ApiHandler:
    '''
    Класс, выполняющий промежуточную функцию обработки запроса к API.
    Он хранит все необходимые параметры, вызывает необходимые методы обработчиков API.
    По умолчанию есть 3 метода, которые обрабатывают запросы API - fetch (list), update, meta. API с этими методами можно внедрить в любой django проект (совместимой версии)
    Все остальные методы (кастомные, зависимые от логики обособленных модулей [приложений django]) обрабатываются кастомным обработчиком (self.api_handler)
    '''

    def __init__(self, entity_name: str, method_name: str, request: Request = None, params: dict = None):
        '''
        :param entity_name: Название вызываемой сущности или части пути к сервисным вызовам APi
        :param method_name: Имя вызываемой функции
        :param request: HTTP запрос Rest framework
        :param params: дополнительные параметры вызова API
        '''
        # Храним все переданные аргументы в одном месте и передаем их в последующем непосредственно в класс обработчик запроса
        self.args = {
            'entity_name': entity_name,
            'method_name': method_name,
            'request': request,
            'params': params
        }
        # Свойство, хранящее параметры обработки вызова
        self.params = {}
        # Класс обработчик кастомных функций
        self.api_handler = None
        # Ошибки выполнения
        self.errors = []
        # Варнинги
        self.warnings = []
        self.log = []

    def is_possible(self):
        '''
        Возможность вызова метода API. Ищет сущность в существующих моделях БД и метод, который можно вызвать по отношению к найденной модели
        :return: bool
        '''
        possible = False
        from django.conf import settings
        if not self.args['entity_name'] in settings.API_ENTITIES:
            self.log.append("{0} не найден в списке сущностей обмена".format(self.args['entity_name']))
            self.errors.append(APICallException('Сущность API не найдена'))
        else:
            self.args['model'] = settings.API_ENTITIES[self.args['entity_name']]['model']
            if 'api_handler' in settings.API_ENTITIES[self.args['entity_name']] and settings.API_ENTITIES[self.args['entity_name']]['api_handler'] is not None:
                self.api_handler = settings.API_ENTITIES[self.args['entity_name']]['api_handler'](self.args['model'], self.params, self.errors, self.warnings)
                self.log.append('Обработчик найден')
            possible = True
        return possible

    def execute(self):
        '''
        Фабричный метод. Здесь происходит выбор класса, который будет исполнять запрос и делегирование ему валидацию и выполнение запроса
        :return: APIResponse | None if valid False
        '''
        method = None
        custom_method = None
        if self.args['model'] is not None and self.args['method_name'] == 'list':
            method = ReadMethod()
            self.log.append('Это чтение данных')
            self.log.append('Обработчик чтения данных')
        elif self.args['model'] is not None and self.args['method_name'] == 'update':
            method = WriteMethod()
            self.log.append('Обработчик записи данных')
        elif self.args['model'] is not None and self.args['method_name'] == 'meta':
            method = ReadMetaMethod()
            self.log.append('Обработчик чтения метаданных')
        if self.api_handler is not None:
            if hasattr(self.api_handler, self.args['method_name']):
                self.log.append('Это не стандартный обработчик CRUD')
                custom_method = getattr(self.api_handler, self.args['method_name'])
                self.log.append('Вызываемая функция: {0}'.format(str(custom_method)))

        method.log = self.log

        result = None
        if method is not None or custom_method is not None:
            def execute(func_method, api_default_result = None):
                if api_default_result is not None:
                    self.params['default_result'] = api_default_result
                api_request = APIRequest(self.args, self.params, self.errors, self.warnings, func_method, self.api_handler)
                self.log.append('Начинается обработка данных')
                if api_request.validate():
                    result = APIResponse(api_request.execute())
                else:
                    result = APIResponse({'status': 'error', 'errors': verbose_errors(api_request.errors) + api_request.result['errors']})
                self.log.append('Обработка данных завершена. Обмен завершен.')
                return result
            if method is not None:
                result = execute(method)
            if custom_method is not None:
                result = execute(custom_method, result)
        else:
            result = APIResponse({'status': 'error', 'errors': verbose_errors([APICallException('Метод не найден')])})

        return result
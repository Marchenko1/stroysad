import json

from django.contrib.auth import get_user_model
from django.core.handlers.wsgi import WSGIRequest
from django.http import QueryDict
from rest_framework.request import Request

from api.lib.classes.exceptions import APIParamsException
from api.lib.classes.validator.base import APIValidator
from api.lib.util.rights import get_service_user
# from config.custom_options import SERVICE_USER_WITH_FULL_RIGHTS

User = get_user_model()


class FileValidator(APIValidator):
    def collect_params_to_dict(self):
        # Собираем все переданные параметры HTTP или dict в один словарь
        if type(self.args) is not dict:
            self.errors.append(APIParamsException('Ошибка переданных параметров: params не является типом dict'))
            return

        if 'params' not in self.args:
            self.args['params'] = self.params

        if 'request' not in self.args or self.args.get('request', None) is None:
            if not 'user' in self.args['params']:
                self.errors.append(APIParamsException('В запрос API не был передан пользователь - user обязательный параметр'))
            else:
                if not isinstance(self.args['params']['user'], User):
                    if self.args['params']['user'] == None:
                        self.errors.append(APIParamsException('В запрос API был передан параметр user с невалидным типом пользователя'))
                    else:
                        self.args['params']['user'] = get_service_user()

        if len(self.errors) == 0:

            if ('request' in self.args and self.args['request'] is not None) or ('request' in self.args['params'] and self.args['params']['request'] is not None):
                request = self.args.get('request') if self.args.get('request') is not None else self.args['params'].get('request')
                request_params = { 'request': request }
                # Rest framework request data
                if type(request) is Request:
                    request_params.update(request.data if type(request.data) is dict or not type(request.data) is QueryDict else request.data.dict())
                else:
                    if type(request is WSGIRequest) and request.content_type == 'multipart/form-data':
                        methods = ['FILES', 'GET', 'POST']
                        for method in methods:
                            if hasattr(getattr(request, method), 'dict'):
                                request_params.update(getattr(request, method).dict())
                    elif type(request is WSGIRequest):
                        body = request.body.decode()
                        if body != '':
                            try:
                                request_params = json.loads(body)
                            except Exception as ex:
                                pass
                if len(request_params.keys()) > 0:
                    self.params.update(request_params)

                self.params['user'] = request.user
            else:
                if 'params' not in self.args:
                    self.errors.append(APIParamsException('В запрос API не были переданы параметры (params)'))

            if 'params' in self.args:
                if self.args['params'] is not None:
                    self.params.update(self.args['params'])
                del self.args['params']
            if 'request' in self.args:
                del self.args['request']

            self.params.update(self.args)

    def validate_rights(self):
        pass

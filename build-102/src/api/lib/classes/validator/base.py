'''
    Класс валидатор входящих параметров и прав доступа пользователя
'''
import json

from django.contrib.auth import get_user_model
from django.http import QueryDict
from rest_framework.request import Request

from api.lib.classes.exceptions import APIParamsException, APIAccessException, APIProgramException
from api.lib.util.rights import get_service_user

User = get_user_model()

class APIValidator:
    __slots__ = ('args', 'params', 'errors', 'warnings', 'model')

    def validate(self):
        # TODO: сделать создание и удаление прав от используемых сущностей API_ENTITIES аналогично проверке self.validate_rights()
        self.collect_params_to_dict()
        self.validate_rights()
        return self.params

    def collect_params_to_dict(self):
        # Собираем все переданные параметры HTTP или dict в один словарь
        if 'params' not in self.args:
            '''
                Если свойства params нет в списке ключей self.args -
                    то сюда был передан уже готовый набор self.params и его обрабатывать не нужно
            '''
            return
        if type(self.args) is not dict:
            self.errors.append(APIParamsException('Ошибка переданных параметров: params не является типом dict'))
            return

        if 'request' not in self.args or self.args.get('request', None) is None:
            if not 'user' in self.args['params']:
                self.errors.append(APIParamsException('В запрос API не был передан пользователь - user обязательный параметр'))
            else:
                if hasattr(self.args['params']['user'], '_meta') and hasattr(self.args['params']['user']._meta, 'model'):
                    if self.args['params']['user']._meta.model is not User:
                        self.errors.append(APIParamsException('В запрос API был передан параметр user с невалидным типом пользователя'))
                else:
                    if self.args['params']['user'] == None:
                        self.errors.append(APIParamsException('В запрос API был передан параметр user с невалидным типом пользователя'))
                    else:
                        self.args['params']['user'] = get_service_user()

        if len(self.errors) == 0:
            self.params.update(self.args)
            if 'request' in self.args and self.args['request'] is not None:
                if 'request' in self.args:
                    request_params = {}
                    # Rest framework request data
                    if type(self.args['request']) is Request:
                        request_params = self.args['request'].data \
                            if type(self.args['request'].data) is dict or not type(self.args['request'].data) is QueryDict \
                            else self.args['request'].data.dict()
                    else:
                        body = self.args['request'].body.decode()
                        if body != '':
                            try:
                                request_params = json.loads(body)
                            except Exception as ex:
                                pass
                    if len(request_params.keys()) > 0:
                        self.params.update(request_params)

                    self.params['user'] = self.args['request'].user
            else:
                if 'params' not in self.args:
                    self.errors.append(APIParamsException('В запрос API не были переданы параметры (params)'))


            if 'params' in self.args:
                if self.args['params'] is not None:
                    self.params.update(self.args['params'])
                del self.args['params']



    def validate_rights(self):
        if len(self.errors) == 0:
            self.validate_model()

    def validate_model(self):
        approved = True

        return approved
from django.db.models import ForeignKey

# from api.models import Attachment
from api.settings import FIELD_TYPE_MATCHES

class FieldsDetalization:
    with_foreign = 2
    with_m2m = 3

class APIFields:
    @classmethod
    def can_filtered(cls, Model, field):
        if hasattr(Model, 'filtered_fields'):
            if field.name in Model.filtered_fields:
                return True
        return False

    @classmethod
    def get_field_properties(cls, field, prefix=None, field_type=None):
        # TODO: нужно переделать

        t = None
        if field_type is not None:
            t = {
                'type': field_type['type'],
                'can_filtered': cls.can_filtered(field.model, field)
            }
            if type(field) == ForeignKey:
                t['entity_name'] = field_type['entity_name']
                t['verbose_related'] = field.related_model.verbose_related if hasattr(field.related_model, 'verbose_related') else None

        if t is None:
            t = {'type': 'str', 'can_filtered': cls.can_filtered(field.model, field)}

        field_properties = {
            'verbose': str(field.verbose_name.capitalize()) if field.verbose_name is not None else field.name.capitalize(),
            'name': field.name if prefix is None else prefix + field.name,
            'meta': t
        }

        if prefix is not None:
            field_properties['model'] = {
                'name': field.model._meta.model_name,
                'verbose': field.model._meta.verbose_name_plural.capitalize()
            }

        return field_properties

    @classmethod
    def get_all_model_fields(cls, model, detalization: list = None, for_filter=False):
        '''
        :param model: Модель Django
        :param detalization: уровень детализации получаемых полей модели [FieldsDetalization.self, FieldsDetalization.with_foreign, FieldsDetalization.with_m2m]
        :param for_filter: выборка метаданных полей для фильтров или метода API list. Если for_filter == True, значит это выборка полей для последующей конкатенации с префиксом __,
        чтобы положить связанные поля в .values(**fields) и объединить в дальнейшем набор из связанных полей
        :return: Массив (list) всех полей модели
        TODO: здесь должны учитываться дополнительные свойства сущности
        TODO: здесь должны формировать фильтры (дополнительные свойства тоже могут быть фильтрами)
        '''
        model_fields = model._meta.fields
        if detalization is None:
            detalization = []

        # # По умолчанию выбираем все связанные 1-M
        # if FieldsDetalization.with_foreign not in detalization:
        #     detalization.append(FieldsDetalization.with_foreign)

        # Если уровень детализации должен включать m2m связи
        if FieldsDetalization.with_m2m in detalization:
            model_fields_many_to_many = model._meta.many_to_many

        model_fields = list(model_fields)
        all_fields = []

        def convert_fields(fields, many=False, prefix=None, sub_fields=False):
            '''
            :param fields: Список полей модели
            :param many: это список m2m полей
            :param prefix: постфикс __<field> для выборки связей
            :param sub_fields: это конвертация полей m2m или верхнего уровня иерархии для того чтобы проверить detalization только целевой модели
            :return:
            '''
            fields_list = []

            for field in fields:
                if sub_fields == True:
                    if hasattr(field.model, 'forbidden_fields'):
                        do_not_show = field.model.forbidden_fields()
                        if field.name in do_not_show:
                            continue
                else:
                    if hasattr(model, 'forbidden_fields'):
                        do_not_show = model.forbidden_fields()
                        if field.name in do_not_show:
                            continue

                if many == True:
                    if for_filter:
                        fields_list += convert_fields(field.related_model._meta.fields, prefix=field.name + '__', sub_fields=True)
                    else:
                        # TODO переделать участок с метаданными полей m2m и one2m, данные поля должны выбираться из cls.get_field_properties
                        m2m_field = {
                            'meta': {
                                'type': 'list',
                                'can_filtered': cls.can_filtered(field.model, field)
                            },
                            'name': field.name,
                            'verbose': str(field.verbose_name.capitalize()) if field.verbose_name is not None else field.name.capitalize(),
                            'sub_fields': convert_fields(field.related_model._meta.fields, sub_fields=True)
                        }
                        fields_list.append(m2m_field)
                else:
                    type_filter = lambda f: f['type_class'] == type(field) or field.related_model == f['type_class']
                    field_type = next(filter(type_filter, FIELD_TYPE_MATCHES), None)
                    if type(field) is ForeignKey and FieldsDetalization.with_foreign not in detalization and sub_fields == False:
                        continue
                    elif type(field) is ForeignKey and FieldsDetalization.with_foreign in detalization and sub_fields == False:
                        if for_filter:
                            # if field.related_model is Attachment:
                            #     fields_list.append(cls.get_field_properties(field, prefix = prefix, field_type = field_type))
                            # else:
                            fields_list += convert_fields(field.related_model._meta.fields, prefix=field.name + '__', sub_fields=True)
                        else:
                            one2m_field = {
                                'meta': {
                                    'type': 'related_entity',
                                    'entity_name': field.related_model._meta.model_name,
                                    'can_filtered': cls.can_filtered(field.model, field)
                                },
                                'name': field.name,
                                'verbose': str(field.verbose_name.capitalize()) if field.verbose_name is not None else field.name.capitalize(),
                                'sub_fields': convert_fields(field.related_model._meta.fields, sub_fields=True),
                                'verbose_related': field.related_model.verbose_related if hasattr(field.related_model, 'verbose_related') else None
                            }
                            fields_list.append(one2m_field)
                    else:
                        fields_list.append(cls.get_field_properties(field, prefix=prefix, field_type=field_type))

            return fields_list

        all_fields += convert_fields(model_fields)

        # Если уровень детализации должен включать m2m связи
        if FieldsDetalization.with_m2m in detalization:
            all_fields += convert_fields(model_fields_many_to_many, True)

        return all_fields

    @classmethod
    def get_all_fields(cls, model, many = False):
        '''
        :param model: Модель Django
        :return: Массив (list) всех полей модели
        TODO: здесь должны учитываться дополнительные свойства сущности
        TODO: здесь должны формировать фильтры (дополнительные свойства тоже могут быть фильтрами)
        '''
        model_fields = model._meta.fields
        model_fields_many_to_many = model._meta.many_to_many
        model_fields = list(model_fields)
        all_fields = []


        def convert_fields(fields, many = False, prefix = None):
            fields_list = []

            for field in fields:
                if many == True:
                    fields_list += convert_fields(field.related_model._meta.fields, False, field.name + '__')
                else:
                    def loop(f):
                        t = None
                        for type_match in FIELD_TYPE_MATCHES:
                            if type(f) == type_match['type_class']:
                                t = {'type': type_match['type']}
                            elif type(f) == ForeignKey and 'type_class' in type_match and f.related_model == type_match['type_class']:
                                t = {
                                    'type': type_match['type'],
                                    'entity_name': type_match['entity_name']
                                }
                        return t

                    field_type = loop(field)
                    if field_type == None:
                        field_type = {'type': 'str'}

                    field_properties = {
                        'verbose': str(field.verbose_name.capitalize()) if field.verbose_name is not None else '-',
                        'name': field.name if prefix is None else prefix + field.name,
                        'meta': field_type
                    }

                    if prefix is not None:
                        field_properties['model'] = {
                            'name': field.model._meta.model_name,
                            'verbose': field.model._meta.verbose_name_plural.capitalize()
                        }
                    fields_list.append(field_properties)

            return fields_list
        all_fields += convert_fields(model_fields)
        if many:
            all_fields += convert_fields(model_fields_many_to_many, True)

        return all_fields
from api.lib.classes.events.EventManager import EventManager
from api.lib.decorators import api_handler
from api.lib.interfaces.entity.ientity import IEntity

class ApiEvent(IEntity):
    '''
    Класс отклика на открытую вакансию.
    '''

    @api_handler
    def list(self, *args, **kwargs):
        events = set(filter(lambda celery_task: celery_task.event.template_generate_is_possible == True, EventManager.get_event_sequences()))
        self.result['events'] = {celery_task.event.name: celery_task.event.template_variables.get(celery_task.event.name, {}) for celery_task in events}
        return self

    @staticmethod
    def rights():
        '''
        :return: dict - права, которые необходимые пользовтаелю для вызова метода API для конкретного объекта.
        Формат прав: { '<Метод>': [ {'name': '<Код прав доступа: can_view_order>', 'verbose': '<Название прав доступа, отображаемое в интерфейсе пользователя: Чтение заказов>'} ] }
        '''
        return {
            'list': [
                {'name': 'view_event', 'verbose': 'Чтение событий'}
            ]
        }


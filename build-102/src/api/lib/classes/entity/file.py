import re

from django.http import HttpResponse

from api.lib.classes.events.EventManager import EventManager
from api.lib.decorators import api_handler
from api.lib.interfaces.entity.ientity import IEntity
from django.conf import settings
import logging
logger = logging.getLogger(__name__)


class ApiFile(IEntity):
    '''
    Класс отклика на открытую вакансию.
    '''

    @api_handler
    def profile_image(self, *args, **kwargs):
        self.result = 'error'
        if 'area_name' in self.params:
            attach_type = self.params['area_name'] if self.params['area_name'] != 'avatarsm' else 'avatar'
            isset_attach = self.model.objects.filter(owner_id=self.params['user'].id, type=attach_type, active=False).last()
            record_mode = 'wa' if isset_attach is None else 'we'
            has_attach = False
            extension = None
            if isset_attach is None:
                attach = self.model()
            else:
                attach = isset_attach
                has_attach = True

            if not attach.have_access(record_mode, user = self.params['user']):
                self.result = HttpResponse('Доступ запрещен', status = 403)
                return self

            attach.owner = self.params['user']
            attach.file = self.params['file']
            attach.type = attach_type
            attach.name = attach.file.name
            attach.ext = attach.extension()
            try:
                if has_attach:
                    attach.save(update_fields = ['file', 'type', 'ext', 'name'])
                else:
                    attach.save()

                self.result = 'success'
            except Exception as ex:
                logger.error(str(ex))
        self.result = HttpResponse(self.result)
        return self

    @api_handler
    def get(self, *args, **kwargs):
        from libs.sendfile import sendfile
        id = re.search(r'/api/files/(.*)/get/', self.params['request'].path).group(1)
        access = self.model.have_access('r', request = self.params['request'], id=id)
        if access:
            self.result = sendfile(self.params['request'], self.model.objects.get(id=id).file.path, attachment = True)
        else: self.result = HttpResponse(status = 403)
        return self

    @staticmethod
    def rights():
        '''
        :return: dict - права, которые необходимые пользовтаелю для вызова метода API для конкретного объекта.
        Формат прав: { '<Метод>': [ {'name': '<Код прав доступа: can_view_order>', 'verbose': '<Название прав доступа, отображаемое в интерфейсе пользователя: Чтение заказов>'} ] }
        '''
        return {
            'profile_image': []
        }


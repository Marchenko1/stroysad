import math

class Paginator:
    def __init__(self, entities, per_page):
        self.errors = []
        self.entities = entities
        self.per_page = per_page
        self.result = None
        try:
            self.count = entities.count() if type(entities) is not list else len(entities)
            self.num_pages = math.ceil((self.count / self.per_page) if self.count > self.per_page else 1)
        except Exception as e:
            self.errors.append(str(e))

    def get_page(self, page):
        self.result = { }
        page = page
        if self.num_pages > 1:
            if len(self.entities) > (page * self.per_page) - self.per_page:
                self.entities = self.entities[(page * self.per_page) - self.per_page:page * self.per_page]
            self.result["page"] = page
        else:
            self.result["page"] = 1

        self.result['data'] = self.entities
        self.result["pages"] = self.num_pages
        self.result["entities_total"] = self.count
        self.result["entities_on_page"] = self.per_page
        return self.result

    def get_offsets(self, page):
        return (page * self.per_page) - self.per_page, page * self.per_page
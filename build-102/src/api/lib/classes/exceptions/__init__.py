class APIException:
    def __init__(self, message):
        self.message = message


class APIProgramException(APIException):
    description = 'Внезапная ошибка в коде'
    message = None
    error_code = 0


class APIParamsException(APIException):
    description = 'Ошибка входящих параметров'
    message = None
    error_code = 1


class APIAccessException(APIException):
    description = 'Ошибка доступа'
    message = None
    error_code = 2


class APICallException(APIException):
    description = 'Ошибка вызова метода API'
    message = None
    error_code = 3


class APIHttpEventException(APIException):
    description = 'Ошибка вызова интернет сервисов по HTTP'
    message = None
    error_code = 4


def verbose_errors(errors):
    return [
        {
            'code': error.error_code,
            'description': error.description,
            'error_text': error.message
        }
        for error in errors
    ]
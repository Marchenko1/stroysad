from api.lib.interfaces.events.imail_event import IMailEvent


class MailEvent(IMailEvent):
    def __init__(self, recipients = None, *args, **kwargs):
        super(MailEvent, self).__init__(*args, **kwargs)
        self.recipients = recipients

    def collect_recipients(self):
        pass
    def emit(self, *args, **kwargs):
        pass
import json

from api.lib.classes.exceptions import verbose_errors, APIException
from api.lib.interfaces.events.ievent import IEvent
from api.models.history.http_events import HttpEventHistory


class HttpEventResponseRetainer(IEvent):
    def do(self, http_result: dict):
        code = http_result.pop('code')
        if 'not_available' in http_result and http_result['not_available'] == True:
            code = 0
        request = http_result.pop('params')

        if 'errors' in http_result:
            http_result['errors'] = verbose_errors(http_result['errors']) if len(http_result['errors']) > 0 and isinstance(http_result['errors'][0], APIException)  else http_result['errors']

        try:
            dump_request = json.dumps(request, ensure_ascii = False)
            dump_result = json.dumps(http_result, ensure_ascii = False)

            new_history = HttpEventHistory(http_code = code, request = dump_request, result = dump_result, is_json = http_result['is_json'])
            new_history.save()
        except Exception as ex:
            dump_request = str(request)
            dump_result = str(http_result)



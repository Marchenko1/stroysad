import json
import uuid

import requests
import urllib3
from requests.auth import HTTPBasicAuth

from api.lib.classes.events.decorators.http_event_result import http_event_result
from api.lib.interfaces.events.ihttp_event import IHttpEvent
Exchange1C_BaseURL = None
Exchange1C_AuthUser = None
Exchange1C_AuthPassword = None


class SendingHttpEvent(IHttpEvent):
    """ Класс для отправки пакетов с событиями в сторонние API """

    @http_event_result
    def emit(self, options):
        event = options['event']
        packages = options['packages'] if 'packages' in options else {}

        if packages is not None and type(packages) is dict:
            urllib3.disable_warnings()
            authorization = HTTPBasicAuth(Exchange1C_AuthUser.encode('UTF-8'), Exchange1C_AuthPassword)
            result = None
            try:
                headers = {'WWW-Authenticate': 'Basic'}
                if 'files' not in packages:
                    headers['Content-Type'] = 'application/json'
                    result = requests.post('{0}/{1}/'.format(Exchange1C_BaseURL, event), json = packages, verify = False, headers = headers, auth = authorization)
                else:
                    result = requests.post('{0}/{1}/'.format(Exchange1C_BaseURL, event), files = packages['files'], data = packages['json'] if 'json' in packages else {}, verify = False, headers = headers, auth = authorization)

            except Exception as ex:
                result = str(ex)
            return result



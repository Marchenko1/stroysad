from api.lib.classes.exceptions import verbose_errors

'''
    Экзекутор и фиксатор событий
    ┈┈┈┈┈┈┈┈┈┈┈┈☆┈┈┈┈┈┈┈┈┈┈┈┈┈┈ 
    ┈┈┈┈┈┈┈┈┈┈┈╱┊╲┈┈┈┈┈┈┈┈┈┈┈┈┈┈ 
    ┈┈┈┈┈┈┈┈┈┈╱┊┊┊╲┈┈┈┈┈┈┈┈┈┈┈┈┈ 
    ┈┈┈┈┈┈┈┈┈╱┊┊┊┊┊╲┈┈┈┈┈┈┈┈┈┈┈┈ 
    ┈┈┈┈┈┈┈┈┈▔▔▔▉▔▔▔┈┈┈┈┈┈┈┈┈┈┈┈ 
'''

def event_retainer(celery_task, options, need_async):
    #
    event_result = None
    if need_async:
        celery_task.delay(options)
    else:
        event_result = celery_task(options)

    if type(event_result) is not bool:
        if type(event_result) is not dict or event_result is None:
            event_result = {'status': 'error' if type(event_result) is bool and event_result == False else 'success', 'data': event_result if type(event_result) is not bool else {}}

        if 'errors' in event_result and len(event_result['errors']) > 0:
            event_result['status'] = 'error'

    return event_result

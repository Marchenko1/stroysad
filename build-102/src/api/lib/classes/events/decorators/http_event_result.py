import json

from requests import Response

from api.lib.classes.events.EventManager import EventManager
from api.lib.classes.exceptions import APIHttpEventException


def http_event_result(func):
    def wrapper(*args, **kwargs):
        # TODO: сделать запись истории об отправке запросов
        event_result = {'status': 'success', 'is_json': False}
        response_code = 200
        available_codes = [200, 500, 400]
        response = None
        error = ''
        is_attempt = False
        history_id = None

        if 'is_attempt' in args[1]['packages']:
            is_attempt = True
            history_id = args[1]['packages']['is_attempt']
            del args[1]['packages']['is_attempt']
        try:
            http_response = func(*args, **kwargs)
            try:
                if type(http_response) is Response:

                    response = json.loads(http_response.text)

                    event_result['status'] = response.get('status', 'error')
                    event_result['is_json'] = True
                else:
                    response_code = 0
                    error = http_response
            except Exception as json_ex:
                response_code = 500
                response = http_response.text
                error = http_response.text
        except Exception as ex:
            error = str(ex)
            response_code = 0

        if response_code != 200:
            event_result['status'] = 'error'
            event_result['errors'] = [error]
            if http_response != error and ((hasattr(http_response, 'code') and http_response.code not in available_codes) or (hasattr(http_response, 'status_code') and http_response.status_code not in available_codes)):
                event_result['not_available'] = True
            else:
                event_result['not_available'] = False

        event_result['code'] = response_code
        event_result['data'] = response
        event_result['params'] = args[1]

        if not is_attempt or (is_attempt and response_code == 200 and event_result['status'] == 'success'):
            if is_attempt:
                from api.models.history.http_events import HttpEventHistory
                HttpEventHistory.objects.get(id=history_id).delete()
            EventManager.emit('onHttpEventResponse', event_result, is_async = False, celery_task_name = 'on-http-event-response')

        return event_result

    return wrapper

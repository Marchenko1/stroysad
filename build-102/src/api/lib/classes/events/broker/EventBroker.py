from api.lib.interfaces.events.ibroker import IEventBroker


class EventBroker(IEventBroker):
    def do(self, options = None):
        # Всегда dict
        intermediate_result = None
        for event in self.events:
            if options is not None and intermediate_result is not None:
                options.update(intermediate_result)
            intermediate_result = event.emit(options)
from api.lib.classes.events.EventManager import EventManager
from api.lib.classes.events.decorators import event_retainer
from api.lib.interfaces.events.ientity_event import IEntityEvent
from api.lib.interfaces.events.igenerateable_template_event import IGenerateableTemplateEvent


class EntityEvent(IEntityEvent, IGenerateableTemplateEvent):
    def __init__(self, *args, **kwargs):
        super(EntityEvent, self).__init__(*args, **kwargs)
        self.template_variables = {
            'onEntityPreSave': {
                'verbose': 'Перед сохранением объекта',
                'vars': {
                    '#SUCCESS#': 'bool'
                }
            },
            'onEntitySave': {
                'verbose': 'После сохранения объекта',
                'vars': 'instance'
            },
            'onEntityDelete': {
                'verbose': 'Удаление объекта',
                'vars': 'instance'
            },
        }

    def generate(func):
        def wrapper(self, options, *args, **kwargs):
            result = func(self, options, *args, **kwargs)
            return result
        return wrapper

    def pre_save_emitter(self, sender, update_fields):
        return True

    def post_save_emitter(self, sender, instance_id, created, update_fields):
        return True

    def pre_delete_emitter(self, sender, instance_id, instance):
        return True

    @generate
    def do(self, options):
        if options['type'] == 'pre_save':
            return self.pre_save_emitter(options['sender'], options['update_fields'])
        elif options['type'] == 'post_save':
            return self.post_save_emitter(options['sender'], options['instance_id'], options['created'], options['update_fields'])
        elif options['type'] == 'pre_delete':
            return self.pre_delete_emitter(options['sender'], options['instance_id'], options['instance'])

    @staticmethod
    def pre_save(sender, instance, **kwargs):
        update_fields = kwargs.get('update_fields', [])
        if update_fields is None:
            update_fields = []
        elif type(update_fields) is frozenset:
            update_fields = list(update_fields)

        EventManager.emit('CRUD', {'type': 'pre_save', 'sender': sender._meta.model_name, 'update_fields': update_fields}, is_async = False)
        # pre_save(*args, **kwargs)

    @staticmethod
    def post_save(sender, instance, created, *args, **kwargs):
        update_fields = kwargs.get('update_fields', [])
        if update_fields is None:
            update_fields = []
        elif type(update_fields) is frozenset:
            update_fields = list(update_fields)

        EventManager.emit('CRUD', {'type': 'post_save', 'sender': sender._meta.model_name, 'instance_id': str(instance.id), 'created': created, 'update_fields': update_fields}, is_async = False)

    @staticmethod
    def pre_delete(sender, instance, using, **kwargs):
        from api.lib.util.api import call_api
        EventManager.emit('CRUD', {'type': 'pre_delete', 'sender': sender._meta.model_name, 'instance_id': str(instance.id), 'instance': call_api(sender._meta.model_name, 'list', params = {'user': None})}, is_async = False)
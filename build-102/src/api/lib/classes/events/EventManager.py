from api.lib.classes.events.decorators import event_retainer
from api.lib.classes.events.subscribes import EventSequences
from api.lib.interfaces.events.ibroker import IEventBroker
from api.lib.interfaces.events.ievent import IEvent
from api.lib.interfaces.events.ievent_manager import IEventManager
from project.celery_proxy import CeleryTaskProxy, app as celery_app

celery_app.conf.update(
    task_serializer='pickle',
    result_serializer='pickle',
    event_serializer='pickle',
    accept_content=['pickle'],
)


class EventManager(IEventManager):
    @staticmethod
    def _need_async(celery_task, is_async, goal_celery_task_name):
        result_async = True
        if celery_task.name == goal_celery_task_name and is_async == False or (is_async == False and goal_celery_task_name is None):
            result_async = False
        return result_async

    @staticmethod
    def _collect_result(result, intermediate_result):
        if result is None:
            result = intermediate_result
        elif type(result) is not set:
            result = set(result)
            result.add(intermediate_result)
        elif type(result) is set:
            result.add(intermediate_result)
        return result

    @staticmethod
    def emit(event_name, options = None, is_async = True, celery_task_name = None):
        '''
        Метод ищет подписанные на event_name события, вызывает их, передавая параметры с помощью options.
        Если таких событий не найдено, но есть EventBroker, значит предполагается, что в него уже были добавлены необходимые обработчики событий (IEvent) и следует вызвать его обработчик.
        :param event_name: название события
        :param options: параметры передаваемые в обработчик событий
        :param is_async: отправлять в асинхронное выполнение или ждать результат
        :param celery_task_name: наименование задания celery, чтобы понимать какие события мы выполняем синхронно, а какие нет
        :return: None
        '''
        result = None
        event_sequence = set(filter(lambda celery_task: celery_task.event.name == event_name, EventSequences))
        count_events = len(event_sequence)
        if count_events > 0:
            events = set()
            event_broker = None
            for celery_task in event_sequence:
                event_or_broker = celery_task.event
                if isinstance(event_or_broker, IEvent):
                    events.add(celery_task)
                elif isinstance(event_or_broker, IEventBroker):
                    event_broker = celery_task

            if event_broker is None:
                for event in events:
                    need_async = EventManager._need_async(event, is_async, celery_task_name)
                    intermediate_result = event_retainer(event, options, need_async)
                    if intermediate_result is not None:
                        result = EventManager._collect_result(result, intermediate_result)
            else:
                need_async = EventManager._need_async(event_broker, is_async, celery_task_name)
                intermediate_result = event_retainer(event_broker, options, need_async)
                if intermediate_result is not None:
                    result = EventManager._collect_result(result, intermediate_result)
        return result

    @staticmethod
    def subscribe(model = None, func = None, signal = None, event_or_broker = None, handler = None):
        if signal is not None:
            signal.connect(func, sender=model)
        else:
            celery_task = CeleryTaskProxy(event_or_broker)
            celery_app.tasks.register(celery_task)
            EventSequences.add(celery_task)


    @staticmethod
    def get_event_sequences():
        return EventSequences

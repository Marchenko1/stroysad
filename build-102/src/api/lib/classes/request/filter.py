from api.lib.classes.exceptions import APIParamsException
from api.settings import operators_dict


class APIFilter:
    @classmethod
    def convert(cls, filters_list, model, user):
        filter = {}
        excludes = {}
        errors = []

        if hasattr(model, 'static_filter_class'):
            filter.update(model.static_filter_class.get_filters(user))

        if type(filters_list) is list and len(filters_list) == 0:
            errors.append(APIParamsException('В списке фильтров нет ни одного элемента'))
        elif type(filters_list) is not list:
            errors.append(APIParamsException('Список фильтров (filter или filter_relations) должен быть типа "list"'))

        for filter_list in filters_list:
            # Конвертируем фильтры по условиям
            for filter_item in filter_list:
                if 'operator' in filter_item:
                    if type(filter_item['value']) is dict:
                        # Диапазон
                        if 'from' in filter_item['value'] or 'to' in filter_item['value']:
                            if 'from' in filter_item['value']:
                                name = ''
                                if filter_item['value']['from'] != '0' and filter_item['value']['from'] != 0:
                                    name = filter_item['key'] + operators_dict['>=']
                                else:
                                    name = filter_item['key'] + operators_dict['>=']
                                filter[name] = float(filter_item['value']['from'])
                            if 'to' in filter_item['value']:
                                name = filter_item['key'] + operators_dict['<=']
                                filter[name] = float(filter_item['value']['to'])
                    elif type(filter_item['value']) is list and filter_item['operator'] != '!in':
                        name = filter_item['key'] + operators_dict['in']
                        filter[name] = filter_item['value']
                    else:
                        if filter_item['value'] == 'True' or filter_item['value'] == 'Да':
                            filter_item['value'] = True
                        elif filter_item['value'] == 'False' or filter_item['value'] == 'Нет':
                            filter_item['value'] = False
                        if filter_item['operator'] == 'in':
                            if type(filter_item['value']) != list:
                                errors.append(APIParamsException("Значение фильтра [%s] должно быть массивом значений" % filter_item['key']))
                        if filter_item['operator'] != '!=':
                            if filter_item['operator'] != '!in':
                                dict_name = filter_item['key'] + operators_dict[filter_item['operator']]
                                filter[dict_name] = filter_item['value']
                            else:
                                dict_name = filter_item['key'] + '__in'
                                excludes[dict_name] = filter_item['value']
                        else:
                            dict_name = filter_item['key']
                            excludes[dict_name] = filter_item['value']
                elif 'condition' in filter_item:
                    if filter_item['condition']['value'] == 'True' or filter_item['condition']['value'] == 'Да':
                        filter_item['condition']['value'] = True
                    elif filter_item['condition']['value'] == 'False' or filter_item['condition']['value'] == 'Нет':
                        filter_item['condition']['value'] = False
                    if filter_item['condition']['operator'] != '!=':
                        condition = {
                            filter_item['condition']['key'] + operators_dict[filter_item['condition']['operator']]:
                                filter_item['condition']["value"]}
                        searched_entity = None
                        try:
                            if filter_item['condition']['key'] == 'status_id' and filter_item['condition']["value"] == None:
                                searched_entity = model.objects.filter(code='null').last()
                            else:
                                searched_entity = model.objects.filter(**condition).last()
                        except Exception as ex:
                            # TODO: Логгинг ошибок во всех except блоках
                            errors.append(APIParamsException("Ключ фильтра указан неверно: {0}".format(str(ex))))
                        if searched_entity != None:
                            value = getattr(searched_entity, filter_item['condition']['target_key'])
                            if filter_item['key'] == 'parent_id':
                                value = value
                            filter[filter_item['key']] = value
                else:
                    errors.append(APIParamsException("Не верно составлен фильтр: {0}".format(str(filter_item))))
        return filter, excludes, errors
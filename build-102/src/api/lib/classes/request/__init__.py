'''
    Класс обработчик входящего запроса
'''
from api.lib.classes.entity.file import ApiFile
from api.lib.classes.exceptions import verbose_errors
from api.lib.classes.methods.base import APIMethod
from api.lib.classes.validator.file import FileValidator
from api.lib.classes.validator.read import ReadValidator


class APIRequest:
    def __init__(self,
                 # Аргументы переданные в вызов API
                 args: dict,
                 # Параметры, которые будут учавствовать в обработке запроса
                 params: dict,
                 # Накапливаемые ошибки
                 errors: list,
                 # Накапливаемые варнинги
                 warnings: list,
                 # Класс обработчик запроса
                 method, class_handler = None):
        '''
        :param params: Параметры запроса - dict в который входит HTTP request
        или текущий пользователь с дополнительными параметрами запроса
        :param method: Класс обработчик вызова API
        '''
        if 'model' not in args:
            raise AttributeError('Модель сущности не определена')

        model = args['model']
        self.args = args
        self.params = params
        self.errors = errors
        self.warnings = warnings
        self.model = model
        self.method = method
        self.class_handler = class_handler
        # У кастомных обработчиков API нет валидатора, устанавливаем валидатор на чтение, чтобы проверились права пользователя и упаковались входящие параметры
        self.validator = self.method.validator if hasattr(self.method, 'validator') else ReadValidator()
        if self.args.get('request', None) is not None and '/api/file/' in self.args['request'].path or type(class_handler) is ApiFile:
            self.validator = FileValidator()
        self.validator.args = self.args
        self.validator.model = self.model
        self.validator.params = self.params
        self.validator.errors = self.errors
        self.validator.warnings = self.warnings
        self.result = {'status': 'success'}

    def validate(self):
        '''
        Валидация входящих параметров + проверка прав доступа + валидация правил обработчика
        :return: bool
        '''
        valid = False
        self.result = {'status': 'success'}

        # Проверяем входящие параметры -> обработанные параметры в случае успеха попадают в self.params
        self.params = self.validator.validate()
        if len(self.errors) > 0:
            self.result['status'] = 'error'
            # Ошибки отображаем всегда через эту функцию - она возвращает "представление" ошибки в виде строки в зависимости от типа ошибки.
            # Предупреждения пока что нет необходимости обрабатывать таким образом, так что они храняться уже в виде строки
            self.result['errors'] = verbose_errors(self.errors)

        params_validation = len(self.errors) == 0
        if params_validation:
            # Проверяем правила обработчика запроса
            handler_validation = self.method.validate(self.params, self.result) if hasattr(self.method, 'validate') else True
            if handler_validation:
                valid = True

        return valid

    def execute(self):
        result = None
        if isinstance(self.method, APIMethod):
            self.method.execute()
            result = self.method.result
        else:
            result = self.method(params = self.params, result = self.result, warnings = self.warnings, errors = self.errors)
        return result


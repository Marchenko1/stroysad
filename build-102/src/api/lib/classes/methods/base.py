'''
    Базовый класс обработчик методов API
'''
from api.lib.classes.exceptions import verbose_errors
from api.lib.classes.metadata.fields import APIFields, FieldsDetalization


class APIMethod:
    def __init__(self):
        self.result = {'status': 'success'}
        self.params = {}
        self.errors = []
        self.warnings = []
        self.log = []

    def get_approved_fields(self):
        all_fields = APIFields.get_all_model_fields(self.params['model'], [FieldsDetalization.with_m2m, FieldsDetalization.with_foreign], for_filter=False)

        return all_fields


    def execute(self):
        pass
    
    def validate(self, params: dict = None, result: dict = None):
        if params is not None:
            self.params = params
        if result is not None:
            self.result = result
        return True
    


import json
import time
from datetime import (
    datetime,
    date
)
from uuid import UUID

import logging
import pytz
from django.db.models import (
    DateTimeField,
    DateField,
    ManyToManyField
)

from api.lib.classes.exceptions import (
    verbose_errors,
    APIParamsException,
    APIProgramException
)
from api.lib.classes.methods.base import APIMethod

from api.lib.util.entities import (
    search_entities,
    search_entity
)
from api.lib.classes.validator.write import WriteValidator


class WriteMethod(APIMethod):
    def __init__(self, *args, **kwargs):
        super(WriteMethod, self).__init__(*args, **kwargs)
        self.validator = WriteValidator()
        self.method_name = 'update'

    def validate(self, params: dict = None, result: dict = None):
        self.params = params
        self.result = result

        if not 'list' in self.params:
            self.errors.append(APIParamsException("Не хватает обязательного параметра list"))
        elif not type(self.params['list']) is list:
            self.errors.append(APIParamsException("Параметр list должен быть массивом объектов к записи"))

        if len(self.errors) > 0:
            self.result['status'] = 'error'
            self.result['errors'] = verbose_errors(self.errors + self.validator.errors)
        return len(self.errors) == 0 and len(self.validator.errors) == 0

    def execute(self):
        from api.lib.util.api import call_api

        self.call_api = call_api
        start_time = time.time()
        updated_counter = 0
        iterator = 0
        isset_errors = False
        # Список результат обновления сущностей
        updated_entities = []
        # Список отложенных связей до конца записи
        self.deferred_relations = []

        self.log.append(
            'Начинается обработка переданных данных для записи. Всего элементов: {0}'.format(len(self.params["list"]))
        )
        for element in self.params['list']:
            if iterator == 0:
                message = "Обработка первого элемента началась."
                self.log.append(message)
                logging.info(message)
            elif iterator % 100 == 0:
                if 'is_relation' not in self.params and 'is_m2m' not in self.params:
                    message = "Обработка элемента № {0}, прошло: {1} сек. ".format(str(iterator), round(time.time() - start_time)) + \
                              "Успешно обработанных элементов: {0}".format(updated_counter)
                    self.log.append(message)
                    logging.info(message)

            self.foreign_relations = []
            instance_id = None

            if not type(element) is dict:
                self.log.append('Элемент № {0} неверного типа: {1}. Обмен продолжается'.format(str(iterator), str(type(element))))
                self.errors.append(APIParamsException("Элемент массива list должен быть ассоциативным массивом"))
                break

            self.sub_entities = {}

            record_errors = []

            try:
                instance, record_errors = self.save_element(element)
            except Exception as ex:
                instance = None
                record_errors = [APIProgramException(str(ex))] + record_errors
                logging.exception(ex)

            status = 'error'
            if instance is not None:
                updated_counter += 1
                status = 'success'
                instance_id = instance.id

            if status == 'success':
                if 'files' in element and 'files_folder' in self.params and hasattr(instance, 'handle_files'):
                    instance.handle_files(self.params['user'], self.params['files_folder'], element['files'])
                elif hasattr(instance, 'handle_files'):
                    instance.handle_files(self.params['user'])

            update_result = {
                'status': status,
                'id':     None,
            }

            if len(record_errors) > 0:
                self.log.append('При записи объекта {0} встречены ошибки. {1}'.format(iterator, verbose_errors(record_errors)))
                if not isset_errors:
                    isset_errors = True
                update_result['errors'] = verbose_errors(record_errors)

            if len(self.foreign_relations) > 0:
                update_result['relations'] = self.foreign_relations

            if 'is_m2m' not in self.params:
                update_result['i'] = iterator

            if len(self.sub_entities.keys()) > 0:
                update_result['sub_entities'] = self.sub_entities
            if instance_id is not None:
                update_result['id'] = str(instance_id)

            updated_entities.append(update_result)

            iterator += 1

        if 'is_relation' not in self.params and 'is_m2m' not in self.params:
            msg_finish = "Обмен данными завершен. Последний элемент: {0}.".format(str(iterator))
            self.log.append(msg_finish)
            logging.info(msg_finish)
        if len(self.deferred_relations) > 0 and len(self.params['list']) > 1:
            self.restore_deferred_relations()

        if isset_errors and updated_counter == 0:
            self.log.append('В результате обмена возникли ошибки. Ни одна запись не была обработана')
            self.result['status'] = 'error'
            # self.result['errors'] = verbose_errors(self.errors)
        # elif len(self.errors) > 0:
        #     self.result['errors'] = verbose_errors(self.errors)

        if len(self.warnings) > 0:
            self.result['warnings'] = self.warnings

        result_data = {
            'results': updated_entities
        }
        if self.result['status'] == 'success':
            self.log.append('Обмен прошел успешно. Число обработанных элементов: {0}'.format(updated_counter))
            result_data['updated'] = updated_counter

        self.result['time'] = time.time() - start_time

        if 'is_relation' not in self.params and 'is_m2m' not in self.params:
            self.log.append('Результаты обработки данных: {0}'.format(json.dumps(result_data, ensure_ascii = False)))
            success_time_message = "Запись объектов заняла: {0} секунд (".format(round(self.result['time'], 2)) + \
                            "{0} минут)".format(round(self.result['time'] / 60, 2))
            logging.info(success_time_message)
            self.log.append(success_time_message)

        self.result['log'] = self.log
        self.result['data'] = result_data

    def save_element(self, element):
        # Проверка на существование
        entity = search_entity(self.params['model'], element)
        record_errors = []
        # Если существует обновляем иначе новая запись
        if entity is not None:
            entity, record_errors = self.update(entity, element)

        return entity, record_errors

    def check_exists(self, element):
        entity = None

        if 'id' in element:
            try:
                entity = self.params['model'].objects.get(id = element['id'])
            except Exception as ex:
                # TODO: логирование ошибок в файл, чтобы потом прочитать
                pass
        if entity is None and 'xml_id' in element:
            try:
                entity = self.params['model'].objects.get(xml_id = element['xml_id'])
            except Exception as ex:
                pass

        if entity is None and ('id' in element or 'xml_id' in element):
            self.warnings.append(
                "Объект с {0} ".format('id' if 'id' in element else 'xml_id') +
                "{0} не найден.".format(element['id'] if 'id' in element else element['xml_id']))

        return entity

    def update(self, entity, element):
        sub_entity_objects = {}
        update_fields = []
        entity_deffered_relations = {}
        record_errors = []
        filter_field = lambda field_: field_.name == field
        self.all_fields_original = self.params['model']._meta.fields
        self.all_m2m_fields_original = self.params['model']._meta.many_to_many
        for field in element:
            if field == 'id':
                continue
            field_value = element[field]
            if hasattr(entity, field):
                fields = list(filter(filter_field, self.all_fields_original))
                if len(fields) == 0:
                    fields = list(filter(filter_field, self.all_m2m_fields_original))
                field_original = fields[0]

                # Если это обычное поле или FK
                if type(field_value) is not list and type(field_value) is not dict:

                    if hasattr(entity, field):
                        is_UUID = type(getattr(entity, field)) is UUID
                        is_datetime = type(getattr(entity, field)) is datetime or type(getattr(entity, field)) is date

                        if is_UUID:
                            if field != 'id':
                                if getattr(entity, field) != UUID(field_value):
                                    setattr(entity, field, field_value)
                                    update_fields.append(field)
                        elif is_datetime:
                            try:
                                field_time = datetime.strptime(field_value, '%Y-%m-%d %H:%M:%S')
                            except Exception as ex:
                                try:
                                    field_time = datetime.fromisoformat(field_value)
                                except Exception as ex:
                                    record_errors.append(APIParamsException(
                                        "Переданная дата не соответствует формату: 0001-01-01 00:00:00. Запись "
                                        "объекта будет отменена."))
                                    return None, record_errors
                            field_time = pytz.utc.localize(field_time)
                            field_value = field_time
                            if getattr(entity, field) != field_value:
                                setattr(entity, field, field_time)
                                update_fields.append(field)
                        elif getattr(entity, field) != field_value\
                                and field != 'username'\
                                and field != 'email':
                            if field == 'phone':
                                if getattr(entity, field) is not None:
                                    continue
                            if type(field_original) is not ManyToManyField:
                                setattr(entity, field, field_value)
                                update_fields.append(field)
                            else:
                                getattr(entity, field).clear()
                    else:
                        record_errors.append(APIParamsException(
                            "Поля {0} в сущности {1} не существует. Запись объекта будет ".format(field, entity._meta.model_name) +
                            "отменена."))
                        return None, record_errors
                # Если это связь FK
                elif type(field_value) is dict:
                    is_deferred_relation = self.set_foreign_relation(entity, field, field_value)
                    if is_deferred_relation:
                        entity_deffered_relations[field] = field_value
                # Иначе M2M связь
                elif type(field_value) is list:
                    m2m_objects, need_clear_m2m = self.collect_m2m_entities(entity, field, field_value)
                    sub_entity_objects[field] = {
                        'list':       m2m_objects,
                        'need_clear': need_clear_m2m
                    }
            else:
                if 'Поле {0} не существует'.format(field) not in self.warnings:
                    self.warnings.append('Поле {0} не существует'.format(field))
        try:
            if len(update_fields) > 0:
                self.modifier_fields_before_save(entity, element, update_fields)
                entity.full_clean()
                setattr(entity, '_update_author', self.params['user'])
                entity.save(update_fields = update_fields)
        except Exception as ex:
            entity = None

            record_errors.append(
                APIParamsException("Валидация сущности не была пройдена, входящие параметры не валидны: {0}".format(str(ex))))

        if entity is not None:
            if len(sub_entity_objects.keys()) > 0:
                self.update_m2m(entity, sub_entity_objects)
            if len(entity_deffered_relations.keys()) > 0:
                for relation_field in entity_deffered_relations:
                    self.deferred_relations.append({
                        'field':     relation_field,
                        'entity_id': entity.id,
                        'value':     entity_deffered_relations[relation_field]
                    })
        return entity, record_errors

    def create(self, element):
        entity = self.params['model']()
        sub_entity_objects = {}
        entity_deffered_relations = {}
        record_errors = []

        self.all_fields_original = self.params['model']._meta.fields
        self.all_m2m_fields_original = self.params['model']._meta.many_to_many

        for field in element:
            if field == 'id':
                continue
            filter_field = lambda field_: field_.name == field
            field_value = element[field]
            if hasattr(entity, field):
                fields = list(filter(filter_field, self.all_fields_original))
                if len(fields) == 0:
                    fields = list(filter(filter_field, self.all_m2m_fields_original))
                field_original = fields[0]
                if type(field_value) is not list and type(field_value) is not dict:
                    is_datetime = type(field_original) is DateTimeField or type(field_original) is DateField

                    if is_datetime:
                        if field_value is not None:
                            try:
                                field_time = datetime.strptime(field_value, '%Y-%m-%d %H:%M:%S')
                            except Exception as ex:
                                try:
                                    field_time = datetime.fromisoformat(field_value)
                                except Exception as ex:
                                    record_errors.append(APIParamsException(
                                        "Переданная дата не соответствует формату: 0001-01-01 00:00:00. Запись "
                                        "объекта будет отменена."))
                                    return None, record_errors
                            field_time = pytz.utc.localize(field_time)
                            field_value = field_time
                            setattr(entity, field, field_value)
                    else:
                        if type(field_original) is not ManyToManyField:
                            setattr(entity, field, field_value)

                elif type(field_value) is dict:
                    is_deferred_relation = self.set_foreign_relation(entity, field, field_value)
                    if is_deferred_relation:
                        entity_deffered_relations[field] = field_value
                elif type(field_value) is list:
                    m2m_objects, need_clear_m2m = self.collect_m2m_entities(entity, field, field_value)
                    sub_entity_objects[field] = {
                        'list':       m2m_objects,
                        'need_clear': need_clear_m2m
                    }
            else:
                if 'Поле {0} не существует'.format(field) not in self.warnings:
                    self.warnings.append('Поле {0} не существует'.format(field))

        self.modifier_fields_before_save(entity, element)
        try:
            entity.full_clean()
            setattr(entity, '_create_author', self.params['user'])
            setattr(entity, '_update_author', self.params['user'])
            entity.save()
        except Exception as ex:
            logging.exception(ex)
            entity = None

            record_errors.append(
                APIParamsException("Валидация сущности не была пройдена, входящие параметры не валидны: {0}".format(str(ex))))
            return None, record_errors

        if entity is not None:
            if len(sub_entity_objects.keys()) > 0:
                self.update_m2m(entity, sub_entity_objects)
            if len(entity_deffered_relations.keys()) > 0:
                for relation_field in entity_deffered_relations:
                    self.deferred_relations.append({
                        'field':     relation_field,
                        'entity_id': entity.id,
                        'value':     entity_deffered_relations[relation_field]
                    })
        return entity, record_errors

    def set_foreign_relation(self, entity, field, field_value):
        field_filter_lambda = lambda filtered_field: filtered_field.name == field
        filtered_fields = list(filter(field_filter_lambda, self.all_fields_original))
        foreign_errors = []
        ''' 
        deferred_relation = False
        Это отложенная связь или нет
        Если связь по существующим объектам не найдена
        и field поле ссылается на модель entity (на саму себя, например parent_id),
        то мы откладываем эту связь до конца записи всех элементов, потому что они могут быть еще не записанными
        например, если в списке товаров условно:
        [
            {'name': 'Гвоздь', 'parent_xml_id': '1'},
            {'name': 'Гвозди', 'xml_id': '1'}
        ]
        при записи этого набора товар гвоздь не найдет свою группу и отложит поиск этой связи
        до конца создания всех элементов, а потом восстановит их.
        '''
        deferred_relation = False

        if len(filtered_fields) > 0:
            field_model = filtered_fields[0].related_model
            field_model_name = field_model._meta.model_name
            # Ищем по id, xml_id и по другим полям поиска
            searched_entity = search_entity(field_model, field_value)
            relation_id = None
            try:
                # Если найдено - устанавливаем объекту свойство с найденным объектом
                if searched_entity is not None:
                    setattr(entity, field, searched_entity)
                    relation_id = searched_entity.id
                else:
                    if field_model is self.params['model']:
                        deferred_relation = True
                    else:
                        # Если не найдено, пытаемся создать новое и привязать
                        result = self.call_api(field_model_name, 'update', params = {
                            'user':        self.params['user'],
                            'model':       field_model,
                            'is_relation': True,
                            'list':        [field_value]
                        })
                        if len(result['errors']) == 0:
                            if result['status'] == 'success' and 'updated' in result['data']:
                                setattr(entity, field + '_id', result['data']['results'][0]['id'])
                                relation_id = result['data']['results'][0]['id']
                            else:
                                if 'errors' in result and len(result['errors']) > 0:
                                    foreign_errors = result['errors']
                        else:
                            foreign_errors += result['errors']



            except Exception as ex:
                # Если что-то пошло не так и не установилось найденное значение
                self.errors.append(APIProgramException(str(ex)))

            relation_status = {
                'relation': field,
                'result':   None,
                'id':       relation_id
            }

            if relation_id is not None:
                relation_status['result'] = 'success'
                relation_status['id'] = relation_id
            else:
                relation_status['result'] = 'error'
                if len(foreign_errors) > 0:
                    relation_status['errors'] = foreign_errors

            if 'errors' in relation_status:
                relation_status['errors'] = relation_status['errors']

            self.foreign_relations.append(relation_status)
        return deferred_relation

    def modifier_fields_before_save(self, entity, element, update_fields = None):
        if hasattr(self.params['model'], 'modifier_before_save'):
            modifier_before_save = self.params['model'].modifier_before_save()
            if modifier_before_save is not None:
                for modifier_handler in modifier_before_save:
                    modifier_before_save[modifier_handler](entity, element, update_fields)

    def collect_m2m_entities(self, entity, m2m_field_name, values):
        sub_entity_objects = []
        need_clear_if_m2m = False
        from django.conf import settings

        if hasattr(entity, m2m_field_name) == False:
            self.errors.append(
                APIParamsException("M2M поля {0} в сущности {1} не существует".format(m2m_field_name, entity._meta.model_name)))
            return None, sub_entity_objects, need_clear_if_m2m
        else:
            m2m_fields = self.all_m2m_fields_original
            m2m_field_model = None

            for m2m_field in m2m_fields:
                if m2m_field.name == m2m_field_name:
                    m2m_field_model = m2m_field.related_model

            if m2m_field_model is None:
                self.errors.append(APIProgramException("Ошибка в коде, не найдена модель m2m связи"))
                return None, sub_entity_objects, need_clear_if_m2m

            # Ищем существущие и откидываем найденные из списка values
            exist_entities, new_values = search_entities(m2m_field_model, values)

            values = new_values

            need_clear_if_m2m = m2m_field_model.need_clear_if_m2m if hasattr(m2m_field_model,
                                                                             'need_clear_if_m2m') else False
            relation = getattr(entity, m2m_field_name)

            sub_entity_objects += exist_entities

            if len(values) > 0:
                params = {
                    'user':   self.params['user'],
                    'model':  m2m_field_model,
                    'is_m2m': True,
                    'list':   values
                }

                api_m2m_model_name = m2m_field_model._meta.model_name
                if api_m2m_model_name in settings.API_ENTITIES:
                    result = self.call_api(api_m2m_model_name, 'update', params = params)
                    result['entity'] = api_m2m_model_name
                    if result['status'] == 'success':
                        m2m_objects = [
                            m2m_field_model.objects.get(id = result['id']) if result['id'] is not None else None for
                            result in result['data']['results']]
                        for m2m_object in m2m_objects:
                            if m2m_object is not None:
                                sub_entity_objects.append(m2m_object)
                        # del result['data']
                    if len(exist_entities) > 0:
                        result['status'] = 'success'
                        updated = 0
                        if 'updated' in result:
                            updated = result['updated'] + len(exist_entities)
                        else:
                            updated = len(exist_entities)

                        result['updated'] = updated
                        result['status'] = 'success'
                    self.sub_entities[api_m2m_model_name] = result

        return sub_entity_objects, need_clear_if_m2m

    def update_m2m(self, entity, sub_entity_objects):
        for m2m_field in sub_entity_objects:
            if hasattr(entity, m2m_field):
                relation_manager = getattr(entity, m2m_field)

                if sub_entity_objects[m2m_field]['need_clear']:
                    all_relations = relation_manager.all()
                    relation_manager.clear()
                    all_relations.delete()

                for m2m_object in sub_entity_objects[m2m_field]['list']:
                    relation_manager.add(m2m_object)

    def restore_deferred_relations(self):
        for deferred_relation in self.deferred_relations:
            try:
                searched_entity = search_entity(self.params['model'], deferred_relation['value'])
                if searched_entity is not None:
                    entity = self.params['model'].objects.get(id = deferred_relation['entity_id'])
                    relation_field_name = deferred_relation['field'] + '_id'
                    if hasattr(entity, relation_field_name):
                        setattr(entity, relation_field_name, searched_entity.id)
                        entity.save(update_fields = [relation_field_name])
            except Exception as ex:
                self.warnings.append(str(ex))

import time

from api.lib.classes.exceptions import verbose_errors
from api.lib.classes.methods.base import APIMethod
from api.lib.classes.request.filter import APIFilter
from api.lib.util.fields import exclude_forbidden_fields, approved_fields_to_meta
from api.lib.classes.validator.meta import ReadMetaValidator


class ReadMetaMethod(APIMethod):
    validator = ReadMetaValidator
    method_name = 'meta'


    def execute(cls):
        start_time = time.time()
        filter, filter_excludes, filter_errors = APIFilter.convert([cls.params['filter'] if 'filter' in cls.params else []], cls.params['model'], cls.params['user'])
        if len(filter_errors) > 0:
            cls.errors += filter_errors

        all_approved_fields = cls.get_approved_fields()

        approved_fields = exclude_forbidden_fields(cls.params['model'], all_approved_fields)
        approved_fields = approved_fields_to_meta(approved_fields)

        if len(cls.errors) > 0:
            cls.result['status'] = 'error'
            cls.result['errors'] = verbose_errors(cls.errors)

        cls.result['time'] = time.time() - start_time
        cls.result['data'] = approved_fields



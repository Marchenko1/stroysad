import time

from api.lib.classes.events.EventManager import EventManager
from api.lib.classes.exceptions import APIParamsException, verbose_errors
from api.lib.classes.metadata.fields import FieldsDetalization, APIFields
from api.lib.classes.methods.base import APIMethod
from api.lib.classes.pagination import Paginator
from api.lib.classes.request.filter import APIFilter
from api.lib.util import json
from api.lib.util.entities import merge_values
from api.lib.util.fields import exclude_forbidden_fields, approved_fields_to_names_list
from api.lib.classes.validator.read import ReadValidator


class ReadMethod(APIMethod):
    def __init__(self, *args, **kwargs):
        super(ReadMethod, self).__init__(*args, **kwargs)
        self.validator = ReadValidator()
        self.method_name = 'list'
        self.default_per_page = 10

    def get_approved_fields(self):
        self.all_fields = APIFields.get_all_model_fields(self.params['model'], [FieldsDetalization.with_m2m, FieldsDetalization.with_foreign], for_filter=True)

        approved_fields = exclude_forbidden_fields(self.params['model'], self.all_fields)
        approved_fields = approved_fields_to_names_list(approved_fields)

        return approved_fields

    def validate(self, params: dict = None, result: dict = None):
        self.params = params
        self.result = result
        self.page = 1
        self.per_page = self.default_per_page
        self.paginator = None

        if 'page' in self.params:
            self.page = self.params['page']

        if 'per_page' in self.params:
            self.per_page = self.params['per_page']
            if self.per_page == -1:
                self.per_page = 100000000

        self.page = int(self.page)
        self.per_page = int(self.per_page)

        self.filter, self.filter_excludes, filter_errors = APIFilter.convert([self.params['filter'] if 'filter' in self.params else []], self.params['model'], self.params['user'])
        if len(filter_errors) > 0:
            self.errors += filter_errors
        return len(filter_errors) == 0

    def execute(self):
        start_time = time.time()

        approved_fields = self.get_approved_fields()

        entities = []

        try:
            entities = self.params['model'].objects.filter(**self.filter).exclude(**self.filter_excludes).values(*['id'] if 'without_related' not in self.params else approved_fields).distinct()
            if 'ordering' in self.params:
                entities = entities.order_by(*self.params['ordering'])
            self.paginator = Paginator(entities, self.per_page)
            offset_from, offset_to = self.paginator.get_offsets(self.page)

            if 'original_response' not in self.params or ('original_response' in self.params and self.params['original_response'] == False):
                if 'without_related' not in self.params:
                    entities_ids = [entity['id'] for entity in entities[offset_from: offset_to]]
                    entities = self.params['model'].objects.filter(id__in=entities_ids)
                    if 'ordering' in self.params:
                        entities = entities.order_by(*self.params['ordering'])
                    entities = merge_values(approved_fields, entities.values(*approved_fields), self.params['model'])
                else:
                    entities = list(entities[offset_from: offset_to].values(*approved_fields))
                    entities = json.JsonConverter.to_json(entities, approved_fields, self.params['model'])
            else:
                entities_ids = [entity['id'] for entity in entities[offset_from: offset_to]]
                entities = self.params['model'].objects.filter(id__in = entities_ids)
                if 'ordering' in self.params:
                    entities = entities.order_by(*self.params['ordering'])
        except Exception as ex:
            self.errors.append(APIParamsException("Фильтр составлен неверно: {0}".format(str(ex))))

        if len(self.errors) > 0:
            self.result['status'] = 'error'
            self.result['errors'] = verbose_errors(self.errors)

        self.result['time'] = time.time() - start_time

        if self.paginator is not None:
            self.paginator.entities = entities
            self.result.update(self.paginator.get_page(self.page))

from django.views.generic import TemplateView
from api.lib.util.api import call_api


class BaseView(TemplateView):
    """
        Базовое TemplateView для работы с API
        :param function: имя функции, которая указывается в urls.py для вызова конкретного метода для конкретного url, если потребуется
        :param api_entity: имя объекта API для которого используется эта View
        :param api_view_entities: имена объектов API которые используются во View помимо основного объекта
    """
    function = None
    api_entity = None
    api_view_entities = None

    def call_api(self, *args, **kwargs):
        return call_api(*args, **kwargs)

    def dispatch(self, request, *args, **kwargs):
        """
            Метод переделан с целью вызова дополнительных функций по указанному в urls.py URL.
            Предполагается, что в BaseView.as_view будет передан именованный аргумент function,
            показывающий какую функцию для указанного URl нужно выполнять.
                Например, к ProductView есть обращение через /product/<pk>/get_files/, который должен получить картинки файлов,
                а функция get() уже занята под /product/<pk>/. Чтобы решить эту проблему необходимо добавить в ProductView функцию
                и передать название этой функции в urls.py как .as_view(function="get_files")
        """
        if self.function is not None and hasattr(self, self.function):
            handler = getattr(self, self.function)
        elif request.method.lower() in self.http_method_names:
            handler = getattr(self, request.method.lower(), self.http_method_not_allowed)
        else:
            handler = self.http_method_not_allowed
        return handler(request, *args, **kwargs)

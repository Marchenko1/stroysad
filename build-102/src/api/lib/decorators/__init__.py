from api.lib.classes.exceptions import verbose_errors
from django.shortcuts import redirect


def api_handler(func):
    def wrapper(*args, **kwargs):
        response_obj = func(*args, **kwargs)

        if len(response_obj.errors) > 0:
            response_obj.result['status'] = 'error'
            response_obj.result['errors'] = verbose_errors(response_obj.errors)

        return response_obj.result

    return wrapper


def authenticated(function, redirect_url = '/auth/'):
    def wrapper(request, *args, **kwargs):
        if request.user.is_authenticated:
            return function(request, *args, **kwargs)
        else:
            return redirect(redirect_url)
    return wrapper
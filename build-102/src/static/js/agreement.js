(function($) {

    /*
        Клик на кнопку согласия обработки персональных данных
     */
    $(document).on('click', '.field-agreement input', function() {
        var $checkbox = $(this);
        var checked = $checkbox.prop('checked');
        var $button = $checkbox.closest('form').find('[data-agreement]');
        if (checked) {
            $button.removeClass('disabled').prop('disabled', false);
        } else {
            $button.addClass('disabled').prop('disabled', true);
        }
    });

})(jQuery);

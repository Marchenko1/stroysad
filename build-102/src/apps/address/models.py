from django.db import models
from django.utils.translation import ugettext_lazy as _



class Address(models.Model):
    """ Адрес """
    city = models.CharField(_('city'), max_length=128)
    street = models.CharField(_('street'), max_length=256)
    building = models.CharField(_('building'), max_length=16, blank=True)
    house = models.CharField(_('house'), max_length=16)
    suite = models.CharField(_('suit'), max_length=16, blank=True)
    zip = models.IntegerField(_('zip'), blank=True, null=True)

    class Meta:
        verbose_name = _('address')
        verbose_name_plural = _('addresses')
        ordering = ('city', 'street')

    def __str__(self):
        addr_formats = (
            ('zip', '{}'),
            ('city', 'г. {}'),
            ('street', 'ул. {}'),
            ('building', 'корп. {}'),
            ('house', 'д. {}'),
            ('suite', 'кв. {}'),
        )

        addr_data = []
        for field, tpl in addr_formats:
            value = getattr(self, field, None)
            if value:
                addr_data.append(tpl.format(value))

        return ', '.join(addr_data)

    def get_copy(self, save=False):
        """ Создание копии экземпляра """
        obj = self.__class__()
        for field in self._meta.get_fields():
            if field.concrete and not field.auto_created:
                setattr(obj, field.name, getattr(self, field.name))

        if save:
            obj.save()

        return obj

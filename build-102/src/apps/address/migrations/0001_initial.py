# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='Address',
            fields=[
                ('id', models.AutoField(primary_key=True, auto_created=True, verbose_name='ID', serialize=False)),
                ('city', models.CharField(max_length=128, verbose_name='city')),
                ('region', models.CharField(blank=True, verbose_name='region', max_length=128)),
                ('street', models.CharField(max_length=256, verbose_name='street')),
                ('house', models.IntegerField(verbose_name='house')),
                ('suite', models.IntegerField(blank=True, null=True, verbose_name='suit')),
            ],
            options={
                'ordering': ('city', 'street'),
                'verbose_name': 'address',
                'verbose_name_plural': 'addresses',
            },
        ),
    ]

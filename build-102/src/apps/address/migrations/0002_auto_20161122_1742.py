# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('address', '0001_initial'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='address',
            name='region',
        ),
        migrations.AddField(
            model_name='address',
            name='building',
            field=models.CharField(blank=True, verbose_name='building', max_length=16),
        ),
        migrations.AddField(
            model_name='address',
            name='zip',
            field=models.IntegerField(blank=True, verbose_name='zip', null=True),
        ),
        migrations.AlterField(
            model_name='address',
            name='house',
            field=models.CharField(verbose_name='house', max_length=16),
        ),
        migrations.AlterField(
            model_name='address',
            name='suite',
            field=models.CharField(blank=True, verbose_name='suit', max_length=16, default=''),
            preserve_default=False,
        ),
    ]

from django import forms
from libs.form_helper.forms import FormHelperMixin
from .models import Address


class AddressForm(FormHelperMixin, forms.ModelForm):
    """ Адрес """
    csrf_token = False
    render_error_message = True
    field_template = 'fields/field.html'

    class Meta:
        model = Address
        fields = '__all__'

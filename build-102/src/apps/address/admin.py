from django.contrib import admin
from .models import Address
from project.admin import ModelAdminMixin


@admin.register(Address)
class AddressAdmin(ModelAdminMixin, admin.ModelAdmin):
    pass

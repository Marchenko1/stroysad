(function($) {

    $(document).ready(function() {
        $('.content').parallax({
            bgHeight: 120
        });
    });

})(jQuery);

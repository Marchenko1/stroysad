from django.contrib import admin
from django.utils.translation import ugettext_lazy as _
from solo.admin import SingletonModelAdmin
from suit.admin import SortableStackedInline
from project.admin import ModelAdminInlineMixin
from seo.admin import SeoModelAdminMixin
from .models import AboutConfig, Advantage


class AdvantagesAdmin(ModelAdminInlineMixin, SortableStackedInline):
    model = Advantage
    extra = 0
    sortable = 'sort_order'
    suit_classes = 'suit-tab suit-tab-advantages'


@admin.register(AboutConfig)
class AboutConfigAdmin(SeoModelAdminMixin, SingletonModelAdmin):
    fieldsets = (
        (None, {
            'classes': ('suit-tab', 'suit-tab-general'),
            'fields': (
                'header', 'background', 'text',
            ),
        }),
    )
    inlines = (AdvantagesAdmin, )
    suit_form_tabs = (
        ('general', _('General')),
        ('advantages', _('Advantages')),
    )

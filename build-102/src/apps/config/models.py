from django.db import models
from django.utils.translation import ugettext_lazy as _
from solo.models import SingletonModel


class Config(SingletonModel):
    phone = models.CharField(_('phone'), max_length=32, blank=True)

    class Meta:
        verbose_name = _("Configuration")

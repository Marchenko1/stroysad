from django import forms
from libs.form_helper.forms import FormHelperMixin
from .models import Recipient


class RecipientForm(FormHelperMixin, forms.ModelForm):
    """ Получатель """
    csrf_token = False
    render_error_message = True
    field_template = 'fields/field.html'

    class Meta:
        model = Recipient
        fields = '__all__'

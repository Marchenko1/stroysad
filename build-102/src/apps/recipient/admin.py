from django.contrib import admin
from .models import Recipient
from project.admin import ModelAdminMixin


@admin.register(Recipient)
class RecipientAdmin(ModelAdminMixin, admin.ModelAdmin):
    pass

from django.db import models
from django.utils.translation import ugettext_lazy as _


class Recipient(models.Model):
    """ Получатель """
    first_name = models.CharField(_('first name'), max_length=30)
    last_name = models.CharField(_('last name'), max_length=30)
    phone = models.CharField(_('contact phone number'), max_length=24)
    email = models.EmailField(_('e-mail'))

    class Meta:
        verbose_name = _('recipient')
        verbose_name_plural = _('recipients')
        ordering = ('last_name', 'first_name')

    def get_full_name(self):
        full_name = ' '.join((self.last_name, self.first_name))
        return full_name.strip()

    def __str__(self):
        recipient = ' '.join((self.last_name, self.first_name, self.phone, self.email))
        return recipient.strip()

    def get_copy(self, save=False):
        """ Создание копии экземпляра """
        obj = self.__class__()
        for field in self._meta.get_fields():
            if field.concrete and not field.auto_created:
                setattr(obj, field.name, getattr(self, field.name))

        if save:
            obj.save()

        return obj

# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='Recipient',
            fields=[
                ('id', models.AutoField(primary_key=True, auto_created=True, verbose_name='ID', serialize=False)),
                ('first_name', models.CharField(max_length=30, verbose_name='first name')),
                ('last_name', models.CharField(max_length=30, verbose_name='last name')),
                ('phone', models.CharField(max_length=24, verbose_name='contact phone number')),
            ],
            options={
                'ordering': ('last_name', 'first_name'),
                'verbose_name': 'recipient',
                'verbose_name_plural': 'recipients',
            },
        ),
    ]

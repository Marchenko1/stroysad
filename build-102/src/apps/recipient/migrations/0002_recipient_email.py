# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('recipient', '0001_initial'),
    ]

    operations = [
        migrations.AddField(
            model_name='recipient',
            name='email',
            field=models.EmailField(default='', max_length=254, verbose_name='e-mail'),
            preserve_default=False,
        ),
    ]

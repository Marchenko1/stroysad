from django import template
register = template.Library()


@register.filter
def strip_string(value: str):
    return value.strip()

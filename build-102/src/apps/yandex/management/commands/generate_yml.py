import datetime
import os

from django.conf import settings
from django.core.management.base import BaseCommand
from django.db.models import Q, F
from django.template.loader import render_to_string
from django.utils.safestring import mark_safe
from shop import models as catalog_models


class Command(BaseCommand):
    help = 'Generate yml for yandex'  # noqa

    def handle(self, *args, **options):
        def get_data():
            return {
                'date':     datetime.datetime.now().strftime('%Y-%m-%d %H:%M'),
                'name':     settings.YML_SHOP_NAME,
                'company':  settings.YML_SHOP_COMPANY,
                'url':      settings.YML_SHOP_URL,
                'currency': 'RUR',
                'rate':     '1',
                'domain':   settings.YML_SHOP_URL
            }
        data = get_data()
        products = catalog_models.ShopProduct.objects.order_by('id')
        data['products'] = products
        data['categories'] = catalog_models.ShopCategory.objects.order_by('id')
        with open(os.path.join(settings.MEDIA_ROOT, 'catalog_ya.xml'), 'w') as catalog_yml:
            catalog_yml.write(render_to_string('catalog.xml', data))



#
#
# def filter_products(products):
#     only_available = usersetting(app='yandex', key='only_available')
#     if only_available == 'yes':
#         products = products.filter(Q(available=True) | Q(dropshipping_available=True))
#     only_regular_products = usersetting(app='yandex', key='only_regular_products')
#     if only_regular_products == 'yes':
#         products = products.exclude(code_1c=F('code_dropshipping'))
#     return products

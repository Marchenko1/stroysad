import os

from django.conf import settings
from django.core.cache import cache
from django.db.models import Q, F
from django.core.management import call_command
from django.test import TestCase
from model_mommy import mommy

from bunnylove.apps.catalog import models as catalog_models
from bunnylove.apps.usersettings import models as settings_models
from bunnylove.apps.yandex.management.commands import generate_yml


class GenerateYmlCommandTest(TestCase):

    def setUp(self):
        self.init_data = {
            'name': 'Bunnylove',
            'company': 'ООО "Влюбленный кролик"',
            'url': 'https://bunnylove.ru',
            'currency': 'RUR',
            'rate': '1'
        }
        self.yml_path = os.path.join(settings.MEDIA_ROOT, 'catalog.xml')

    def test_get_data(self):
        reiceved_data = generate_yml.get_data()
        del reiceved_data['date']
        self.assertDictEqual(self.init_data, reiceved_data)

    def test_filter_products(self):
        cache_key_only_available = '{namespace}.{app}.{key}'.format(
            namespace=settings.CACHE_NAMESPACE, app='yandex', key='only_available')
        cache_key_only_regular_products = '{namespace}.{app}.{key}'.format(
            namespace=settings.CACHE_NAMESPACE, app='yandex', key='only_regular_products')

        category1 = catalog_models.Category.objects.create(name='category1', slug='slug-category1-for-yml-command-test')
        category2 = catalog_models.Category.objects.create(name='category2', slug='slug-category2-for-yml-command-test',
                                                           parent=category1)
        category3 = catalog_models.Category.objects.create(name='category3', slug='slug-category3-for-yml-command-test')
        category4 = catalog_models.Category.objects.create(name='category4', slug='slug-category4-for-yml-command-test',
                                                           parent=category3)
        mommy.make(catalog_models.Product, name='p1', slug='slug-p1-for-yml-command-test',
                   categories=[category2], available=True, dropshipping_available=False,
                   code_1c='yml-command-test-1')
        mommy.make(catalog_models.Product, name='p2', slug='slug-p2-for-yml-command-test',
                   categories=[category4], available=False, dropshipping_available=True,
                   code_1c='yml-command-test-2', code_dropshipping='yml-command-test-2')
        mommy.make(catalog_models.Product, name='p3', slug='slug-p3-for-yml-command-test',
                   categories=[category2], available=True, dropshipping_available=False)
        mommy.make(catalog_models.Product, name='p4', slug='slug-p4-for-yml-command-test',
                   categories=[category4], available=False, dropshipping_available=True,
                   code_1c='yml-command-test-4', code_dropshipping='yml-command-test-4')

        products = catalog_models.Product.objects.filter(
            slug__icontains='for-yml-command-test').order_by('id')

        settings_models.Setting.objects.filter(app='yandex', key='only_available').update(value='no')
        settings_models.Setting.objects.filter(app='yandex', key='only_regular_products').update(value='no')
        cache.delete(cache_key_only_available)
        cache.delete(cache_key_only_regular_products)
        self.assertEqual(products, generate_yml.filter_products(products))

        products_expected = products.filter(Q(available=True) | Q(dropshipping_available=True))
        settings_models.Setting.objects.filter(app='yandex', key='only_available').update(value='yes')
        self.assertListEqual(list(products_expected.values_list('name', flat=True)),
                             list(generate_yml.filter_products(products).values_list('name', flat=True)))

        products_expected = products.exclude(code_1c=F('code_dropshipping'))
        settings_models.Setting.objects.filter(app='yandex', key='only_available').update(value='no')
        settings_models.Setting.objects.filter(app='yandex', key='only_regular_products').update(value='yes')
        cache.delete(cache_key_only_available)
        cache.delete(cache_key_only_regular_products)
        self.assertListEqual(list(products_expected.values_list('name', flat=True)),
                             list(generate_yml.filter_products(products).values_list('name', flat=True)))

        products_expected = products.filter(
            Q(available=True) | Q(dropshipping_available=True)).exclude(code_1c=F('code_dropshipping'))
        settings_models.Setting.objects.filter(app='yandex', key='only_available').update(value='yes')
        settings_models.Setting.objects.filter(app='yandex', key='only_regular_products').update(value='yes')
        cache.delete(cache_key_only_available)
        cache.delete(cache_key_only_regular_products)
        self.assertListEqual(list(products_expected.values_list('name', flat=True)),
                             list(generate_yml.filter_products(products).values_list('name', flat=True)))

    def test_create_file(self):
        category1 = catalog_models.Category.objects.create(name='category1', slug='slug-category1-for-yml-command-test')
        category2 = catalog_models.Category.objects.create(name='category2', slug='slug-category2-for-yml-command-test',
                                                           parent=category1)
        category3 = catalog_models.Category.objects.create(name='category3', slug='slug-category3-for-yml-command-test')
        category4 = catalog_models.Category.objects.create(name='category4', slug='slug-category4-for-yml-command-test',
                                                           parent=category3)
        mommy.make(catalog_models.Product, name='p1', slug='slug-p1-for-yml-command-test',
                   categories=[category2], available=True, dropshipping_available=False,
                   code_1c='yml-command-test-1')
        mommy.make(catalog_models.Product, name='p2', slug='slug-p2-for-yml-command-test',
                   categories=[category4], available=False, dropshipping_available=True,
                   code_1c='yml-command-test-2', code_dropshipping='yml-command-test-2')
        mommy.make(catalog_models.Product, name='p3', slug='slug-p3-for-yml-command-test',
                   categories=[category2], available=True, dropshipping_available=False)
        mommy.make(catalog_models.Product, name='p4', slug='slug-p4-for-yml-command-test',
                   categories=[category4], available=False, dropshipping_available=True,
                   code_1c='yml-command-test-4', code_dropshipping='yml-command-test-4')

        call_command('generate_yml')
        self.assertTrue(os.path.exists(self.yml_path))
        os.remove(self.yml_path)

from django.views.generic import TemplateView
from libs.views import CachedViewMixin
from seo.seo import Seo
from .models import PolicyConfig


class IndexView(CachedViewMixin, TemplateView):
    template_name = 'policy/index.html'
    config = None

    def last_modified(self, *args, **kwargs):
        self.config = PolicyConfig.get_solo()
        return self.config.updated

    def get(self, request, *args, tag_slug=None, **kwargs):
        # SEO
        seo = Seo()
        seo.set_data(self.config, defaults={
            'title': self.config.header,
            'og_title': self.config.header,
        })
        seo.save(request)

        return self.render_to_response({
            'config': self.config,
        })

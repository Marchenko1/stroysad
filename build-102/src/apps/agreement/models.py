from django.db import models
from django.shortcuts import resolve_url
from django.utils.translation import ugettext_lazy as _
from solo.models import SingletonModel
from ckeditor.fields import CKEditorUploadField


class AgreementConfig(SingletonModel):
    header = models.CharField(_('header'), max_length=255)
    text = CKEditorUploadField(_('text'))
    updated = models.DateTimeField(_('change date'), auto_now=True)

    class Meta:
        default_permissions = ('change',)
        verbose_name = _('Settings')

    def get_absolute_url(self):
        return resolve_url('agreement:index')

    def __str__(self):
        return self.header

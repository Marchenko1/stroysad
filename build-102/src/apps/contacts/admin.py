from django.contrib import admin
from django.utils.translation import ugettext_lazy as _
from suit.admin import SortableModelAdmin
from solo.admin import SingletonModelAdmin
from project.admin import ModelAdminMixin
from seo.admin import SeoModelAdminMixin
from .models import ContactsConfig, MessageReciever, Address


@admin.register(Address)
class AddressAdmin(ModelAdminMixin, SortableModelAdmin):
    fieldsets = (
        (None, {
            'classes': ('suit-tab', 'suit-tab-general'),
            'fields': (
                'country', 'city', 'address', 'phones', 'email', 'work_hours', 'comment', 'coords'
            )
        }),
    )
    list_display = ('city', 'address', 'comment')
    sortable = 'sort_order'
    suit_form_tabs = (
        ('general', _('General')),
    )

    class Media:
        js = (
            'contacts/admin/js/contacts.js',
        )


class MessageRecieverAdmin(admin.TabularInline):
    model = MessageReciever
    extra = 0
    min_num = 1
    suit_classes = 'suit-tab suit-tab-messages'


@admin.register(ContactsConfig)
class ContactsConfigAdmin(SeoModelAdminMixin, SingletonModelAdmin):
    fieldsets = (
        (None, {
            'fields': ('header', )
        }),
    )
    inlines = (MessageRecieverAdmin, )
    suit_form_tabs = (
        ('messages', _('Messages')),
    )

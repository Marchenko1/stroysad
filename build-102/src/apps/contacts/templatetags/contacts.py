from django.template import Library, loader, RequestContext
from ..forms import ContactForm

register = Library()


@register.simple_tag(takes_context=True)
def contacts_form(context):
    request = context.get('request')
    if not request:
        return ''

    return loader.render_to_string('contacts/form.html', {
        'form': ContactForm(),
    }, context_instance=RequestContext(request))

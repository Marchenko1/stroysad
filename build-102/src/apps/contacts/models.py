from django.db import models
from django.shortcuts import resolve_url
from django.utils.translation import ugettext_lazy as _
from google_maps import GoogleCoordsField
from solo.models import SingletonModel


class ContactsConfig(SingletonModel):
    header = models.CharField(_('header'), max_length=128)
    updated = models.DateTimeField(_('change date'), auto_now=True)

    class Meta:
        verbose_name = _("Settings")

    def get_absolute_url(self):
        return resolve_url('contacts:index')

    @property
    def last_modified(self):
        return max(self.updated, *(item.updated for item in Address.objects.all()))


class MessageReciever(models.Model):
    config = models.ForeignKey(ContactsConfig, related_name='recievers')
    email = models.EmailField(_('e-mail'))

    class Meta:
        verbose_name = _('message reciever')
        verbose_name_plural = _('message recievers')

    def __str__(self):
        return self.email


class Address(models.Model):
    country = models.CharField(_('country'), default=_('Russia'), max_length=255)
    city = models.CharField(_('city'), max_length=255)
    address = models.CharField(_('address'), max_length=255, blank=True)
    phones = models.CharField(_('phones'), max_length=255, blank=True)
    email = models.EmailField(_('e-mail'), blank=True)
    work_hours = models.TextField(_('working hours'), blank=True)
    comment = models.TextField(_('comment'), blank=True)
    coords = GoogleCoordsField(_('coords'), blank=True)
    sort_order = models.PositiveIntegerField(_('sort order'))

    updated = models.DateTimeField(_('change date'), auto_now=True)

    class Meta:
        verbose_name = _('address')
        verbose_name_plural = _('addresses')
        ordering = ('sort_order', )

    def __str__(self):
        return self.city

    def phones_list(self):
        return map(str.strip, self.phones.split(','))

    @property
    def remote_link(self):
        coords = map(str, self.coords)
        coords = ','.join(reversed(tuple(coords)))
        return 'http://maps.google.com/maps?&z=16&q={0}&ll={0}'.format(coords)

(function() {

    $(document).on('change', '#id_country, #id_city, #id_address', function() {
        var gmap = $('#id_coords').next('.google-map').data('gmap');

        var country = $('#id_country').val();
        var city = $('#id_city').val();
        var address = $('#id_address').val();
        if (!address) {
            return
        }

        var full_address = [country, city, address].map($.trim).filter(Boolean).join(', ');

        gmap.geocode(full_address, function(point) {
            var marker = this.markers[0];
            if (marker) {
                marker.position(point);
            } else {
                marker = GMapMarker({
                    map: this,
                    position: point
                })
            }

            marker.trigger('dragend');
            this.panTo(point);
        });
    });

})(jQuery);
(function($) {

    var gmap;
	
    var scrollToMap = function () {
		document.getElementById("yandex-map").scrollIntoView({
		  behavior: 'smooth'
		});
    };
    
    // получение точки по jQuery-объекту адреса
    var pointByAddr = function($address) {
        var addr_data = $address.data();
        return GMapPoint(addr_data.lat, addr_data.lng);
    };

    // показ адреса
    var showAddr = function($address) {
        var point = pointByAddr($address);
        if (point && gmap) {
            gmap.center(point);

            gmap.balloon.close();

            var marker = gmap.markers[0];
            if (marker) {
                marker.position(point);
            }
        }
    };

    // клик на адрес
    $(document).on('click', '.clickable', function() {
        showAddr($(this).parent());
        scrollToMap();
    });


    $(document).ready(function() {
        var $address = $('.clickable').first().parent();
        if ($address.length) {
            gmap = GMap('#google-map', {
                center: pointByAddr($address),
                dblClickZoom: true,
                zoom: 14
            }).on('ready', function() {
                GMapMarker({
                    map: this,
                    position: this.center()
                }).on('click', function() {
                    // открытие окна при клике
                    var map = this.map();
                    var $info = $address.find('.hidden');
                    map.balloon.content($info.html());
                    this.openBalloon();
                });

                // всплывающее окно
                this.balloon = GMapBalloon({
                    map: this
                });

                showAddr($address);
            }).on('resize', function() {
                var marker = this.markers[0];
                if (marker) {
                    this.center(marker);
                }
            });
        }
    });

    var myMap;



})(jQuery);
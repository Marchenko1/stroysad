(function($) {

    /*
        Формы на страницах
     */
    $(document).ready(function() {
        var $contactForm = $('#contacts-request-form');
        $contactForm.ajaxForm({
            beforeSubmit: function(arr, xhr, options) {
                arr.push({
                    name: 'referer',
                    value: location.href
                })
            },
            success: function(response) {
                $contactForm.find('.errors').removeClass();

                if (response.errors) {
                    for(var i=0, l=response.errors.length; i<l; i++) {
                        var err = response.errors[i];
                        var $field = $('.' + err.fullname);
                        $field.addClass(err.class)
                            .find('.error').addClass('visible')
                            .children('span').text(err.errors[0]);
                    }
                } else {
                    $contactForm.get(0).reset()
                }
            }
        });
    });


    /*
        Открытие окна контактов
     */
    $(document).on('click', '.request-popup', function() {
        $.preloader();

        $.ajax({
            url: window.js_storage.ajax_contact,
            type: 'GET',
            success: function(response) {
                $.popup({
                    classes: 'contacts-popup',
                    content: response
                }).show();
                $('#id_phone').mask("+7(999) 999-99-99");
            },
            error: function() {
                alert(gettext('Connection error'));
                $.popup().hide();
            }
        });
        return false;
    });


    /*
        Отправка контактов через AJAX
     */
    $(document).on('submit', '#ajax-request-form', function() {
        var $form = $(this);
        var data = $form.serializeArray();
        $.preloader();

        data.push({
            name: 'referer',
            value: location.href
        });

        console.log(window.js_storage.ajax_contact);

        $.ajax({
            url: window.js_storage.ajax_contact,
            type: 'POST',
            data: data,
            dataType: 'json',
            success: function(response) {
                gtag('event', 'goal', {'event_category': 'form', 'event_action': 'submit'});
                yaCounter42438114.reachGoal('form');

                if (response.form) {
                    $.popup({
                        classes: 'contacts-popup',
                        content: response.form
                    }).show();
                    $('#id_phone').mask("+7(999) 999-99-99");
                } else {
                    $.popup().hide();
                }
            },
            error: function() {
                alert(gettext('Connection error'));
                $.popup().hide();
            }
        });
        return false;
    });

})(jQuery);
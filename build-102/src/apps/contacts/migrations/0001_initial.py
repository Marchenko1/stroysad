# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models
import google_maps.fields


class Migration(migrations.Migration):

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='Address',
            fields=[
                ('id', models.AutoField(primary_key=True, auto_created=True, verbose_name='ID', serialize=False)),
                ('country', models.CharField(max_length=255, verbose_name='country', default='Russia')),
                ('city', models.CharField(max_length=255, verbose_name='city')),
                ('address', models.CharField(blank=True, verbose_name='address', max_length=255)),
                ('phones', models.CharField(blank=True, verbose_name='phones', max_length=255)),
                ('email', models.EmailField(blank=True, verbose_name='e-mail', max_length=254)),
                ('work_hours', models.TextField(blank=True, verbose_name='working hours')),
                ('comment', models.TextField(blank=True, verbose_name='comment')),
                ('coords', google_maps.fields.GoogleCoordsField(blank=True, verbose_name='coords')),
                ('sort_order', models.PositiveIntegerField(verbose_name='sort order')),
                ('updated', models.DateTimeField(verbose_name='change date', auto_now=True)),
            ],
            options={
                'ordering': ('sort_order',),
                'verbose_name': 'address',
                'verbose_name_plural': 'addresses',
            },
        ),
        migrations.CreateModel(
            name='ContactsConfig',
            fields=[
                ('id', models.AutoField(primary_key=True, auto_created=True, verbose_name='ID', serialize=False)),
                ('header', models.CharField(max_length=128, verbose_name='header')),
                ('updated', models.DateTimeField(verbose_name='change date', auto_now=True)),
            ],
            options={
                'verbose_name': 'Settings',
            },
        ),
        migrations.CreateModel(
            name='MessageReciever',
            fields=[
                ('id', models.AutoField(primary_key=True, auto_created=True, verbose_name='ID', serialize=False)),
                ('email', models.EmailField(max_length=254, verbose_name='e-mail')),
                ('config', models.ForeignKey(to='contacts.ContactsConfig', related_name='recievers')),
            ],
            options={
                'verbose_name': 'message reciever',
                'verbose_name_plural': 'message recievers',
            },
        ),
    ]

from django.http.response import Http404
from django.views.generic.base import TemplateView
from libs.views import CachedViewMixin
from seo.seo import Seo
from .models import ContactsConfig, Address


class IndexView(CachedViewMixin, TemplateView):
    config = None
    addresses = None
    template_name = 'contacts/index.html'

    def last_modified(self, request):
        self.config = ContactsConfig.get_solo()
        self.addresses = Address.objects.all()

        if not self.addresses:
            raise Http404


        return self.config.last_modified

    def get(self, request, *args, **kwargs):
        # SEO
        seo = Seo()
        seo.title.clear()
        seo.set_data(self.config, defaults={
            'title': self.config.header,
        })
        seo.save(request)

        return self.render_to_response({
            'config': self.config,
            'addresses': self.addresses,
        })

from django.conf.urls import patterns, url
from . import views, views_ajax


urlpatterns = patterns('',
    # AJAX
    url(r'^form/$', views_ajax.ContactFormView.as_view(), name='ajax_contact'),
    #url(r'^form/$', views.IndexView.as_view(), name='ajax_contact'),
    url(r'^$', views.IndexView.as_view(), name='index'),
)

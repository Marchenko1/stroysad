from django import forms
from django.utils.translation import ugettext_lazy as _
from libs.form_helper.forms import FormHelperMixin


class ContactForm(FormHelperMixin, forms.Form):
    render_error_message = True
    field_template = 'fields/field.html'
    field_templates = {
        'agreement': 'fields/agreement_field.html',
    }

    person = forms.CharField(
        label=_('Contact person'),
        max_length=128,
        error_messages={
            'required': _('Please enter contact person'),
            'max_length': _('Person name should not be longer than %(limit_value)d characters'),
        }
    )

    phone = forms.CharField(
        label=_('Phone'),
        max_length=12,
        error_messages={
            'required': _('Please enter phone number'),
            'max_length': _('Phone number should not be longer than %(limit_value)d characters'),
        }
    )

    agreement = forms.BooleanField(
        required=True,
        label=_('Agreement'),
        initial=True,
    )

from django.http.response import JsonResponse
from django.views.generic.edit import FormView
from django.utils.translation import ugettext_lazy as _
from libs.email import send
from libs.views_ajax import AjaxViewMixin
from .forms import ContactForm
from .models import MessageReciever


class ContactFormView(AjaxViewMixin, FormView):
    form_class = ContactForm
    template_name = 'contacts/ajax_form.html'

    def form_valid(self, form):
        recievers = MessageReciever.objects.all().values_list('email', flat=True)
        if recievers:
            send(self.request, recievers,
                subject=_('Message from {domain}'),
                template='contacts/email.html',
                context={
                    'data': form.cleaned_data,
                    'referer': self.request.POST.get('referer'),
                }
            )

        return JsonResponse({})

    def form_invalid(self, form):
        return JsonResponse({
            'errors': form.error_dict,
            'form': self.render_to_string('contacts/ajax_form.html', {
                'form': form,
            }),
        })

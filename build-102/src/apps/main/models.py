from django.db import models
from django.shortcuts import resolve_url
from django.utils.translation import ugettext_lazy as _, ugettext
from solo.models import SingletonModel
from attachable_blocks import AttachableBlockField
from blocks.models import SliderBlock


class MainPageConfig(SingletonModel):
    slider = AttachableBlockField(SliderBlock, verbose_name=_('slider block'), null=True, blank=True)

    updated = models.DateTimeField(_('change date'), auto_now=True)

    class Meta:
        verbose_name = _("Settings")

    def get_absolute_url(self):
        return resolve_url('index')

    def __str__(self):
        return ugettext('Main page')

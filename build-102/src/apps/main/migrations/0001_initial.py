# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models
import attachable_blocks.fields


class Migration(migrations.Migration):

    dependencies = [
        ('blocks', '__first__'),
    ]

    operations = [
        migrations.CreateModel(
            name='MainPageConfig',
            fields=[
                ('id', models.AutoField(primary_key=True, auto_created=True, verbose_name='ID', serialize=False)),
                ('updated', models.DateTimeField(verbose_name='change date', auto_now=True)),
                ('slider', attachable_blocks.fields.AttachableBlockField(blank=True, verbose_name='slider block', null=True, to='blocks.SliderBlock')),
            ],
            options={
                'verbose_name': 'Settings',
            },
        ),
    ]

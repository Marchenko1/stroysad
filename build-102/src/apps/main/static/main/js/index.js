(function($) {

    $(document).ready(function() {
        var order_complete = /#order_complete/gi.exec(location.hash);
        if (order_complete) {
            $.ajax({
                url: window.js_storage.order_complete,
                type: 'GET',
                success: function(response) {
                    $.popup({
                        classes: 'contacts-popup',
                        content: response

                    }).show();
                },
                error: function() {
                    alert(gettext('Connection error'));
                    $.popup().hide();
                }
            });
            return false;
        }
    });

})(jQuery);

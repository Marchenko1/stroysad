from django.contrib import admin
from django.utils.translation import ugettext_lazy as _
from solo.admin import SingletonModelAdmin
from seo.admin import SeoModelAdminMixin
from attachable_blocks import AttachedBlocksStackedInline
from .models import MainPageConfig


class MainPageBlocks(AttachedBlocksStackedInline):
    suit_classes = 'suit-tab suit-tab-blocks'


@admin.register(MainPageConfig)
class MainPageConfigAdmin(SeoModelAdminMixin, SingletonModelAdmin):
    fieldsets = (
        (None, {
            'classes': ('suit-tab', 'suit-tab-general'),
            'fields': ('slider', )
        }),
    )
    inlines = (MainPageBlocks, )
    suit_form_tabs = (
        ('general', _('General')),
        ('blocks', _('Blocks')),
    )

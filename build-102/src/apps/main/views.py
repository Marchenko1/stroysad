from django.views.generic.base import TemplateView
from seo.seo import Seo
from shop.models import ShopConfig, ShopCategory, DiscountConfig
from .models import MainPageConfig
from blog.models import BlogConfig, BlogPost, Tag


class IndexView(TemplateView):
    config = None
    shop_config = None
    template_name = 'main/index.html'

    def get(self, request, *args, **kwargs):
        self.config = MainPageConfig.get_solo()
        self.shop_config = ShopConfig.get_solo()

        # SEO
        seo = Seo()
        seo.set_data(self.config)
        seo.save(request)

        return self.render_to_response({
            'config': self.config,
            'root_categories': ShopCategory.objects.root_categories(),
            'shop_config': self.shop_config,
            'discount_config': DiscountConfig.get_solo(),
            'posts': BlogPost.objects.filter(visible=True).order_by('-id')[:3]
        })

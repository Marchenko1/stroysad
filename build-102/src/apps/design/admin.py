from django.contrib import admin
from django.utils.translation import ugettext_lazy as _
from solo.admin import SingletonModelAdmin
from seo.admin import SeoModelAdminMixin
from social_networks.admin import AutoPostMixin
from .models import DesignConfig


@admin.register(DesignConfig)
class DesignConfigAdmin(SeoModelAdminMixin, AutoPostMixin, SingletonModelAdmin):
    fieldsets = (
        (None, {
            'classes': ('suit-tab', 'suit-tab-general'),
            'fields': (
                'header', 'background', 'background_alt',
            ),
        }),
        (_('Content'), {
            'classes': ('suit-tab', 'suit-tab-general'),
            'fields': (
                'text',
            ),
        }),
    )
    suit_form_tabs = (
        ('general', _('General')),
    )

    def get_autopost_text(self, obj):
        return obj.text

from django.db import models
from django.conf import settings
from django.shortcuts import resolve_url
from django.utils.translation import ugettext_lazy as _
from solo.models import SingletonModel
from ckeditor import CKEditorField
from libs.stdimage.fields import StdImageField
from libs.storages.media_storage import MediaStorage


class DesignConfig(SingletonModel):
    header = models.CharField(_('header'), max_length=255)
    background = StdImageField(_('background'),
        storage=MediaStorage('design/background'),
        min_dimensions=(1400, 800),
        admin_variation='admin',
        variations=dict(
            desktop=dict(
                size=(1920, 0),
                stretch=True,
            ),
            tablet=dict(
                size=(1200, 0),
                crop=False,
            ),
            mobile=dict(
                size=(768, 0),
                crop=False,
            ),
            admin=dict(
                size=(320, 182),
            ),
        ),
    )
    background_alt = models.CharField(_('alt'), max_length=255, blank=True)
    text = CKEditorField(_('text'), editor_options=settings.CKEDITOR_CONFIG_MINI)

    updated = models.DateTimeField(_('change date'), auto_now=True)

    class Meta:
        verbose_name = _("Settings")

    def get_absolute_url(self):
        return resolve_url('design:index')

    def __str__(self):
        return self.header

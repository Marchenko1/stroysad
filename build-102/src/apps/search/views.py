from django.views.generic.base import TemplateView
from django.http.response import HttpResponseBadRequest
from django.utils.translation import ugettext_lazy as _
from paginator import Paginator
from seo.seo import Seo
from libs.cache import cached
from libs.views import StringRenderMixin
from libs.sphinx.search import SphinxSearch, SearchError
from .forms import SearchForm


class ShopProductSearch(SphinxSearch):
    index = 'shopdvor_products'
    order_by = '@relevance DESC, is_hot DESC, @id DESC'
    weights = {
        'title': 2,
        'description': 1,
    }

    def shopdvor_products_queryset(self, model, ids):
        return model.objects.filter(pk__in=ids).select_related(
            'category', 'producer', 'discount'
        ).with_photos()


class SearchPaginator(Paginator):
    search_class = ShopProductSearch

    @cached('self.query', 'self.search_class.index', time=10*60)
    def item_count(self):
        try:
            self._fictive = self.searcher.fetch(self.query, limit=0)
        except SearchError:
            return 0
        else:
            return self._fictive.total

    def __init__(self, *args, query=None, **kwargs):
        self.query = query
        self.searcher = self.search_class()
        kwargs['object_list'] = range(self.item_count())
        super().__init__(*args, **kwargs)

    def __bool__(self):
        return self.item_count() > 0

    def page(self, number):
        number = self.validate_number(number)
        bottom = (number - 1) * self.per_page

        try:
            items = self.searcher.fetch_models(self.query, offset=bottom, limit=self.per_page)
        except SearchError:
            items = ()

        return self._get_page(items, number, self)


class SearchView(StringRenderMixin, TemplateView):
    template_name = 'search/result.html'

    # @cached(time=15 * 60)
    def get_related(self):
        products = ShopProductSearch().fetch_models('', limit=6)
        if len(products) < 6:
            return ''

        return self.render_to_string('search/_related.html', {
            'products': products,
        })

    def get(self, request, **kwargs):
        form = SearchForm(request.GET)
        if not form.is_valid():
            raise HttpResponseBadRequest

        query = form.cleaned_data.get('q')
        paginator = SearchPaginator(
            request,
            query=query,
            per_page=60,
            page_neighbors=1,
            side_neighbors=1,
        )

        # SEO
        seo = Seo()
        seo.title = _('Search products')
        seo.save(request)

        if paginator:
            context = {
                'form': form,
                'title': _('Search results for «%s»') % query,
                'paginator': paginator,
            }
        else:
            context = {
                'form': form,
                'title': _('We were unable to find results for «%s»') % query,
                'related': self.get_related(),
            }

        return self.render_to_response(context)

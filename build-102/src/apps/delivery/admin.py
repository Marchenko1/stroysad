from django.contrib import admin
from django.utils.translation import ugettext_lazy as _
from solo.admin import SingletonModelAdmin
from suit.admin import SortableStackedInline
from project.admin import ModelAdminInlineMixin
from seo.admin import SeoModelAdminMixin
from .models import DeliveryConfig, PayAdvantage, DeliveryAdvantage


class PayAdvantagesAdmin(ModelAdminInlineMixin, SortableStackedInline):
    model = PayAdvantage
    extra = 0
    sortable = 'sort_order'
    suit_classes = 'suit-tab suit-tab-advantages'


class DeliveryAdvantageAdmin(ModelAdminInlineMixin, SortableStackedInline):
    model = DeliveryAdvantage
    extra = 0
    sortable = 'sort_order'
    suit_classes = 'suit-tab suit-tab-advantages'


@admin.register(DeliveryConfig)
class DeliveryConfigAdmin(SeoModelAdminMixin, SingletonModelAdmin):
    fieldsets = (
        (None, {
            'classes': ('suit-tab', 'suit-tab-general'),
            'fields': (
                'header', 'background', 'background_alt',
            ),
        }),
        (_('Payment'), {
            'classes': ('suit-tab', 'suit-tab-general'),
            'fields': (
                'pay_title', 'pay_text',
            ),
        }),
        (_('Delivery'), {
            'classes': ('suit-tab', 'suit-tab-general'),
            'fields': (
                'delivery_title', 'delivery_text',
            ),
        }),
    )
    inlines = (PayAdvantagesAdmin, DeliveryAdvantageAdmin)
    suit_form_tabs = (
        ('general', _('General')),
        ('advantages', _('Advantages')),
    )

# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('delivery', '0001_initial'),
    ]

    operations = [
        migrations.AddField(
            model_name='deliveryconfig',
            name='background_alt',
            field=models.CharField(blank=True, max_length=255, verbose_name='alt'),
        ),
    ]

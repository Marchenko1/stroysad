from django.db import models
from django.conf import settings
from django.shortcuts import resolve_url
from django.utils.translation import ugettext_lazy as _
from solo.models import SingletonModel
from ckeditor import CKEditorField
from libs.sprite_image import SpriteImageField
from libs.stdimage.fields import StdImageField
from libs.storages.media_storage import MediaStorage


class DeliveryConfig(SingletonModel):
    header = models.CharField(_('header'), max_length=255)
    background = StdImageField(_('background'),
        storage=MediaStorage('delivery/background'),
        min_dimensions=(1400, 800),
        admin_variation='admin',
        variations=dict(
            desktop=dict(
                size=(1920, 0),
                stretch=True,
            ),
            tablet=dict(
                size=(1200, 0),
                crop=False,
            ),
            mobile=dict(
                size=(768, 0),
                crop=False,
            ),
            admin=dict(
                size=(320, 182),
            ),
        ),
    )
    background_alt = models.CharField(_('alt'), max_length=255, blank=True)

    pay_title = models.CharField(_('title'), max_length=255)
    pay_text = CKEditorField(_('text'), editor_options=settings.CKEDITOR_CONFIG_MINI)

    delivery_title = models.CharField(_('title'), max_length=255)
    delivery_text = CKEditorField(_('text'), editor_options=settings.CKEDITOR_CONFIG_MINI)

    updated = models.DateTimeField(_('change date'), auto_now=True)

    class Meta:
        verbose_name = _("Settings")

    def get_absolute_url(self):
        return resolve_url('delivery:index')

    def __str__(self):
        return self.header


class PayAdvantage(models.Model):
    ICONS = settings.SPRITE_ICONS

    page = models.ForeignKey(DeliveryConfig, verbose_name=_('page'), related_name='pay_advantages')
    title = models.CharField(_('title'), max_length=128)
    icon = SpriteImageField(_('icon'),
        sprite='img/sprite.svg',
        size=(50, 50),
        choices=ICONS,
        default=ICONS[0][0],
    )
    sort_order = models.PositiveIntegerField(_('sort order'))

    class Meta:
        verbose_name = _('payment advantage')
        verbose_name_plural = _('payment advantages')
        ordering = ('sort_order',)

    def __str__(self):
        return self.title


class DeliveryAdvantage(models.Model):
    ICONS = settings.SPRITE_ICONS

    page = models.ForeignKey(DeliveryConfig, verbose_name=_('page'), related_name='delivery_advantages')
    title = models.CharField(_('title'), max_length=128)
    icon = SpriteImageField(_('icon'),
        sprite='img/sprite.svg',
        size=(50, 50),
        choices=ICONS,
        default=ICONS[0][0],
    )
    sort_order = models.PositiveIntegerField(_('sort order'))

    class Meta:
        verbose_name = _('delivery advantage')
        verbose_name_plural = _('delivery advantages')
        ordering = ('sort_order',)

    def __str__(self):
        return self.title

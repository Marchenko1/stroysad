from django.template import loader, Library
from ..models import TestimonialsConfig, Testimonial

register = Library()


@register.simple_tag
def testimonials_block():
    testimonials = Testimonial.objects.all().order_by('?')[:3]
    if not testimonials:
        return ''

    config = TestimonialsConfig.get_solo()

    return loader.render_to_string('testimonials/block.html', {
        'config': config,
        'testimonials': testimonials,
    })

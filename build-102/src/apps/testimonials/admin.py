from django.contrib import admin
from django.utils.translation import ugettext_lazy as _
from solo.admin import SingletonModelAdmin
from seo.admin import SeoModelAdminMixin
from .models import TestimonialsConfig, Testimonial
from project.admin import ModelAdminInlineMixin
from suit.admin import SortableStackedInline


class TestimonialsInline(ModelAdminInlineMixin, SortableStackedInline):
    model = Testimonial
    extra = 0
    max_num = 10
    suit_classes = 'suit-tab suit-tab-general'
    sortable = 'sort_order'


@admin.register(TestimonialsConfig)
class TestimonialsConfigAdmin(SeoModelAdminMixin, SingletonModelAdmin):
    fieldsets = (
        (None, {
            'classes': ('suit-tab', 'suit-tab-general'),
            'fields': (
                'header',
            ),
        }),
    )
    inlines = (TestimonialsInline, )
    suit_form_tabs = (
        ('general', _('General')),
    )

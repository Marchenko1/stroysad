# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models
import libs.storages.media_storage
import libs.stdimage.fields


class Migration(migrations.Migration):

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='Testimonial',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, primary_key=True, auto_created=True)),
                ('photo', libs.stdimage.fields.StdImageField(verbose_name='photo', storage=libs.storages.media_storage.MediaStorage('testimonials'), aspects=('normal',), variations={'admin': {'size': (160, 160)}, 'normal': {'size': (160, 160)}}, upload_to='', min_dimensions=(160, 160))),
                ('name', models.CharField(verbose_name='name', max_length=255)),
                ('position', models.CharField(verbose_name='position', max_length=255)),
                ('text', models.TextField(verbose_name='text')),
                ('sort_order', models.PositiveIntegerField(verbose_name='order', default=0)),
                ('updated', models.DateTimeField(verbose_name='change date', auto_now=True)),
            ],
            options={
                'verbose_name': 'testimonial',
                'verbose_name_plural': 'testimonials',
                'ordering': ('sort_order',),
            },
        ),
        migrations.CreateModel(
            name='TestimonialsConfig',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, primary_key=True, auto_created=True)),
                ('header', models.CharField(verbose_name='header', blank=True, max_length=255)),
                ('updated', models.DateTimeField(verbose_name='change date', auto_now=True)),
            ],
            options={
                'verbose_name': 'settings',
            },
        ),
        migrations.AddField(
            model_name='testimonial',
            name='config',
            field=models.ForeignKey(to='testimonials.TestimonialsConfig'),
        ),
    ]

# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models
import libs.stdimage.fields
import libs.storages.media_storage


class Migration(migrations.Migration):

    dependencies = [
        ('testimonials', '0001_initial'),
    ]

    operations = [
        migrations.AlterField(
            model_name='testimonial',
            name='photo',
            field=libs.stdimage.fields.StdImageField(aspects=('normal',), upload_to='', min_dimensions=(64, 64), variations={'normal': {'size': (64, 64)}}, verbose_name='photo', storage=libs.storages.media_storage.MediaStorage('testimonials')),
        ),
    ]

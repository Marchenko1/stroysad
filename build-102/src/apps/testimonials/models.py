from django.db import models
from django.utils.translation import ugettext_lazy as _
from solo.models import SingletonModel
from libs.storages import MediaStorage
from libs.stdimage.fields import StdImageField


class TestimonialsConfig(SingletonModel):
    header = models.CharField(_('header'), blank=True, max_length=255)

    updated = models.DateTimeField(_('change date'), auto_now=True)

    class Meta:
        verbose_name = _('settings')

    def __str__(self):
        return self.header


class Testimonial(models.Model):
    config = models.ForeignKey(TestimonialsConfig)

    photo = StdImageField(_('photo'),
        storage=MediaStorage('testimonials'),
        min_dimensions=(64, 64),
        admin_variation='normal',
        crop_area=True,
        aspects=('normal',),
        variations=dict(
            normal=dict(
                size=(64, 64),
            ),
        ),
    )
    name = models.CharField(_('name'), max_length=255)
    position = models.CharField(_('position'), max_length=255)
    text = models.TextField(_('text'))

    sort_order = models.PositiveIntegerField(_('order'), default=0)
    updated = models.DateTimeField(_('change date'), auto_now=True)

    class Meta:
        verbose_name = _('testimonial')
        verbose_name_plural = _('testimonials')
        ordering = ('sort_order', )

    def __str__(self):
        return self.name

# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models
import libs.stdimage.fields
import libs.storages.media_storage


class Migration(migrations.Migration):

    dependencies = [
        ('blog', '0002_auto_20160922_1303'),
    ]

    operations = [
        migrations.AlterField(
            model_name='blogpost',
            name='preview',
            field=libs.stdimage.fields.StdImageField(min_dimensions=(1920, 400), verbose_name='preview', variations={'admin': {'size': (450, 250)}, 'mobile': {'size': (540, 300)}, 'small': {'size': (570, 390)}, 'normal': {'size': (1920, 400)}}, storage=libs.storages.media_storage.MediaStorage('blog/preview'), upload_to='', aspects=('normal',), blank=True),
        ),
    ]

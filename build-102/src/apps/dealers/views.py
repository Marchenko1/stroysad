from django.views.generic.base import TemplateView
from libs.views import CachedViewMixin
from seo.seo import Seo
from .models import DealersConfig


class IndexView(CachedViewMixin, TemplateView):
    config = None
    template_name = 'dealers/index.html'

    def last_modified(self, *args, **kwargs):
        self.config = DealersConfig.get_solo()
        return self.config.updated

    def get(self, request, *args, **kwargs):
        # SEO
        seo = Seo()
        seo.title.clear()
        seo.set_data(self.config, defaults={
            'title': self.config.header,
        })
        seo.save(request)

        return self.render_to_response({
            'config': self.config,
        })

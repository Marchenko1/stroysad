from django.contrib import admin
from django.utils.translation import ugettext_lazy as _
from solo.admin import SingletonModelAdmin
from suit.admin import SortableStackedInline
from project.admin import ModelAdminInlineMixin
from seo.admin import SeoModelAdminMixin
from social_networks.admin import AutoPostMixin
from .models import DealersConfig, Advantage


class AdvantagesAdmin(ModelAdminInlineMixin, SortableStackedInline):
    model = Advantage
    extra = 0
    sortable = 'sort_order'
    suit_classes = 'suit-tab suit-tab-advantages'


@admin.register(DealersConfig)
class DealersConfigAdmin(SeoModelAdminMixin, AutoPostMixin, SingletonModelAdmin):
    fieldsets = (
        (None, {
            'classes': ('suit-tab', 'suit-tab-general'),
            'fields': (
                'header', 'description', 'background', 'background_alt',
            ),
        }),
        (_('Content'), {
            'classes': ('suit-tab', 'suit-tab-general'),
            'fields': (
                'text',
            ),
        }),
    )
    inlines = (AdvantagesAdmin, )
    suit_form_tabs = (
        ('general', _('General')),
        ('advantages', _('Advantages')),
    )

    def get_autopost_text(self, obj):
        return obj.text

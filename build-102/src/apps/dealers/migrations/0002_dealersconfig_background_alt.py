# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('dealers', '0001_initial'),
    ]

    operations = [
        migrations.AddField(
            model_name='dealersconfig',
            name='background_alt',
            field=models.CharField(verbose_name='alt', max_length=255, blank=True),
        ),
    ]

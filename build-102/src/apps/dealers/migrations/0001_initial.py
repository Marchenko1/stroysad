# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models
import ckeditor.fields
import libs.sprite_image.fields
import libs.storages
import libs.stdimage.fields


class Migration(migrations.Migration):

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='Advantage',
            fields=[
                ('id', models.AutoField(primary_key=True, auto_created=True, verbose_name='ID', serialize=False)),
                ('title', models.CharField(max_length=128, verbose_name='title')),
                ('icon', libs.sprite_image.fields.SpriteImageField(verbose_name='icon', size=(50, 50), default='timer', choices=[('timer', (-1, -135)), ('drops', (-53, -135)), ('gears', (-105, -135)), ('waves', (-157, -135)), ('carrot', (-209, -135)), ('lift', (-261, -135)), ('home', (-313, -135)), ('cards', (-365, -135)), ('blank', (-417, -135)), ('truck', (-469, -135)), ('cash', (-521, -135)), ('coins', (-573, -135)), ('operator', (-625, -135)), ('ruler', (-677, -135)), ('bulb', (-729, -135)), ('box', (-781, -135))], sprite='img/sprite.svg')),
                ('sort_order', models.PositiveIntegerField(verbose_name='sort order')),
            ],
            options={
                'ordering': ('sort_order',),
                'verbose_name': 'advantage',
                'verbose_name_plural': 'advantages',
            },
        ),
        migrations.CreateModel(
            name='DealersConfig',
            fields=[
                ('id', models.AutoField(primary_key=True, auto_created=True, verbose_name='ID', serialize=False)),
                ('header', models.CharField(max_length=255, verbose_name='header')),
                ('background', libs.stdimage.fields.StdImageField(storage=libs.storages.MediaStorage('dealers/background'), verbose_name='background', aspects=(), upload_to='', variations={'admin': {'size': (320, 107)}, 'desktop': {'stretch': True, 'size': (1920, 0)}, 'tablet': {'crop': False, 'size': (1200, 0)}, 'mobile': {'crop': False, 'size': (768, 0)}}, min_dimensions=(1400, 500))),
                ('description', ckeditor.fields.CKEditorField(verbose_name='description')),
                ('text', ckeditor.fields.CKEditorField(verbose_name='text')),
                ('updated', models.DateTimeField(verbose_name='change date', auto_now=True)),
            ],
            options={
                'verbose_name': 'Settings',
            },
        ),
        migrations.AddField(
            model_name='advantage',
            name='page',
            field=models.ForeignKey(verbose_name='page', to='dealers.DealersConfig', related_name='advantages'),
        ),
    ]

from django.views.generic.base import TemplateView
from libs.views import CachedViewMixin
from seo.seo import Seo
from .models import TrainingConfig


class IndexView(CachedViewMixin, TemplateView):
    config = None
    template_name = 'training/index.html'

    def last_modified(self, request):
        self.config = TrainingConfig.get_solo()
        return self.config.updated

    def get(self, request, *args, **kwargs):
        # SEO
        seo = Seo()
        seo.title.clear()
        seo.set_data(self.config, defaults={
            'title': self.config.header,
        })
        seo.save(request)

        return self.render_to_response({
            'config': self.config,
        })

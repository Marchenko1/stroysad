# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models
import ckeditor.fields
import libs.stdimage.fields
import libs.storages


class Migration(migrations.Migration):

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='TrainingConfig',
            fields=[
                ('id', models.AutoField(primary_key=True, auto_created=True, verbose_name='ID', serialize=False)),
                ('header', models.CharField(max_length=255, verbose_name='header')),
                ('background', libs.stdimage.fields.StdImageField(storage=libs.storages.MediaStorage('training/background'), verbose_name='background', aspects=(), upload_to='', variations={'admin': {'size': (320, 182)}, 'desktop': {'stretch': True, 'size': (1920, 0)}, 'tablet': {'crop': False, 'size': (1200, 0)}, 'mobile': {'crop': False, 'size': (768, 0)}}, min_dimensions=(1400, 800))),
                ('text', ckeditor.fields.CKEditorField(verbose_name='text')),
                ('updated', models.DateTimeField(verbose_name='change date', auto_now=True)),
            ],
            options={
                'verbose_name': 'Settings',
            },
        ),
    ]

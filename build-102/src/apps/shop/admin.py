﻿from django import forms
from django.db import models
from django.contrib import admin
from django.utils.timezone import now
from django.db.models import F, Func, Value
from django.db.models.functions import Concat
from django.contrib.admin.utils import unquote
from django.utils.translation import ugettext_lazy as _
from django.contrib.admin.filters import SimpleListFilter
from project.filters import HierarchyFilter
from project.admin import ModelAdminMixin, ModelAdminInlineMixin
from seo.admin import SeoModelAdminMixin
from solo.admin import SingletonModelAdmin
from social_networks.admin import AutoPostMixin
from attachable_blocks import AttachedBlocksStackedInline
from libs.mptt import *
from libs import admin_utils
from libs.autocomplete import AutocompleteWidget
from .models import (
    ShopConfig, ShopWarehouse, ShopCategory, ShopProduct, ShopProductProducer, OrderTypeOption,
    ShopOrder, EmailReciever, Manual, ShopProducerDescription, Discount, DiscountConfig, PostCategory
)
from .signals import products_changed, categories_changed
from suit.admin import SortableStackedInline
from files.admin import PageFileInlineMixin


class ShopConfigBlocksInline(AttachedBlocksStackedInline):
    suit_classes = 'suit-tab suit-tab-blocks'


class PostCategoryAdmin(ModelAdminInlineMixin, admin.TabularInline):
    model = PostCategory
    extra = 0
    min_num = 0
    suit_classes = 'suit-tab suit-tab-posts'


class EmailRecieverAdmin(ModelAdminInlineMixin, admin.TabularInline):
    model = EmailReciever
    extra = 0
    suit_classes = 'suit-tab suit-tab-managers'


class OrderTypeOptionInline(ModelAdminInlineMixin, admin.TabularInline):
    model = OrderTypeOption
    extra = 0
    suit_classes = 'suit-tab suit-tab-order_type'


class ProducerDescriptionInline(ModelAdminInlineMixin, admin.StackedInline):
    model = ShopProducerDescription
    fieldsets = (
        (None, {
            'fields': (
                'producer',
            ),
        }),
        (None, {
            'fields': (
                'title_producer',
            ),
        }),
        (None, {
            'fields': (
                'description',
            ),
        }),
        (_('META'), {
            'fields': (
                'meta_title', 'meta_description',
            ),
        }),
    )
    extra = 0
    suit_classes = 'suit-tab suit-tab-producer_description'


class ManualInline(PageFileInlineMixin, SortableStackedInline):
    model = Manual


@admin.register(ShopConfig)
class ShopConfigAdmin(SeoModelAdminMixin, AutoPostMixin, SingletonModelAdmin):
    fieldsets = (
        (None, {
            'classes': ('suit-tab', 'suit-tab-general'),
            'fields': (
                'header', 'title', 'description', 'online_payment_limit',
            ),
        }),
        (_('Confirm texts'), {
            'classes': ('suit-tab', 'suit-tab-general'),
            'fields': (
                'payment_text',
            ),
        }),
        (_('Boxberry'), {
            'classes': ('suit-tab', 'suit-tab-order_type'),
            'fields': (
                'boxberry_weight',
            ),
        }),
    )
    inlines = (ShopConfigBlocksInline, OrderTypeOptionInline, EmailRecieverAdmin)
    suit_form_tabs = (
        ('general', _('General')),
        ('order_type', _('Order type settings')),
        ('managers', _('Managers')),
        ('blocks', _('Blocks')),
    )

    def get_autopost_text(self, obj):
        return ''


@admin.register(ShopWarehouse)
class ShopWarehouseAdmin(ModelAdminMixin, admin.ModelAdmin):
    list_display = ('__str__', 'city', 'address', 'active')


class ShopCategoryForm(forms.ModelForm):
    class Meta:
        model = ShopCategory
        fields = '__all__'
        widgets = {
            'parent': AutocompleteWidget(
                format_item=ShopCategory.autocomplete_item,
                attrs={
                    'style': 'width:50%',
                }
            )
        }

    def __init__(self, *args, **kwargs):
        """ Exclude self """
        super().__init__(*args, **kwargs)
        if self.instance.pk:
            self.fields['parent'].queryset = self.fields['parent'].queryset.exclude(pk=self.instance.pk)


@admin.register(ShopCategory)
class ShopCategoryAdmin(SeoModelAdminMixin, AutoPostMixin, SortableMPTTModelAdmin):
    fieldsets = (
        (None, {
            'classes': ('suit-tab', 'suit-tab-general'),
            'fields': (
                'parent',
            ),
        }),
        (None, {
            'classes': ('suit-tab', 'suit-tab-general'),
            'fields': (
                'title', 'alias', 'title_desc', 'is_visible', 'header', 'short_description',
                'description', 'text_additional', 'video', 'video_title',
            ),
        }),
        (_('Shop main'), {
            'classes': ('suit-tab', 'suit-tab-general'),
            'fields': (
                'shop_line', 'shop_main_visible',
            ),
        }),
        (_('Discount'), {
            'classes': ('suit-tab', 'suit-tab-general'),
            'fields': (
                'discount',
            ),
        }),
    )
    mptt_level_indent = 20
    form = ShopCategoryForm
    actions = ('action_hide', 'action_show')
    list_display = ('view', 'title', 'is_visible', 'product_count', 'total_product_count')
    list_display_links = ('title',)
    prepopulated_fields = {'alias': ('title',)}
    sortable = 'sort_order'
    inlines = (
        PostCategoryAdmin,
        ShopConfigBlocksInline,
        ProducerDescriptionInline,
    )
    suit_form_tabs = (
        ('general', _('General')),
        ('producer_description', _('producers description')),
        ('posts', _('posts')),
    )

    def get_autopost_text(self, obj):
        return obj.description

    def action_hide(self, request, queryset):
        descentants = queryset.get_descendants(include_self=True)
        descentants.filter(
            is_visible=True
        ).update(
            is_visible=False
        )
        descentants.update(total_product_count=0)
        categories_changed.send(self.model, categories=queryset, include_self=False)
    action_hide.short_description = _('Hide selected %(verbose_name_plural)s')

    def action_show(self, request, queryset):
        queryset.get_ancestors(include_self=True).filter(
            is_visible=False
        ).update(
            is_visible=True
        )

        categories_changed.send(self.model, categories=queryset)

    action_show.short_description = _('Show selected %(verbose_name_plural)s')


class StatusShopProductCategoryFilter(SimpleListFilter):
    """ Фильтр по категории """
    title = _('Category')
    parameter_name = 'category'

    def lookups(self, request, model_admin):
        return ShopCategory.objects.annotate(
            text=Concat(
                Func(
                    Value('–'),
                    F('level'),
                    function='REPEAT',
                    output_field=models.CharField()
                ),
                F('title'),
            )
        ).values_list('id', 'text')

    def queryset(self, request, queryset):
        value = self.value()
        if value:
            category = ShopCategory.objects.filter(pk=value)
            categories = ShopCategory.objects.get_queryset_descendants(category, include_self=True)
            queryset = queryset.filter(category__in=categories).distinct()
        return queryset


@admin.register(ShopProductProducer)
class ShopProductProducerAdmin(ModelAdminMixin, admin.ModelAdmin):
    fieldsets = (
        (None, {
            'fields': (
                'title', 'alias',
            ),
        }),
        (_('Discount'), {
            'fields': (
                'discount',
            ),
        }),
    )


class ShopProductForm(forms.ModelForm):
    class Meta:
        model = ShopProduct
        fields = '__all__'
        widgets = {
            'category': AutocompleteWidget(
                format_item=ShopCategory.autocomplete_item,
                attrs={
                    'style': 'width:50%',
                }
            ),
            'producer': AutocompleteWidget(
                attrs={
                    'style': 'width:50%',
                }
            ),
            'discount': AutocompleteWidget(
                attrs={
                    'style': 'width:50%',
                }
            ),
        }


@admin.register(ShopProduct)
class ShopProductAdmin(SeoModelAdminMixin, AutoPostMixin, admin.ModelAdmin):
    fieldsets = (
        (_('Place'), {
            'classes': ('suit-tab', 'suit-tab-general'),
            'fields': (
                'category', 'producer',
            ),
        }),
        (_('General'), {
            'classes': ('suit-tab', 'suit-tab-general'),
            'fields': (
                'title', 'alias', 'serial', 'price',
            ),
        }),
        (_('Gallery & Text'), {
            'classes': ('suit-tab', 'suit-tab-general'),
            'fields': (
                'gallery', 'description',
            ),
        }),
        (_('Video'), {
            'classes': ('suit-tab', 'suit-tab-general'),
            'fields': (
                'video_title', 'video',
            ),
        }),
        (_('Addition'), {
            'classes': ('suit-tab', 'suit-tab-general'),
            'fields': (
                'is_visible', 'is_hot',
            ),
        }),
        (_('Discount'), {
            'classes': ('suit-tab', 'suit-tab-general'),
            'fields': (
                'discount',
            ),
        }),
    )
    form = ShopProductForm
    actions = ('action_hide', 'action_show', 'action_set_hot', 'action_unset_hot')
    list_display = (
        'view', 'micropreview', '__str__', 'category_link',
        'price_alternate', 'is_hot', 'is_visible',
    )
    search_fields = ('title', 'category__title', 'serial')
    list_display_links = ('micropreview', '__str__', )
    list_filter = (StatusShopProductCategoryFilter, )
    prepopulated_fields = {
        'alias': ('title', ),
        'serial': ('title', ),
    }
    inlines = (ManualInline,)
    suit_form_tabs = (
        ('general', _('General')),
    )

    def get_autopost_text(self, obj):
        return obj.description

    def micropreview(self, obj):
        if not obj.photo:
            return '-//-'
        return '<img src="{}">'.format(obj.photo.admin_micro.url)
    micropreview.short_description = _('Preview')
    micropreview.allow_tags = True

    def category_link(self, obj):
        meta = getattr(obj.category, '_meta')
        return '<a href="{0}">{1}</a>'.format(
            admin_utils.get_change_url(meta.app_label, meta.model_name, obj.category.pk),
            obj.category
        )
    category_link.short_description = _('Category')
    category_link.allow_tags = True

    def price_alternate(self, obj):
        return '<nobr>%s</nobr>' % obj.price.alternative
    price_alternate.allow_tags = True
    price_alternate.short_description = _('Price')

    def action_hide(self, request, queryset):
        queryset.update(is_visible=False)
        categories = queryset.values_list(
            'category_id', flat=True
        )
        products_changed.send(self.model, categories=categories)
    action_hide.short_description = _('Hide selected %(verbose_name_plural)s')

    def action_show(self, request, queryset):
        queryset.update(is_visible=True)
        categories = queryset.values_list(
            'category_id', flat=True
        )
        products_changed.send(self.model, categories=categories)
    action_show.short_description = _('Show selected %(verbose_name_plural)s')

    def action_set_hot(self, request, queryset):
        queryset.update(is_hot=True)
    action_set_hot.short_description = _('Mark %(verbose_name_plural)s as hot')

    def action_unset_hot(self, request, queryset):
        queryset.update(is_hot=False)
    action_unset_hot.short_description = _('Mark %(verbose_name_plural)s as not hot')


@admin.register(DiscountConfig)
class DiscountConfigAdmin(SeoModelAdminMixin, SingletonModelAdmin):
    fieldsets = (
        (None, {
            'classes': ('suit-tab', 'suit-tab-general'),
            'fields': (
                'title', 'header', 'description',
            ),
        }),
    )
    suit_form_tabs = (
        ('general', _('General')),
    )


@admin.register(Discount)
class DiscountAdmin(ModelAdminMixin, admin.ModelAdmin):
    fieldsets = (
        (None, {
            'classes': ('suit-tab', 'suit-tab-general'),
            'fields': (
                'title', 'value', 'unit', 'start_date', 'finish_date', 'is_visible',
            ),
        }),
    )
    list_display = ('__str__', 'fmt_value', 'start_date', 'finish_date', 'is_visible', )
    search_fields = ('title', )
    list_display_links = ('__str__', )
    # list_filter = (StatusShopProductCategoryFilter, )
    suit_form_tabs = (
        ('general', _('General')),
    )

    def fmt_value(self, obj):
        return obj.short_description
    fmt_value.allow_tags = True
    fmt_value.short_description = _('Discount')


class ShopOrderStatusFilter(HierarchyFilter):
    """ Фильтр по статусу """
    title = _('Status')
    parameter_name = 'status'

    STATUSES = (
        ('all', _('All')),
        ('non_checked', _('Not checked')),
        ('checked', _('Checked')),
        ('paid', _('Paid')),
        ('cancelled', _('Cancelled')),
        ('archived', _('Archived')),
    )

    def value(self):
        value = super().value()
        if not value:
            value = 'non_checked'
        return value

    def lookups(self, request, model_admin):
        result = []
        for key, name in self.STATUSES:
            qs = self.queryset(request, model_admin.model._default_manager, key)
            result.append((key, '%s (%d)' % (name, qs.count())))

        return result

    def get_branch_choices(self, value):
        return [
            (value, dict(self.STATUSES).get(value))
        ]

    def queryset(self, request, queryset, value=None):
        value = value or self.value()
        if value == 'non_checked':
            queryset = queryset.filter(
                confirmed=True, checked=False, paid=None, archived=False, cancelled=False
            )
        elif value == 'checked':
            queryset = queryset.filter(
                confirmed=True, checked=True, paid=False, archived=False, cancelled=False
            )
        elif value == 'paid':
            queryset = queryset.filter(
                confirmed=True, checked=True, paid=True, archived=False, cancelled=False
            )
        elif value == 'cancelled':
            queryset = queryset.filter(
                confirmed=True, checked=None, paid=None, archived=False, cancelled=True
            )
        elif value == 'archived':
            queryset = queryset.filter(
                confirmed=True, checked=None, paid=None, archived=True, cancelled=None
            )
        return queryset


class ShopOrderForm(forms.ModelForm):
    DATED_FLAGS = (
        ('is_confirmed', 'confirm_date'),
        ('is_cancelled', 'cancel_date'),
        ('is_checked', 'check_date'),
        ('is_paid', 'pay_date'),
        ('is_archived', 'archivation_date'),
    )

    class Meta:
        model = ShopOrder
        fields = '__all__'
        widgets = {
            'recipient': forms.Select(attrs={
                'style': 'width: 100%; max-width: 400px;'
            }),
            'address': forms.Select(attrs={
                'style': 'width: 100%; max-width: 400px;'
            }),
            'warehouse': forms.Select(attrs={
                'style': 'width: 100%; max-width: 400px;'
            }),
        }

    def _set_date(self, value, flagname, datename):
        if value and not getattr(self.instance, flagname):
            setattr(self.instance, datename, now())
        elif not value:
            setattr(self.instance, datename, None)

    def clean(self):
        data = super().clean()
        for flagname, datename in self.DATED_FLAGS:
            value = data.get(flagname)
            if value is not None:
                self._set_date(value, flagname, datename)
        return data


@admin.register(ShopOrder)
class ShopOrderAdmin(ModelAdminMixin, admin.ModelAdmin):
    fieldsets = (
        (_('Cost'), {
            'classes': ('suit-tab', 'suit-tab-general'),
            'fields': ('fmt_products_cost', 'fmt_total_cost', ),
        }),
        (_('Customer'), {
            'classes': ('suit-tab', 'suit-tab-general'),
            'fields': ('recipient', ),
        }),

        (_('Delivery and payment'), {
            'classes': ('suit-tab', 'suit-tab-general'),
            'fields': ('order_type', 'payment_type'),
        }),
        (_('Delivery options'), {
            'classes': ('suit-tab', 'suit-tab-general'),
            'fields': ('address',),
        }),
        (_('Pickup options'), {
            'classes': ('suit-tab', 'suit-tab-general'),
            'fields': ('warehouse',),
        }),
        (_('Boxberry options'), {
            'classes': ('suit-tab', 'suit-tab-general'),
            'fields': ('boxberry_city', 'boxberry_address'),
        }),
        (_('Comment'), {
            'classes': ('suit-tab', 'suit-tab-general'),
            'fields': ('comment',),
        }),

        (_('Cancelled'), {
            'classes': ('suit-tab', 'suit-tab-status'),
            'fields': (
                'is_cancelled', 'cancel_date',
            ),
        }),
        (_('Checked'), {
            'classes': ('suit-tab', 'suit-tab-status'),
            'fields': (
                'is_checked', 'check_date',
            ),
        }),
        (_('Paid'), {
            'classes': ('suit-tab', 'suit-tab-status'),
            'fields': (
                'is_paid', 'pay_date',
            ),
        }),
        (_('Archived'), {
            'classes': ('suit-tab', 'suit-tab-status'),
            'fields': (
                'is_archived', 'archivation_date',
            ),
        }),
        (_('Confirmed'), {
            'classes': ('suit-tab', 'suit-tab-status'),
            'fields': (
                'is_confirmed', 'confirm_date',
            ),
        }),
        (_('Created'), {
            'classes': ('suit-tab', 'suit-tab-status'),
            'fields': (
                'date',
            ),
        }),
    )
    form = ShopOrderForm
    date_hierarchy = 'date'
    readonly_fields = (
        'fmt_products_cost', 'fmt_total_cost', 'fmt_profile_name',
        'recipient', 'address', 'boxberry_city', 'boxberry_address',
        'date', 'is_confirmed', 'confirm_date',
        'cancel_date', 'check_date', 'pay_date', 'archivation_date',
    )
    list_display = (
        '__str__', 'fmt_total_cost', 'order_type', 'fmt_profile_name', 'is_paid', 'pay_date', 'date',
    )
    actions = ('action_set_checked', 'action_set_cancelled', 'action_set_paid', 'action_set_archived')
    list_filter = (ShopOrderStatusFilter, 'order_type', 'payment_type', 'date')
    suit_form_tabs = (
        ('general', _('General')),
        ('status', _('Status')),
        ('products', _('Products')),
    )
    suit_form_includes = (
        ('shop/admin/products.html', 'top', 'products'),
    )

    def suit_row_attributes(self, obj, request):
        if obj.is_cancelled:
            return {'class': 'error'}
        if obj.is_paid:
            return {'class': 'success'}

    def fmt_products_cost(self, obj):
        return obj.products_cost.alternative
    fmt_products_cost.short_description = _('Products cost')
    fmt_products_cost.admin_order_field = 'products_cost'

    def fmt_total_cost(self, obj):
        return obj.total_cost.alternative
    fmt_total_cost.short_description = _('Total cost')

    def fmt_profile_name(self, obj):
        return obj.profile_name
    fmt_profile_name.short_description = 'Тип платежа'

    def has_add_permission(self, request):
        return False

    def action_set_checked(self, request, queryset):
        queryset.update(is_checked=True)
    action_set_checked.short_description = _('Set %(verbose_name_plural)s checked')

    def action_set_paid(self, request, queryset):
        queryset.update(is_paid=True)
    action_set_paid.short_description = _('Set %(verbose_name_plural)s paid')

    def action_set_cancelled(self, request, queryset):
        queryset.update(is_cancelled=True)
    action_set_cancelled.short_description = _('Set %(verbose_name_plural)s cancelled')

    def action_set_archived(self, request, queryset):
        queryset.update(is_archived=True)
    action_set_archived.short_description = _('Set %(verbose_name_plural)s archived')

    def change_view(self, request, object_id, *args, **kwargs):
        if object_id is None:
            entity = None
        else:
            entity = self.get_object(request, unquote(object_id))

        extra_context = kwargs.pop('extra_context', None) or {}
        kwargs['extra_context'] = dict(extra_context, **{
            'entity': entity,
        })
        return super().change_view(request, object_id, *args, **kwargs)

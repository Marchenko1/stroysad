from django import forms
from django.utils.translation import ugettext_lazy as _
from libs.form_helper.forms import FormHelperMixin
from .models import ShopWarehouse, ShopOrder


class ShopOrderTypeForm(FormHelperMixin, forms.ModelForm):
    class Meta:
        model = ShopOrder
        fields = ('order_type', )
        widgets = {
            'order_type': forms.RadioSelect,
        }


class ShopCommentForm(FormHelperMixin, forms.ModelForm):
    csrf_token = False
    render_error_message = True
    field_template = 'fields/field.html'
    field_templates = {
        'comment': 'form_helper/unlabeled_field.html',
        'agreement': 'fields/agreement_field.html',
    }

    agreement = forms.BooleanField(
        required=True,
        label=_('Agreement'),
        initial=True,
    )

    class Meta:
        model = ShopOrder
        fields = ('comment', )
        widgets = {
            'comment': forms.Textarea(attrs={
                'rows': 4,
            })
        }


class ShopOrderPickupForm(FormHelperMixin, forms.ModelForm):
    csrf_token = False
    render_error_message = True
    field_template = 'fields/field.html'

    warehouse = forms.ModelChoiceField(
        label=ShopWarehouse._meta.verbose_name.capitalize(),
        required=True,
        empty_label=None,
        queryset=ShopWarehouse.objects.filter(active=True),
    )

    class Meta:
        model = ShopOrder
        fields = ('warehouse', )


class ShopOrderBoxberryForm(FormHelperMixin, forms.ModelForm):
    csrf_token = False
    render_error_message = True
    field_template = 'fields/field.html'

    class Meta:
        model = ShopOrder
        fields = ('boxberry_city', 'boxberry_address', 'boxberry_price')
        widgets = {
            'boxberry_city': forms.TextInput(attrs={
                'readonly': True,
            }),
            'boxberry_address': forms.TextInput(attrs={
                'readonly': True,
            }),
            'boxberry_price': forms.HiddenInput,
        }

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        for key in self.fields:
            if key in ('boxberry_city', 'boxberry_address'):
                self.fields[key].required = True


class ShopOrderPaymentForm(FormHelperMixin, forms.ModelForm):
    csrf_token = False

    class Meta:
        model = ShopOrder
        fields = ('payment_type',)
        widgets = {
            'payment_type': forms.RadioSelect,
        }

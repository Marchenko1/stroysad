from django.conf.urls import patterns, url
from libs.autoslug import ALIAS_REGEXP
from . import views, cart, views_ajax


urlpatterns = patterns('',
    # AJAX
    url(
        r'^save-cart/$',
        cart.SaveCart.as_view(),
        name='save_cart',
    ),
    url(
        r'^load-cart/$',
        cart.LoadCart.as_view(),
        name='load_cart',
    ),
    url(
        r'^clear-cart/$',
        cart.ClearCart.as_view(),
        name='clear_cart',
    ),
    url(
        r'^cancel-order/$',
        views_ajax.CancelOrderView.as_view(),
        name='cancel_order',
    ),
    url(
        r'^order_complete/$',
        views_ajax.OrderCompleteView.as_view(),
        name='order_complete',
    ),


    url(
        r'^shop/discount/$', views.DiscountView.as_view(),
        name='discount'
    ),
    url(
        r'^order/cart/$',
        views.OrderCartView.as_view(),
        name='order-cart'
    ),
    url(
        r'^(?P<category_alias>{0})_(?P<producer>{0})/$'.format(ALIAS_REGEXP),
        views.CategoryView.as_view(),
        name='producer'
    ),
    url(
        r'^(?P<category_alias>{0})/$'.format(ALIAS_REGEXP),
        views.CategoryView.as_view(),
        name='category'
    ),
    url(
        r'^(?P<category_alias>{0})/(?P<alias>{0})/$'.format(ALIAS_REGEXP),
        views.DetailView.as_view(),
        name='detail'
    ),
)

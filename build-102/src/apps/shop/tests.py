import datetime
import os

from django.conf import settings
from django.template.loader import render_to_string
from django.test import TestCase


class AnimalTestCase(TestCase):
    def setUp(self):
        print(123)

    def test_ya_catalog_generate(self):
        """Animals that can speak are correctly identified"""
        from shop import models as catalog_models

        def get_data():
            return {
                'date':     datetime.datetime.now().strftime('%Y-%m-%d %H:%M'),
                'name':     settings.YML_SHOP_NAME,
                'company':  settings.YML_SHOP_COMPANY,
                'url':      settings.YML_SHOP_URL,
                'currency': 'RUR',
                'rate':     '1',
                'domain':   settings.YML_SHOP_URL
            }

        data = get_data()
        products = catalog_models.ShopProduct.objects.order_by('id')
        data['products'] = products
        data['categories'] = catalog_models.ShopCategory.objects.order_by('id')
        with open(os.path.join(settings.MEDIA_ROOT, 'catalog_ya.xml'), 'w') as catalog_yml:
            catalog_yml.write(render_to_string('catalog.xml', data))

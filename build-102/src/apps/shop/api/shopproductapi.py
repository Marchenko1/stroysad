

import logging

from api.lib.decorators import api_handler
from api.lib.interfaces.entity.ientity import IEntity

logger = logging.getLogger(__name__)


class ShopProductApi(IEntity):
    """
    Класс API сущности Товара
    """

(function ($) {

    $(document).on('click', '#text-hide', function () {
        $('#text-hide').addClass('dnone');
        $('.m60').removeClass('hide');
        return false;
    })


    // Раскрывающиеся Shower-блоки на мобиле
    $(document).on('click', '.shower-title', function () {
        var $title = $(this);
        var $block = $title.next('.shower-block');
        if (!$block.length) {
            return false
        }

        if ($title.hasClass('opened')) {
            $title.removeClass('opened');
            $block.stop(true, false).slideUp({
                duration: 500,
                complete: function () {
                    $block.get(0).style.cssText = 'display: none';
                }
            });
        } else {
            $title.addClass('opened');
            $block.stop(true, false).slideDown({
                duration: 500,
                complete: function () {
                    $block.get(0).style.cssText = 'display: block';
                }
            });
        }
    });


    // Кастомные чекбоксы
    $(document).ready(function () {
        $('.producer').find('input').checkbox({
            afterChange: function () {
                if (this.$input.prop('checked')) {
                    var name = this.$input.attr('name');
                    var $groupBoxes = $('input[name="' + name + '"]').not(this.$input);
                    $groupBoxes.each(function (i, item) {
                        var box = $(item).data('checkbox');
                        if (box) {
                            box.uncheck()
                        }
                    });
                }
            }
        });
    });


    $(document).on('click', '.producer-filter', function () {
        var form = $(this).parent('form'),
            producer = '',
            action,
            url;

        form.find("input:checkbox[name=producers]:checked").each(function () {
            producer = $(this).val();
            action = $(this).data('action');
        });

        if (producer != '') {
            url = action;
        } else {
            url = form.attr('action');
        }

        window.location.href = url;

        return false;
    });

})(jQuery);
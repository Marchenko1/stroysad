(function ($) {

    var format_price = function(value) {
        var str_value = value.toFixed(2).toString();
        var parts = str_value.split('.');

        var int = parts[0].split('').reverse().join('');
        int = int.replace(/(\d{3})(?=\d)/g, "$1 ").split('').reverse().join('');

        var frac = parts[1];
        if (frac === '00') {
            frac = null;
        }

        var result = frac ? [int, frac].join('.') : int;
        return result + '₽';
    };


    var tables = [];
    var cascade = false;
    var Table = Class(null, function Table(cls, superclass) {
        cls.init = function (root) {
            this.$root = $(root).first();
            if (!this.$root.length) {
                console.error('Table can\'t find root element');
                return false
            }

            // кастомные счетчики
            var that = this;
            this.$root.find('.custom-counter').counter({
                maxValue: window.js_storage.max_product_count,
                buttonClass: 'custom-counter-button btn btn-medium',
                afterChange: function (value) {
                    var price = parseFloat((this.$root.data('price') || '0').replace(',', '.'));
                    var prod_price = price * value;

                    var $row = this.$root.closest('tr');
                    var prod_id = parseInt($row.data('product-id')) || 0;
                    $row.find('.product-price').data('value', prod_price).text(format_price(prod_price));

                    that.updatePrice();

                    if (cascade === true) {
                        return;
                    } else {
                        cascade = true;
                    }

                    // меняем значение в полях других таблиц (c cascade == true)
                    $.each(tables, function (i, item) {
                        if (item !== that) {
                            var $counter = item.$root.find('.product-count-' + prod_id);
                            var counter = $counter.data(CustomCounter.prototype.DATA_KEY);
                            counter.value(value)
                        } else {
                            var storage = {};
                            that.$root.find('tr[class^="product-row-"]').each(function() {
                                var $row = $(this);
                                var prod_id = parseInt($row.data('product-id')) || 0;
                                var count = $row.find('.custom-counter input').val();
                                storage[prod_id] = parseInt(count) || 0;
                            });
                            window.cart.saveStorage(storage).done(function(response){
                                if (response.header && response.total_price) {
                                    $('.header-text').html(response.header);
                                }
                            });
                        }
                    });

                    cascade = false;
                }
            });

            // Удаление товара
            this.$root.on('click', '.product-remove a', function () {
                var $row = $(this).closest('tr');
                var prod_id = parseInt($row.data('product-id')) || 0;

                that.removeRow(prod_id, function () {
                    window.cart.removeItem(prod_id).done(function () {
                        if ($.isEmptyObject(window.cart.getStorage())) {
                            location.href = window.js_storage.empty_cart_redirect;
                        }
                    });
                });

                $.each(tables, function (i, item) {
                    if (item !== that) {
                        item.removeRow(prod_id);
                    }
                });

                return false;
            });
        };

        cls.updatePrice = function () {
            var total_price = 0;
            this.$root.find('.product-price').each(function () {
                total_price += parseFloat($(this).data('value')) || 0;
            });

            var formatted_price = format_price(total_price);
            this.$root.find('.total-price').find('.price-big').text(formatted_price);
            $('.total-cost').html(formatted_price);
        };

        cls.removeRow = function (prod_id, callback) {
            var $tr = this.$root.find('.product-row-' + prod_id);
            if (!$tr.length || $tr.hasClass('removing')) {
                return false;
            } else {
                $tr.addClass('removing');
            }

            var that = this;
            setTimeout(function () {
                $tr.remove();
                that.updatePrice();

                if ($.isFunction(callback)) {
                    callback.call(that);
                }
            }, 200);
        };
    });



    $(document).ready(function () {
        var $form = $('#order-cart');
        var $tables = $form.find('.table');
        tables = $.map($tables, function (item) {
            return Table.create(item);
        });


        /*
            Перед отправкой формы блокируем повторные данные
         */
        $form.on('submit', function () {
            // Блокируем все input скрытых форм
            $form.find('.table:visible').find('input').prop('disabled', false);
            $form.find('.table:hidden').find('input').prop('disabled', true);

            var storage = {};
            $form.find('.custom-counter-input').each(function () {
                var $this = $(this);
                var prod_id = parseInt($this.closest('tr').data('product-id'));
                var value = parseInt($this.val());

                if (prod_id && value) {
                    storage[prod_id] = value;
                }
            });

            cart.saveStorage(storage);

            return true;
        });
    });


    window.boxberry_callback = function(result) {
        var $form = $('.boxberry-form');
        $form.find('input[name$="boxberry_city"]').val(result.name);
        $form.find('input[name$="boxberry_address"]').val(result.address);
        $form.find('input[name$="boxberry_price"]').val(result.price);
    };

    $(document).ready(function () {
        // radio
        $('input[type="radio"]').radiobox();
    });

    $(document).on('click', '.boxberry-popup', function() {
        /** @namespace boxberry */
        /** @namespace window.js_storage.user_city */
        /** @namespace window.js_storage.boxberry_key */
        /** @namespace window.js_storage.boxberry_weight */
        boxberry.open(
            // функция
            window.boxberry_callback,

            // ключ интеграции
            window.js_storage.boxberry_key,

            // город
            window.js_storage.user_city,

            // код города отправления
            '',

            // объявленная стоимость посылки
            '0',

            // вес посылки
            window.js_storage.boxberry_weight
        );
        return false;
    });

    $(document).on('checked.radiobox', '.order-type-form', function(e, radiobox) {
        var value = parseInt(radiobox.$input.val());

        $('.delivery-form').hide();
        $('.delivery-form-' + value).show();

        var $payments = $('#payment-type');

        // Транспортная компания
        if (value === 4) {
            // отключение оплаты "наличными при получении"
            $payments.find('.custom-radio-wrapper[data-value="1"]').hide();

            // автовыбор оплаты банковской картой
            var bank_card = $payments.find('.custom-radio-wrapper[data-value="3"]').data(CustomRadiobox.prototype.DATA_KEY);
            if (bank_card) {
                bank_card.check();
            }
        } else {
            $('#id_payment_type-payment_type_0').closest('.custom-radio-wrapper').show();
        }
    });

})(jQuery);
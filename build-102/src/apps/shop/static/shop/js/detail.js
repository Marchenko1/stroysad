(function($) {

    $(document).ready(function() {
        var $slider = $('#photo-column').find('.slider');
        if ($slider.length) {
            Slider($slider, {
                adaptiveHeight: false,
                sliderHeight: Slider.prototype.HEIGHT_CURRENT
            }).attachPlugins([
                SliderSideAnimation({
                    speed: 800
                }),
                SliderNavigationPlugin({
                    animationName: 'side'
                }),
                SliderDragPlugin({
                    speed: 800
                })
            ]);
        }
    });

    // Кастомные числовы поля
    $(document).ready(function() {
        $('.custom-counter').counter({
            maxValue: window.js_storage.max_product_count,
            buttonClass: 'custom-counter-button btn btn-medium'
        })
    });

})(jQuery);
(function($) {

    $.widget('custom.myselectmenu', $.ui.selectmenu, {
        _resizeMenu: function() {
            var width = Math.max(
                this.button.outerWidth(),

                // support: IE10
                // IE10 wraps long text (possibly a rounding bug)
                // so we add 1px to avoid the wrapping
                this.menuWrap.width("").outerWidth() + 1
            );

            this.menuWrap.outerWidth(width);
        },
        _select: function(item, event) {
            var oldIndex = this.element[0].selectedIndex;

            // Change native select element
            this.element[0].selectedIndex = item.index;
            this._setText(this.buttonText, item.label);
            this._setAria(item);
            this._trigger("select", event, {item: item});

            if (item.index !== oldIndex) {
                this._trigger("change", event, {item: item});
                this.element.trigger('change');
            }

            this.close(event);
        }
    });

    var positionSelects = function() {
        $('select').each(function(i, select) {
            var menu = $(select).data('custom-myselectmenu');
            if (menu.button.attr('aria-expanded') == 'true') {
                menu._resizeMenu();
                menu._position();
            }
        });
    };

    $(window).on('scroll resize', function() {
        $.animation_frame(positionSelects)();
    });

})(jQuery);
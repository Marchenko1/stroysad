(function($) {

    //// Кнопка добавления товара
    $(document).on('click', '.product-add-btn', function() {
       var $button = $(this);
       if ($button.hasClass('disabled')) {
           return false
       } else {
           $button.addClass('disabled');
       }

       var product_id = $button.data('product-id');
       var count_id = $button.data('count-id');
       var count = 1;
       if (count_id) {
           var $count = $('#' + count_id);
           if ($count.length) {
               count = parseInt($count.val()) || 1;
           }
       }

       // добавление товара в корзину и сброс кол-ва в поле
       cart.addItem(product_id, count).done(function() {
           if (count_id && $count.length) {
               $count.val(1);
               $count.trigger('change');
           }
       }).always(function() {
           $button.removeClass('disabled');
       });
    });

})(jQuery);
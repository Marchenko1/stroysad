import uuid
from decimal import Decimal
from django.db import models
from django.conf import settings
from django.db.models.deletion import SET_NULL
from django.utils.timezone import now
from django.shortcuts import resolve_url
from django.core.exceptions import ValidationError
from django.utils.functional import cached_property
from django.core.validators import MinValueValidator
from django.utils.translation import ugettext_lazy as _
from django.contrib.contenttypes.models import ContentType
from solo.models import SingletonModel
from libs.aliased_queryset import AliasedQuerySetMixin
from libs.valute_field import ValuteField, Valute
from libs.autoslug import AutoSlugField
from libs.mptt import *
from libs.videolink_field.fields import VideoLinkField
from gallery.fields import GalleryField
from gallery.models import GalleryBase, GalleryImageItem
from files import PageFile
from ckeditor.fields import CKEditorField, CKEditorUploadField
from blog.models import BlogPost
from address.models import Address
from recipient.models import Recipient
from .signals import products_changed, categories_changed
from .api.shopproductapi import ShopProductApi


class ShopConfig(SingletonModel):
    header = models.CharField(_('header'), max_length=128)
    title = models.CharField(_('Title'), max_length=128)
    description = CKEditorField(_('description'), blank=True, editor_options=settings.CKEDITOR_CONFIG_MICRO)
    online_payment_limit = ValuteField(_('online payment limit'), default=0)
    payment_text = models.TextField(_('payment text'))
    boxberry_weight = models.PositiveIntegerField(_('default order weight'), default=500, help_text=_('g'))
    updated = models.DateTimeField(_('change date'), auto_now=True)

    class Meta:
        verbose_name = _('settings')

    def get_absolute_url(self):
        return resolve_url('index')


class DiscountConfig(SingletonModel):
    header = models.CharField(_('header'), max_length=128)
    title = models.CharField(_('Title'), max_length=128)
    description = CKEditorField(_('description'), blank=True, editor_options=settings.CKEDITOR_CONFIG_MICRO)

    updated = models.DateTimeField(_('change date'), auto_now=True)

    class Meta:
        verbose_name = _('discount settings')

    def get_absolute_url(self):
        return resolve_url('shop:discount')

    @property
    def has_discounts(self):
        return Discount.objects.filter(visible=True).exists()


class DiscountQuerySet(AliasedQuerySetMixin, models.QuerySet):
    def aliases(self, qs, kwargs):
        visible = kwargs.pop('visible', None)
        if visible is True:
            qs &= models.Q(is_visible=True, start_date__lte=now(), finish_date__gte=now())
        elif visible is False:
            qs ^= models.Q(is_visible=True, start_date__lte=now(), finish_date__gte=now())
        return qs


class Discount(models.Model):
    PERCENTAGE = 1
    FIXED = 2
    UNIT_TYPE = (
        (PERCENTAGE, _('Percentage discount')),
        (FIXED, _('Fixed monetary amount')),
    )

    title = models.CharField(_('discount title'), max_length=128)
    value = models.PositiveSmallIntegerField(_('value'), validators=[MinValueValidator(1)])
    unit = models.PositiveSmallIntegerField(_('unit'), choices=UNIT_TYPE, default=PERCENTAGE)

    start_date = models.DateTimeField(_('start date'))
    finish_date = models.DateTimeField(_('finish date'))

    is_visible = models.BooleanField(_('visible'), default=True)

    created = models.DateTimeField(_('create date'), default=now, editable=False)
    updated = models.DateTimeField(_('change date'), auto_now=True)

    objects = DiscountQuerySet.as_manager()

    class Meta:
        verbose_name = _('discount')
        verbose_name_plural = _('discounts')
        ordering = ('-created', )

    def __str__(self):
        return self.title

    @property
    def is_active(self):
        if self.is_visible is True and self.start_date <= now() and self.finish_date >= now():
            return True

        return False

    @property
    def short_description(self):
        if self.unit == Discount.FIXED:
            return '-%s' % Valute(self.value)
        elif self.unit == Discount.PERCENTAGE:
            return '-%s' % self.value + '%'

    def calculate(self, product_cost):
        if self.unit == self.FIXED:
            result = product_cost - Decimal(self.value)
            return result if result > 0 else Valute(0)
        elif self.unit == self.PERCENTAGE:
            result = product_cost - product_cost * Decimal(self.value) / 100
            result = Valute(int(result.as_decimal()))
            return result if result > 0 else Valute(0)

        return Valute(0)


class ShopWarehouse(models.Model):
    """ Склады """
    city = models.CharField(_('city'), max_length=128, db_index=True)
    address = models.CharField(_('address'), max_length=255)
    active = models.BooleanField(_('active'), default=False)

    class Meta:
        verbose_name = _('warehouse address')
        verbose_name_plural = _('warehouse addresses')
        ordering = ('city', 'address')

    def __str__(self):
        return '{0.city}, {0.address}'.format(self)


class ShopCategoryQuerySet(AliasedQuerySetMixin, MPTTQuerySet):
    def aliases(self, qs, kwargs):
        visible = kwargs.pop('visible', None)
        if visible is None:
            pass
        elif visible:
            qs &= models.Q(is_visible=True, total_product_count__gt=0)
        else:
            qs &= ~models.Q(is_visible=True, total_product_count__gt=0)
        return qs


class ShopCategoryTreeManager(MPTTQuerySetManager):
    _queryset_class = ShopCategoryQuerySet

    def root_categories(self):
        return self.root_nodes().filter(visible=True)


class ShopCategory(MPTTModel):
    """ Категория товаров """
    SHOP_LINE = (
        (1, '1'),
        (2, '2'),
    )
    parent = TreeForeignKey('self',
        blank=True,
        null=True,
        verbose_name=_('parent category'),
        related_name='children'
    )
    discount = models.ForeignKey(Discount,
        null=True,
        blank=True,
        verbose_name=_('discount'),
        on_delete=SET_NULL,
    )

    title = models.CharField(_('category title'), max_length=128)
    title_desc = models.CharField(_('title for description'), max_length=128, blank=True)
    alias = AutoSlugField(_('alias'),
        populate_from=('title',),
        unique=True,
        help_text=_('Leave it blank to auto generate it')
    )
    video = VideoLinkField(_('video'), providers=('youtube',), blank=True)
    video_title = models.CharField(_('video title'), max_length=128, blank=True)
    header = models.CharField(_('header'), max_length=128)
    short_description = CKEditorUploadField(_('short description'), blank=True)
    description = CKEditorUploadField(_('short description'), blank=True)
    text_additional = CKEditorUploadField(_('short description'), blank=True)
    is_visible = models.BooleanField(_('visible'), default=True, db_index=True)
    shop_main_visible = models.BooleanField(_('visible'), default=True, db_index=True, help_text=_('visible on shop main'))
    product_count = models.PositiveIntegerField(_('product count'),
        default=0,
        editable=False,
        help_text=_('count of immediate visible products'),
    )
    total_product_count = models.PositiveIntegerField(_('total product count'),
        default=0,
        editable=False,
        help_text=_('count of visible products'),
    )
    shop_line = models.PositiveIntegerField(_('line count'),
        default=1,
        choices=SHOP_LINE,
        help_text=_('shop main product line count')
    )
    sort_order = models.PositiveIntegerField(_('sort order'))
    created = models.DateTimeField(_('create date'), default=now, editable=False)
    updated = models.DateTimeField(_('change date'), auto_now=True)

    objects = ShopCategoryTreeManager()

    class Meta:
        verbose_name = _('category')
        verbose_name_plural = _('categories')

    class MPTTMeta:
        order_insertion_by = ('sort_order', )

    @property
    def first_parent_id(self):
        """ Возвращает ID первого родителя """
        try:
            return self.get_ancestors(ascending = False).last().id
        except AttributeError:
            return

    @property
    def names_tree(self):
        """ Возвращает имена родительских разделов """
        return '/'.join(self.get_ancestors(ascending = False, include_self = True).values_list('title', flat = True))

    def save(self, *args, **kwargs):
        is_add = self.pk is None
        original = None if is_add else self.__class__.objects.select_related('parent').only(
            'is_visible', 'parent'
        ).get(pk=self.pk)

        # Видимость
        if self.is_visible and self.parent_id and not self.parent.is_visible:
            # корректировка для случая видимой подкатегории в невидимой
            if is_add:
                # если добавление - скрываем текущую категорию
                self.is_visible = False
            elif self.parent_id != original.parent_id:
                # если смена родителя - скрываем текущую категорию и её потомков
                self.is_visible = False
                self.get_descendants().filter(is_visible=True).update(is_visible=False)
            else:
                # если смена видимости - показываем предков
                self.get_ancestors().filter(is_visible=False).update(is_visible=True)
        elif not is_add and not self.is_visible and original.is_visible:
            # скрытие категории - скрываем потомков
            self.get_descendants().filter(is_visible=True).update(is_visible=False)

        try:
            super().save(*args, **kwargs)
        except InvalidMove:
            pass

        # Кол-во продуктов
        if is_add:
            pass
        elif self.parent_id != original.parent_id:
            # смена родителя
            categories = []
            if self.parent_id:
                categories.append(self.parent_id)
            if original.parent_id:
                categories.append(original.parent_id)
            if categories:
                categories_changed.send(self.__class__, categories=categories)
        elif self.is_visible and not original.is_visible:
            # категория показана
            categories_changed.send(self.__class__, categories=self.pk)
        elif not self.is_visible and original.is_visible:
            # категория скрыта
            self.get_descendants(include_self=True).update(total_product_count=0)
            categories_changed.send(self.__class__, categories=self.pk, include_self=False)

    def __str__(self):
        return self.title

    @cached_property
    def description_title(self):
        return self.title_desc if self.title_desc else self.title

    @cached_property
    def subcategories(self):
        """ Видимые дочерние подкатегории """
        return self.get_children().filter(visible=True)

    @cached_property
    def products(self):
        """ Видимые продукты категории и её подкатегорий """
        return ShopProduct.objects.filter(
            visible=True,
            category__in=self.get_descendants(include_self=True).filter(visible=True)
        ).distinct()

    @cached_property
    def producers(self):
        producer_ids = self.products.exclude(
            producer=None
        ).order_by('producer__id').distinct('producer__id').values_list('producer', flat=True)
        return ShopProductProducer.objects.filter(pk__in=producer_ids)

    @staticmethod
    def autocomplete_item(obj):
        return {
            'id': obj.pk,
            'text': '–' * obj.level + ' ' + obj.title,
        }

    def get_absolute_url(self):
        return resolve_url('shop:category', category_alias=self.alias)


class PostCategory(models.Model):
    category = models.ForeignKey(ShopCategory)
    post = models.ForeignKey(BlogPost, verbose_name=_('post'))
    subcategory = models.BooleanField(_('for subcategory'), default=False)

    class Meta:
        verbose_name = _('Post category')
        verbose_name_plural = _('Posts category')
        ordering = ('-post', )


class ShopProductProducer(models.Model):
    """ Производитель """
    discount = models.ForeignKey(Discount,
        null=True,
        blank=True,
        verbose_name=_('discount'),
        on_delete=SET_NULL,
    )
    title = models.CharField(_('title'), max_length=128, unique=True)
    alias = AutoSlugField(_('alias'),
        populate_from=('title',),
        unique=True,
        help_text=_('Leave it blank to auto generate it')
    )

    class Meta:
        verbose_name = _('producer')
        verbose_name_plural = _('producers')
        ordering = ('title', )

    def __str__(self):
        return self.title


class ShopProducerDescription(models.Model):
    category = models.ForeignKey(ShopCategory, verbose_name=_('category'))

    producer = models.ForeignKey(ShopProductProducer, verbose_name=_('producer'))
    title_producer = models.CharField(_('producer title'), max_length=1024, blank=True)
    description = CKEditorUploadField(_('description'), blank=True)

    meta_title = models.CharField(_('title'), max_length=128, blank=True)
    meta_description = models.TextField(_('meta description'), max_length=255, blank=True)

    class Meta:
        verbose_name = _('producer description')
        verbose_name_plural = _('producers description')
        unique_together = ('producer', 'category')

    def __str__(self):
        return _('Producer description <%s>') % self.producer.title


class ShopProductQuerySet(AliasedQuerySetMixin, models.QuerySet):
    def aliases(self, qs, kwargs):
        visible = kwargs.pop('visible', None)
        if visible is None:
            pass
        elif visible:
            qs &= models.Q(is_visible=True)
        else:
            qs &= models.Q(is_visible=False)

        supercategory = kwargs.pop('supercategory', None)
        if supercategory is None:
            pass
        else:
            if isinstance(supercategory, (str, int)):
                supercategory = ShopCategory.objects.get(pk=supercategory)

            categories = supercategory.get_descendants(include_self=True).filter(visible=True)
            qs &= models.Q(category__in=categories)

        return qs

    def with_photos(self):
        """
            !! Сразу выполняет запрос !!
            Возвращает кортеж продуктов с картинками и описаниями к картинкам.
            Применяется для оптимизации SQL-запроса большого количества продуктов.
        """
        products = tuple(self)
        gallery_ct = ContentType.objects.get_for_model(ShopProductGallery)
        photos_dict = {
            photo.object_id: photo
            for photo in ShopProductGalleryImageItem.objects.filter(
                content_type=gallery_ct,
                object_id__in=(prod.gallery_id for prod in products)
            ).distinct('object_id')
        }
        for product in products:
            product_photo = photos_dict.get(product.gallery_id)
            if product_photo:
                product._photo = product_photo.image
                product._photo_description = product_photo.description

        return products


class ShopProductGalleryImageItem(GalleryImageItem):
    STORAGE_LOCATION = 'shop/product/gallery'
    MIN_DIMENSIONS = (100, 60)
    ADMIN_CLIENT_RESIZE = True

    SHOW_VARIATION = 'big'
    ASPECTS = 'big'
    ADMIN_VARIATION = 'micro'
    VARIATIONS = dict(
        big=dict(
            size=(600, 600),
            crop=False,
            background=(255, 255, 255, 255),
        ),
        medium=dict(
            size=(450, 450),
            crop=False,
            background=(255, 255, 255, 255),
        ),
        small=dict(
            size=(300, 300),
            crop=False,
            background=(255, 255, 255, 255),
        ),
        micro=dict(
            size=(200, 200),
            crop=False,
            background=(255, 255, 255, 255),
        ),
        admin_micro=dict(
            size=(60, 60),
            crop=False,
            background=(255, 255, 255, 255),
        )
    )


class ShopProductGallery(GalleryBase):
    IMAGE_MODEL = ShopProductGalleryImageItem
    ADMIN_ITEM_SIZE = (160, 160)


class ShopProduct(models.Model):
    """ Товар """
    _photo = None
    _photo_description = None

    category = models.ForeignKey(ShopCategory,
        verbose_name=_('category'),
        related_name='immediate_products'
    )
    producer = models.ForeignKey(ShopProductProducer,
        null=True,
        blank=True,
        verbose_name=_('producer'),
        related_name='products',
        on_delete=SET_NULL,
    )
    discount = models.ForeignKey(Discount,
        null=True,
        blank=True,
        verbose_name=_('discount'),
        on_delete=SET_NULL,
    )

    title = models.CharField(_('title'), max_length=128)
    alias = AutoSlugField(_('alias'),
        populate_from=('title',),
        unique=True,
        help_text=_('Leave it blank to auto generate it')
    )
    serial = models.SlugField(_('serial number'),
        max_length=64,
        unique=True,
        help_text=_('Unique identifier of the product')
    )
    gallery = GalleryField(ShopProductGallery, verbose_name=_('gallery'), blank=True, null=True)
    description = CKEditorField(_('description'), blank=True)
    video_title = models.CharField(_('video title'), max_length=128, blank=True)
    video = VideoLinkField(_('video'), providers=('youtube',), blank=True)
    price = ValuteField(_('price'), validators=[MinValueValidator(0)])
    is_visible = models.BooleanField(_('visible'), default=True)
    is_hot = models.BooleanField(_('hot'), default=False, help_text=_('priority show this product on main page'))

    created = models.DateTimeField(_('create date'), default=now, editable=False)
    updated = models.DateTimeField(_('change date'), auto_now=True)

    objects = ShopProductQuerySet.as_manager()

    # Обработчик API
    api_handler = ShopProductApi

    class Meta:
        verbose_name = _('product')
        verbose_name_plural = _('products')
        index_together = (('category', 'is_visible', 'producer'),)
        ordering = ('-is_hot', '-created',)

    def __str__(self):
        return self.title

    def get_absolute_url(self):
        return resolve_url('shop:detail',
            category_alias=self.category.alias,
            alias=self.alias
        )

    def save(self, *args, **kwargs):
        is_add = self.pk is None
        if is_add:
            original = None
        else:
            original = self.__class__.objects.only(
                'is_visible', 'category_id'
            ).get(pk=self.pk)

        super().save(*args, **kwargs)

        if is_add:
            # добавление видимого продукта
            if self.is_visible:
                products_changed.send(
                    self.__class__,
                    categories=self.category_id
                )
        elif self.category_id != original.category_id:
            # смена категории
            categories = []
            if original.is_visible:
                categories.append(original.category_id)
            if self.is_visible:
                categories.append(self.category_id)

            if categories:
                products_changed.send(
                    self.__class__,
                    categories=categories
                )
        elif self.is_visible != original.is_visible:
            # смена видимости
            products_changed.send(
                self.__class__,
                categories=self.category_id
            )

    @cached_property
    def photo(self):
        if self._photo:
            return self._photo

        if self.gallery:
            item = self.gallery.image_items.first()
            return item.image if item else None
        return None

    @cached_property
    def photo_description(self):
        default_description = self.title
        if self._photo_description is not None:
            return self._photo_description or default_description

        if self.gallery:
            item = self.gallery.image_items.first()
            return item.description if item else default_description
        return default_description

    @property
    def get_discount(self):
        data = {'discount': '', 'unit': '', 'price': self.price, }
        if self.discount and self.discount.is_active and self.discount.calculate(self.price) < data['price']:
            data['price'] = self.discount.calculate(self.price)
            data['discount'] = self.discount.short_description
            data['unit'] = self.discount.unit
        if self.category and self.category.discount and self.category.discount.is_active and self.category.discount.calculate(self.price) < data['price']:
            data['price'] = self.category.discount.calculate(self.price)
            data['discount'] = self.category.discount.short_description
            data['unit'] = self.category.discount.unit
        if self.producer and self.producer.discount and self.producer.discount.is_active and self.producer.discount.calculate(self.price) < data['price']:
            data['price'] = self.producer.discount.calculate(self.price)
            data['discount'] = self.producer.discount.short_description
            data['unit'] = self.producer.discount.unit

        return data

    @property
    def total_price(self):
        return self.get_discount['price']

    @property
    def short_discount(self):
        return self.get_discount['discount']

    @property
    def discount_unit(self):
        return self.get_discount['unit']

    @property
    def is_discount(self):
        if self.price != self.total_price:
            return True

        return False


class Manual(PageFile):
    STORAGE_LOCATION = 'shop/files'
    manual = models.ForeignKey(ShopProduct, related_name='files')


class ShopOrderQuerySet(AliasedQuerySetMixin, models.QuerySet):
    def aliases(self, qs, kwargs):
        # подтвержденные
        confirmed = kwargs.pop('confirmed', None)
        if confirmed is None:
            pass
        else:
            qs &= models.Q(is_confirmed=confirmed)

        # отмененные
        cancelled = kwargs.pop('cancelled', None)
        if cancelled is None:
            pass
        else:
            qs &= models.Q(is_cancelled=cancelled)

        # оплаченные
        paid = kwargs.pop('paid', None)
        if paid is None:
            pass
        else:
            qs &= models.Q(is_paid=paid)

        # проверенные
        checked = kwargs.pop('checked', None)
        if checked is None:
            pass
        else:
            qs &= models.Q(is_checked=checked)

        # в архиве
        archived = kwargs.pop('archived', None)
        if archived is None:
            pass
        else:
            qs &= models.Q(is_archived=archived)

        return qs


class ShopOrder(models.Model):
    """ Заказ """
    DELIVERY_TYPE = 1
    PICKUP_TYPE = 2
    BOXBERRY_TYPE = 3
    TRANSPORT_COMPANY_TYPE = 4
    ORDER_TYPE = (
        (DELIVERY_TYPE, _('Delivery')),
        (PICKUP_TYPE, _('Pickup')),
        (BOXBERRY_TYPE, _('Boxberry')),
        (TRANSPORT_COMPANY_TYPE, _('Transport Company')),
    )

    PAYMENT_CASH = 1
    PAYMENT_CASHLESS = 2
    PAYMENT_ONLINE = 3
    PAYMENT_TYPES = (
        (PAYMENT_CASH, _('Cash payment')),
        (PAYMENT_CASHLESS, _('Cashless payment')),
        (PAYMENT_ONLINE, _('Online payment')),
    )

    uuid = models.UUIDField(_('UUID'), default=uuid.uuid4, unique=True, editable=False)
    user = models.ForeignKey(settings.AUTH_USER_MODEL, verbose_name=_('user'), related_name='orders', blank=True, null=True)
    products_cost = ValuteField(_('products cost'),
        validators=[MinValueValidator(0)],
        editable=False,
    )
    step = models.PositiveSmallIntegerField(_('current step'), default=1)

    order_type = models.PositiveSmallIntegerField(_('type'), choices=ORDER_TYPE, default=DELIVERY_TYPE)

    # склад для самовывоза
    warehouse = models.ForeignKey(ShopWarehouse, verbose_name=_('warehouse'), null=True, blank=True, related_name='+')

    # получатель
    recipient = models.ForeignKey(Recipient,
        verbose_name=_('recipient'), null=True, blank=True, related_name='+', on_delete=SET_NULL
    )

    # адрес доставки
    address = models.ForeignKey(Address,
        verbose_name=_('address'), null=True, blank=True, related_name='+', on_delete=SET_NULL
    )

    # boxberry
    boxberry_city = models.CharField(_('city'), max_length=256, blank=True)
    boxberry_address = models.CharField(_('address'), max_length=512, blank=True)
    boxberry_price = ValuteField(_('price'),
        blank=True,
        validators=[MinValueValidator(0)],
    )

    comment = models.TextField(_('comment'), blank=True)

    # тип оплаты
    payment_type = models.PositiveSmallIntegerField(_('payment type'),
        choices=PAYMENT_TYPES, default=PAYMENT_ONLINE
    )

    # Подтвержден плательщиком
    is_confirmed = models.BooleanField(_('confirmed'), default=False, editable=False,
        help_text=_('Confirmed by the user')
    )
    confirm_date = models.DateTimeField(_('confirm date'),
        null=True,
        editable=False,
    )

    # Отменен плательщиком
    is_cancelled = models.BooleanField(_('cancelled'), default=False)
    cancel_date = models.DateTimeField(_('cancel date'),
        null=True,
        editable=False,
    )

    # Проверка менеджером
    is_checked = models.BooleanField(_('checked'), default=False)
    check_date = models.DateTimeField(_('check date'),
        null=True,
        editable=False,
    )

    # Оплачен
    is_paid = models.BooleanField(_('paid'), default=False)
    pay_date = models.DateTimeField(_('pay date'),
        null=True,
        editable=False,
    )

    # В архиве
    is_archived = models.BooleanField(_('archived'), default=False)
    archivation_date = models.DateTimeField(_('archivation date'),
        null=True,
        editable=False,
    )

    date = models.DateTimeField(_('create date'), editable=False)

    objects = ShopOrderQuerySet.as_manager()

    class Meta:
        verbose_name = _('order')
        verbose_name_plural = _('orders')
        ordering = ('-date', )

    def __str__(self):
        return _('Order #%04d') % self.pk

    def save(self, *args, **kwargs):
        if not self.date:
            self.date = now()

        super().save(*args, **kwargs)

    def clean(self):
        errors = {}

        # неподтвержденный заказ не может быть проверен или оплачен
        if not self.is_confirmed:
            if self.is_checked:
                errors['is_checked'] = _('Unconfirmed order can\'t be checked')
            if self.is_paid:
                errors['is_paid'] = _('Unconfirmed order can\'t be paid')

        if errors:
            raise ValidationError(errors)

    @property
    def str_order_type(self):
        return dict(self.ORDER_TYPE).get(self.order_type)

    @property
    def str_payment_type(self):
        return dict(self.PAYMENT_TYPES).get(self.payment_type)

    @property
    def profile_name(self):
        if self.payment_type == self.PAYMENT_ONLINE:
            return 'Онлайн платеж'
        elif self.payment_type == self.PAYMENT_CASH:
            return 'Наличные'
        elif self.payment_type == self.PAYMENT_CASHLESS:
            org = self.user.organizations.first() if self.user else None
            if org:
                name = org.short_name
                if not self.is_paid:
                    name += ', счет выставлен'
                return name
            else:
                return 'Счет выставлен'

    @property
    def total_cost(self):
        return self.products_cost


class OrderProduct(models.Model):
    """ Продукты заказа """
    order = models.ForeignKey(ShopOrder, verbose_name=_('order'), related_name='order_products')
    product = models.ForeignKey(ShopProduct, verbose_name=_('product'))
    order_price = ValuteField(_('price per item'), validators=[MinValueValidator(0)])
    count = models.PositiveSmallIntegerField(_('count'))

    class Meta:
        verbose_name = _('product')
        verbose_name_plural = _('products')
        unique_together = ('order', 'product')

    @property
    def total_cost(self):
        return self.order_price * self.count

    def __str__(self):
        return '%s (x%s)' % (self.product.title, self.count)


class EmailReciever(models.Model):
    config = models.ForeignKey(ShopConfig, related_name='recievers')
    email = models.EmailField(_('e-mail'))

    class Meta:
        verbose_name = _('e-mail reciever')
        verbose_name_plural = _('e-mail recievers')

    def __str__(self):
        return self.email


class OrderTypeOption(models.Model):
    config = models.ForeignKey(ShopConfig)

    order_type = models.PositiveSmallIntegerField(_('type'), choices=ShopOrder.ORDER_TYPE, default=ShopOrder.DELIVERY_TYPE, unique=True)

    note = models.CharField(_('note'), max_length=128, blank=True)
    period = models.CharField(_('period'), max_length=128, blank=True)
    cost = models.CharField(_('cost'), max_length=128, blank=True)

    class Meta:
        verbose_name = _('type option')
        verbose_name_plural = _('type options')

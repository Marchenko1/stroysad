from django.conf.urls import patterns, include, url
from api.views import APIView, APIFileView, APIExchangeView

urlpatterns = [
    url(r'files/(.*)/(.*)/', APIFileView.as_view(), name='api-file-action'),
    url(r'files/(.*)/', APIFileView.as_view(), name='api-root-files'),
    url(r'ping/$', APIView.as_view()),
    url(r'(.*)/(.*)/', APIView.as_view()),
    url(r'(.*)/(.*)/(.*)/', APIExchangeView.as_view()),
]
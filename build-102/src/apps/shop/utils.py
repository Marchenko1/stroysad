﻿from libs.email import send
from .models import EmailReciever


def mail_managers(request=None, subject=None, message_template=None, message_context=None):
    """ Отправка письма менеджерам магазина """
    recievers = EmailReciever.objects.all().values_list('email', flat=True)
    if recievers:
        send(request, recievers,
            subject=subject,
            template=message_template,
            context=message_context
        )

def mail_client(request=None, subject=None, message_template=None, message_context=None):
    """ Отправка письма клиентам магазина """
    order = message_context['order']
    receivers = [order.recipient.email, ]
    if receivers:
        send(request, receivers,
            subject=subject,
            template=message_template,
            context=message_context
        )

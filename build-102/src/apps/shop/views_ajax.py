﻿from django.shortcuts import get_object_or_404
from django.http.response import HttpResponseForbidden
from django.views.generic.base import TemplateView, View
from django.contrib.sites.shortcuts import get_current_site
from shop.signals import order_cancelled
from shop.models import ShopOrder, ShopConfig
from libs.views_ajax import AjaxViewMixin


class CancelOrderView(AjaxViewMixin, View):
    """ Отмена заказа """
    def post(self, request):
        order_id = request.POST.get('order_id')
        order = get_object_or_404(ShopOrder, pk=order_id)

        if not request.user.is_authenticated():
            return HttpResponseForbidden()

        if order.user != request.user:
            return HttpResponseForbidden()

        if order.is_cancelled or order.is_paid:
            return HttpResponseForbidden()

        try:
            order_cancelled.send(sender=ShopOrder, order=order, site=get_current_site(request))
        except Exception:
            pass

        return self.json_response()


class OrderCompleteView(AjaxViewMixin, TemplateView):
    template_name = 'shop/popup/order_complete.html'

    def get(self, request):
        return self.render_to_response({
            'config': ShopConfig.get_solo(),
        })

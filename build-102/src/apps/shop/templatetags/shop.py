from django.template import Library, loader
from ..models import ShopConfig, ShopCategory, OrderTypeOption
from ..cart import Cart, render_cart

register = Library()


@register.assignment_tag(takes_context=True)
def cart(context):
    request = context.get('request')
    if not request:
        return ''

    cart = Cart.from_session(request)
    if not cart:
        return ''

    return render_cart(request, cart)


@register.simple_tag(takes_context=True)
def categories_products(context):
    config = ShopConfig.get_solo()

    # оптимизация запроса
    categories = ShopCategory.objects.root_categories().filter(shop_main_visible=True)
    for category in categories:
        category.all_products = category.products.select_related('producer', 'discount')[:12]
        category.all_products = category.all_products.with_photos()
        for product in category.all_products:
            product.category = category

    return loader.render_to_string('shop/category_products.html', {
        'shop_config': config,
        'categories': categories,
    }, request=context.get('request'))


@register.filter(is_safe=True)
def type_option(value, option):
    order_type = OrderTypeOption.objects.filter(order_type=value).first()

    if not order_type:
        return ''

    if option == 'note':
        return order_type.note
    elif option == 'period':
        return order_type.period
    elif option == 'cost':
        return order_type.cost

    return ''

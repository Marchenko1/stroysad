﻿from itertools import chain
from django.utils.timezone import now
from django.http.response import Http404
from django.shortcuts import get_object_or_404
from django.views.generic.base import TemplateView
from django.shortcuts import resolve_url, redirect
from django.contrib.sites.shortcuts import get_current_site
from libs.cache.cached import cached
from libs.geocity.api import info
from libs.geocity.utils import get_client_ip
from libs.views import StringRenderMixin, CachedViewMixin
from seo.seo import Seo
from robokassa.forms import RobokassaForm
from paginator import Paginator, EmptyPage, get_paginator_meta
from blog.models import BlogPost
from address.forms import AddressForm
from main.models import MainPageConfig
from recipient.forms import RecipientForm
from .signals import order_confirmed
from .models import (
    ShopConfig, ShopCategory, ShopProduct, ShopOrder, OrderProduct,
    ShopProductProducer, ShopProducerDescription, DiscountConfig, PostCategory
)
from .forms import (
    ShopOrderTypeForm, ShopOrderPickupForm, ShopOrderPaymentForm,
    ShopOrderBoxberryForm, ShopCommentForm
)
from .cart import Cart
from blog.models import BlogConfig, BlogPost, Tag
from django.db.models import Q


class CategoryView(TemplateView):
    config = None
    category = None
    template_name = 'shop/category.html'

    def get(self, request, producer='', *args, **kwargs):

        self.config = ShopConfig.get_solo()
        discount_config = DiscountConfig.get_solo()

        try:
            self.category = ShopCategory.objects.get(alias=kwargs['category_alias'])
        except (ShopCategory.DoesNotExist, ShopCategory.MultipleObjectsReturned):
            raise Http404

        if not self.category.is_visible:
            raise Http404

        # Выбранный производитель
        selected_producer = get_object_or_404(ShopProductProducer, alias=producer) if producer else ''

        # Фильтрация продуктов по производителям
        producer_title = ''
        producer_description = ''
        producer_meta_title = ''
        producer_meta_description = ''
        if selected_producer:
            canonical = resolve_url('shop:category', category_alias=self.category.alias)
            products = self.category.products.filter(producer=selected_producer)

            try:
                shop_producer_data = ShopProducerDescription.objects.get(producer=selected_producer,
                                                                         category=self.category)
            except ShopProducerDescription.DoesNotExist:
                shop_producer_data = None

            if shop_producer_data:
                producer_title = shop_producer_data.title_producer
                producer_description = shop_producer_data.description
                producer_meta_title = shop_producer_data.meta_title
                producer_meta_description = shop_producer_data.meta_description

        else:
            canonical = ''
            products = self.category.products

        # Breadcrumbs
        parent_categories = self.category.get_ancestors()
        for category in parent_categories:
            request.breadcrumbs.add(category.title, resolve_url('shop:category', category_alias=category.alias))
        request.breadcrumbs.add(self.category.title)

        # Title категории
        category_title = '{category} купить в Москве, Самаре, Тольятти и Краснодаре | Строй Сад'.format(
            category=self.category.header,
        )

        # Description категории
        category_description = 'Выбрать и заказать {category} по выгодной цене. Стоимость товара в интернет-магазине Строй Сад.'.format(
            category=self.category.header.lower(),
        )

        # оптимизация запроса продуктов
        products = products.defer('category').select_related('gallery', 'producer', 'discount').with_photos()
        for product in products:
            product.category = self.category

        try:
            paginator = Paginator(
                request,
                object_list=products,
                per_page=60,
                page_neighbors=1,
                side_neighbors=1,
                allow_empty_first_page=False,
            )
        except EmptyPage:
            raise Http404

        if producer_meta_title != '':
            category_title = producer_meta_title

        # SEO
        seo = Seo()
        seo.title.clear()
        seo.set(get_paginator_meta(paginator))
        seo.set_data(self.category, defaults={
            # 'title': category_title,
            'description': category_description,
            'canonical': canonical,
        })

        if producer_meta_description:
            seo.set_description_hard(self.category, producer_meta_description)
            seo.title.clear()
            seo.title.append(category_title)
        seo.save(request, True)

        branch_category = self.category.get_ancestors(include_self=True)
        active_subcategory = PostCategory.objects.filter(category__pk__in=branch_category,
                                                         subcategory=True).values_list('post', flat=True)
        posts_category = PostCategory.objects.filter(category=self.category).values_list('post', flat=True)

        all_posts = set(chain(active_subcategory, posts_category))

        posts = BlogPost.objects.filter(pk__in=all_posts, visible=True)

        return self.render_to_response({
            'config': self.config,
            'current_root': self.category.get_root(),
            'current_category': self.category,
            'root_categories': ShopCategory.objects.root_categories(),
            'paginator': paginator,
            'producers': self.category.producers,
            'selected_producer': selected_producer,
            'producer_description': producer_description,
            'producer_title': producer_title,
            'discount_config': discount_config,
            'posts': posts,
            'producer_meta_description': producer_meta_description,
        })


class DetailView(StringRenderMixin, CachedViewMixin, TemplateView):
    config = None
    product = None
    category = None
    template_name = 'shop/detail.html'

    @cached('product.pk')
    def get_other_products(self, product):
        category = product.category
        while True:
            prods = category.products.exclude(pk=product.pk)
            if prods.count() < 6:
                if category.parent:
                    category = category.parent
                    continue
                else:
                    return ''

            other_prods = prods.order_by('?')[:6]
            return self.render_to_string('shop/products.html', {
                'products': other_prods,
            })

    def last_modified(self, request, *args, **kwargs):
        self.config = ShopConfig.get_solo()
        try:
            self.category = ShopCategory.objects.get(alias=kwargs['category_alias'])
        except (ShopCategory.DoesNotExist, ShopCategory.MultipleObjectsReturned):
            raise Http404

        try:
            self.product = ShopProduct.objects.get(alias=kwargs['alias'])
        except (ShopProduct.DoesNotExist, ShopProduct.MultipleObjectsReturned):
            raise Http404
        return self.product.updated

    def get(self, request, *args, **kwargs):
        if not self.category.is_visible or not self.product.is_visible:
            raise Http404

        if self.category != self.product.category:
            return redirect(self.product, permanent=True)
        else:
            self.category = self.product.category

        # Breadcrumbs
        parent_categories = self.category.get_ancestors(include_self=True)
        for category in parent_categories:
            request.breadcrumbs.add(category.title, resolve_url('shop:category', category_alias=category.alias))
        request.breadcrumbs.add(self.product.title)

        # SEO
        description_categories = []
        supercategory = self.category.get_ancestors(include_self=False).first()
        if supercategory:
            description_categories.append(supercategory.description_title.lower())
        description_categories.append(self.category.title.lower())

        # description = 'Заказать {categories} с доставкой. Купить {product} по выгодной стоимости — {price}р.'.format(
        description = '{product} в наличии, купить оптом и в розницу со склада в Москве, Казани, Самаре, Тольятти и ' \
                      'Краснодаре. Большой выбор сопутствующих товаров для автополива с доставкой по всей ' \
                      'России.'.format(
            categories=', '.join(description_categories),
            price=self.product.total_price.as_string(),
            product=self.product.title,
        )
        title = '{product} купить по цене {price}₽ в Москве, Казани, Самаре, Тольятти и Краснодаре'.format(
            product=self.product.title,
            price=self.product.total_price.as_string(),
        )
        seo = Seo()
        seo.set_data(self.product, defaults={
            'title': title,
            'description': description,
            'og_image': self.product.photo,
        })
        seo.save(request)

        return self.render_to_response({
            'config': self.config,
            'current_category': self.category,
            'current_product': self.product,
            'other_products': self.get_other_products(self.product),
        })


class DiscountView(TemplateView):
    config = None
    template_name = 'shop/discount.html'

    def get(self, request, *args, **kwargs):
        self.config = DiscountConfig.get_solo()

        # Breadcrumbs
        request.breadcrumbs.add(self.config.title)

        # SEO
        seo = Seo()
        seo.set_data(self.config, defaults={
            'title': self.config.header
        })
        seo.save(request)

        from django.db.models import Q

        products = ShopProduct.objects.filter(
            Q(discount__isnull=False, discount__is_visible=True, discount__start_date__lte=now(),
              discount__finish_date__gte=now()) |
            Q(category__discount__isnull=False, category__discount__is_visible=True,
              category__discount__start_date__lte=now(), category__discount__finish_date__gte=now()) |
            Q(producer__discount__isnull=False, producer__discount__is_visible=True,
              producer__discount__start_date__lte=now(), producer__discount__finish_date__gte=now())
        ).select_related('category', 'category__discount', 'gallery', 'producer', 'discount').distinct().with_photos()

        return self.render_to_response({
            'config': self.config,
            'products': tuple(products),
            'root_categories': ShopCategory.objects.root_categories(),
        })


class OrderCartView(TemplateView):
    config = None
    template_name = 'shop/order_cart.html'

    def init_boxberry(self, request, shop_config):
        city = info(get_client_ip(request), detailed=True)
        if city:
            city = city['city']['name_ru']
        else:
            city = 'Москва'
        request.js_storage.update(
            user_city=city,
            boxberry_weight=shop_config.boxberry_weight,
        )

    def get(self, request, *args, **kwargs):
        self.config = ShopConfig.get_solo()
        self.init_boxberry(request, self.config)

        cart = Cart.from_session(request)
        if not cart:
            return redirect('index')

        recipient_form = RecipientForm(prefix='recipient')
        order_type_form = ShopOrderTypeForm(prefix='order_type')
        delivery_address_form = AddressForm(prefix='delivery-address')
        pickup_address_form = ShopOrderPickupForm(prefix='pickup-address')
        boxberry_address_form = ShopOrderBoxberryForm(prefix='boxberry-address')
        payment_type_form = ShopOrderPaymentForm(prefix='payment_type')
        comment_form = ShopCommentForm(prefix='comment')

        seo = Seo()
        seo.set_data(self.config, defaults={
            'title': self.config.title,
        })
        seo.save(request)

        return self.render_to_response({
            'cart': cart,
            'recipient_form': recipient_form,
            'order_type_form': order_type_form,
            'delivery_address_form': delivery_address_form,
            'pickup_address_form': pickup_address_form,
            'boxberry_address_form': boxberry_address_form,
            'payment_type_form': payment_type_form,
            'comment_form': comment_form,
        })

    def post(self, request):
        self.config = ShopConfig.get_solo()
        self.init_boxberry(request, self.config)

        main_config = MainPageConfig.get_solo()
        cart = Cart.from_data(request.POST)
        if not cart:
            try:
                del request.session['user_order']
            except KeyError:
                pass
            response = redirect('index')
            cart.clear(request, response)
            return response
        else:
            cart.to_session(request)

        order_type_form = ShopOrderTypeForm(request.POST, request.FILES, prefix='order_type')
        recipient_form = RecipientForm(request.POST, request.FILES, prefix='recipient')
        delivery_address_form = AddressForm(request.POST, request.FILES, prefix='delivery-address')
        pickup_address_form = ShopOrderPickupForm(request.POST, request.FILES, prefix='pickup-address')
        boxberry_address_form = ShopOrderBoxberryForm(request.POST, request.FILES, prefix='boxberry-address')
        payment_type_form = ShopOrderPaymentForm(request.POST, request.FILES, prefix='payment_type')
        comment_form = ShopCommentForm(request.POST, request.FILES, prefix='comment')

        is_recipient_valid = recipient_form.is_valid()
        is_order_type_valid = order_type_form.is_valid()
        is_payment_type_valid = payment_type_form.is_valid()
        is_comment_valid = comment_form.is_valid()

        order_type = order_type_form.cleaned_data['order_type']
        subform_valid = order_type not in [ShopOrder.DELIVERY_TYPE, ShopOrder.PICKUP_TYPE, ShopOrder.BOXBERRY_TYPE] or \
                        (order_type == ShopOrder.DELIVERY_TYPE and delivery_address_form.is_valid()) or \
                        (order_type == ShopOrder.PICKUP_TYPE and pickup_address_form.is_valid()) or \
                        (order_type == ShopOrder.BOXBERRY_TYPE and boxberry_address_form.is_valid())

        if (is_recipient_valid and is_order_type_valid and is_payment_type_valid and
                is_comment_valid and subform_valid):
            pass
        else:
            return self.render_to_response({
                'cart': cart,
                'recipient_form': recipient_form,
                'order_type_form': order_type_form,
                'delivery_address_form': delivery_address_form,
                'pickup_address_form': pickup_address_form,
                'boxberry_address_form': boxberry_address_form,
                'payment_type_form': payment_type_form,
                'comment_form': comment_form,
            })
        recipient = recipient_form.save()

        # создание заказа
        order = ShopOrder(
            recipient=recipient,
            order_type=order_type_form.cleaned_data['order_type'],
            comment=comment_form.cleaned_data['comment'],
            payment_type=payment_type_form.cleaned_data['payment_type'],
        )
        order.full_clean()
        order.save()

        if order.order_type == ShopOrder.PICKUP_TYPE:
            pickup_address = pickup_address_form.cleaned_data['warehouse']
            order.warehouse = pickup_address
        elif order.order_type == ShopOrder.DELIVERY_TYPE:
            delivery_address = delivery_address_form.save()
            order.address = delivery_address
        elif order.order_type == ShopOrder.BOXBERRY_TYPE:
            order.boxberry_city = boxberry_address_form.cleaned_data['boxberry_city']
            order.boxberry_address = boxberry_address_form.cleaned_data['boxberry_address']
            order.boxberry_price = boxberry_address_form.cleaned_data['boxberry_price']

        # привязка товаров
        for product, count in cart.products_counts:
            order_prod = OrderProduct(
                order=order,
                product=product,
                order_price=product.total_price,
                count=count
            )
            order_prod.full_clean()
            order_prod.save()

        order.products_cost = cart.total_price
        order.save()

        # подтверждение заказа
        try:
            order_confirmed.send(sender=ShopOrder, order=order, site=get_current_site(request))
        except Exception:
            pass

        if order.payment_type == ShopOrder.PAYMENT_ONLINE:
            # Robokassa
            robokassa = RobokassaForm(initial={
                'InvId': order.pk,
                'OutSum': order.total_cost.as_decimal(),
                'Desc': order.profile_name,
            })
            response = redirect(robokassa.get_redirect_url())
        else:
            response = redirect(main_config.get_absolute_url() + '#order_complete')

        # очистка корзины
        try:
            del request.session['user_order']
        except KeyError:
            pass
        cart.clear(request, response)

        return response

# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models
import libs.valute_field.fields
import django.core.validators


class Migration(migrations.Migration):

    dependencies = [
        ('shop', '0002_auto_20170705_1234'),
    ]

    operations = [
        migrations.AddField(
            model_name='shoporder',
            name='boxberry_address',
            field=models.CharField(max_length=512, blank=True, verbose_name='address'),
        ),
        migrations.AddField(
            model_name='shoporder',
            name='boxberry_city',
            field=models.CharField(max_length=256, blank=True, verbose_name='city'),
        ),
        migrations.AddField(
            model_name='shoporder',
            name='boxberry_price',
            field=libs.valute_field.fields.ValuteField(validators=[django.core.validators.MinValueValidator(0)], blank=True, verbose_name='price'),
        ),
        migrations.AddField(
            model_name='shoporder',
            name='boxberry_zip',
            field=models.CharField(max_length=64, blank=True, verbose_name='zip'),
        ),
    ]

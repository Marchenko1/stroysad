# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models
import ckeditor.fields


class Migration(migrations.Migration):

    dependencies = [
        ('shop', '0007_auto_20220601_1430'),
    ]

    operations = [
        migrations.AlterField(
            model_name='shopproducerdescription',
            name='description',
            field=ckeditor.fields.CKEditorUploadField(blank=True, verbose_name='description'),
        ),
        migrations.AlterField(
            model_name='shopproducerdescription',
            name='title_producer',
            field=models.CharField(blank=True, max_length=1024, verbose_name='producer title'),
        ),
    ]

# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models
import ckeditor.fields


class Migration(migrations.Migration):

    dependencies = [
        ('shop', '0006_auto_20210127_0937'),
    ]

    operations = [
        migrations.AddField(
            model_name='shopproducerdescription',
            name='title_producer',
            field=models.CharField(max_length=1024, verbose_name='title_producer', blank=True),
        ),
        migrations.AlterField(
            model_name='shopcategory',
            name='description',
            field=ckeditor.fields.CKEditorUploadField(verbose_name='short description', blank=True),
        ),
        migrations.AlterField(
            model_name='shopcategory',
            name='short_description',
            field=ckeditor.fields.CKEditorUploadField(verbose_name='short description', blank=True),
        ),
        migrations.AlterField(
            model_name='shopcategory',
            name='text_additional',
            field=ckeditor.fields.CKEditorUploadField(verbose_name='short description', blank=True),
        ),
    ]

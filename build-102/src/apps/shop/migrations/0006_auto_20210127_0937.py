# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('shop', '0005_shopconfig_boxberry_weight'),
    ]

    operations = [
        migrations.AlterField(
            model_name='shopconfig',
            name='boxberry_weight',
            field=models.PositiveIntegerField(default=500, help_text='g', verbose_name='default order weight'),
        ),
    ]

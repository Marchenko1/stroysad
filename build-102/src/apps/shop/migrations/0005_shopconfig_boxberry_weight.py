# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('shop', '0004_remove_shoporder_boxberry_zip'),
    ]

    operations = [
        migrations.AddField(
            model_name='shopconfig',
            name='boxberry_weight',
            field=models.PositiveIntegerField(default=500, verbose_name='weight'),
        ),
    ]

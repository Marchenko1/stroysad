# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models
import libs.videolink_field.fields
import uuid
import django.db.models.deletion
import mptt.fields
import gallery.models
import files.models
import gallery.fields
import django.utils.timezone
from django.conf import settings
import django.core.validators
import libs.storages.media_storage
import libs.autoslug
import libs.valute_field.fields
import ckeditor.fields


class Migration(migrations.Migration):

    dependencies = [
        ('address', '0002_auto_20161122_1742'),
        ('blog', '0003_auto_20170420_1155'),
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
        ('gallery', '0003_auto_20170526_1722'),
        ('recipient', '0002_recipient_email'),
        ('users', '0001_initial'),
    ]

    operations = [
        migrations.CreateModel(
            name='Discount',
            fields=[
                ('id', models.AutoField(primary_key=True, serialize=False, verbose_name='ID', auto_created=True)),
                ('title', models.CharField(verbose_name='discount title', max_length=128)),
                ('value', models.PositiveSmallIntegerField(validators=[django.core.validators.MinValueValidator(1)], verbose_name='value')),
                ('unit', models.PositiveSmallIntegerField(choices=[(1, 'Percentage discount'), (2, 'Fixed monetary amount')], verbose_name='unit', default=1)),
                ('start_date', models.DateTimeField(verbose_name='start date')),
                ('finish_date', models.DateTimeField(verbose_name='finish date')),
                ('is_visible', models.BooleanField(verbose_name='visible', default=True)),
                ('created', models.DateTimeField(editable=False, verbose_name='create date', default=django.utils.timezone.now)),
                ('updated', models.DateTimeField(verbose_name='change date', auto_now=True)),
            ],
            options={
                'verbose_name': 'discount',
                'verbose_name_plural': 'discounts',
                'ordering': ('-created',),
            },
        ),
        migrations.CreateModel(
            name='DiscountConfig',
            fields=[
                ('id', models.AutoField(primary_key=True, serialize=False, verbose_name='ID', auto_created=True)),
                ('header', models.CharField(verbose_name='header', max_length=128)),
                ('title', models.CharField(verbose_name='Title', max_length=128)),
                ('description', ckeditor.fields.CKEditorField(blank=True, verbose_name='description')),
                ('updated', models.DateTimeField(verbose_name='change date', auto_now=True)),
            ],
            options={
                'verbose_name': 'discount settings',
            },
        ),
        migrations.CreateModel(
            name='EmailReciever',
            fields=[
                ('id', models.AutoField(primary_key=True, serialize=False, verbose_name='ID', auto_created=True)),
                ('email', models.EmailField(verbose_name='e-mail', max_length=254)),
            ],
            options={
                'verbose_name': 'e-mail reciever',
                'verbose_name_plural': 'e-mail recievers',
            },
        ),
        migrations.CreateModel(
            name='Manual',
            fields=[
                ('id', models.AutoField(primary_key=True, serialize=False, verbose_name='ID', auto_created=True)),
                ('file', models.FileField(storage=libs.storages.media_storage.MediaStorage(), verbose_name='file', upload_to=files.models.generate_filepath, max_length=150)),
                ('displayed_name', models.CharField(blank=True, help_text='If you leave it empty the file name will be used', verbose_name='display name', max_length=150)),
                ('sort_order', models.PositiveIntegerField(verbose_name='sort order')),
            ],
            options={
                'verbose_name_plural': 'files',
                'verbose_name': 'file',
                'abstract': False,
                'ordering': ('sort_order',),
            },
        ),
        migrations.CreateModel(
            name='OrderProduct',
            fields=[
                ('id', models.AutoField(primary_key=True, serialize=False, verbose_name='ID', auto_created=True)),
                ('order_price', libs.valute_field.fields.ValuteField(validators=[django.core.validators.MinValueValidator(0)], verbose_name='price per item')),
                ('count', models.PositiveSmallIntegerField(verbose_name='count')),
            ],
            options={
                'verbose_name': 'product',
                'verbose_name_plural': 'products',
            },
        ),
        migrations.CreateModel(
            name='OrderTypeOption',
            fields=[
                ('id', models.AutoField(primary_key=True, serialize=False, verbose_name='ID', auto_created=True)),
                ('order_type', models.PositiveSmallIntegerField(unique=True, choices=[(1, 'Delivery'), (2, 'Pickup'), (3, 'Boxberry'), (4, 'Transport Company')], verbose_name='type', default=1)),
                ('note', models.CharField(blank=True, verbose_name='note', max_length=128)),
                ('period', models.CharField(blank=True, verbose_name='period', max_length=128)),
                ('cost', models.CharField(blank=True, verbose_name='cost', max_length=128)),
            ],
            options={
                'verbose_name': 'type option',
                'verbose_name_plural': 'type options',
            },
        ),
        migrations.CreateModel(
            name='PostCategory',
            fields=[
                ('id', models.AutoField(primary_key=True, serialize=False, verbose_name='ID', auto_created=True)),
                ('subcategory', models.BooleanField(verbose_name='for subcategory', default=False)),
            ],
            options={
                'verbose_name': 'Post category',
                'verbose_name_plural': 'Posts category',
                'ordering': ('-post',),
            },
        ),
        migrations.CreateModel(
            name='ShopCategory',
            fields=[
                ('id', models.AutoField(primary_key=True, serialize=False, verbose_name='ID', auto_created=True)),
                ('title', models.CharField(verbose_name='category title', max_length=128)),
                ('title_desc', models.CharField(blank=True, verbose_name='title for description', max_length=128)),
                ('alias', libs.autoslug.AutoSlugField(unique=True, help_text='Leave it blank to auto generate it', populate_from=('title',), verbose_name='alias')),
                ('video', libs.videolink_field.fields.VideoLinkField(blank=True, verbose_name='video', providers=set(['youtube']))),
                ('video_title', models.CharField(blank=True, verbose_name='video title', max_length=128)),
                ('header', models.CharField(verbose_name='header', max_length=128)),
                ('short_description', ckeditor.fields.CKEditorField(blank=True, verbose_name='short description')),
                ('description', ckeditor.fields.CKEditorField(blank=True, verbose_name='description')),
                ('text_additional', ckeditor.fields.CKEditorField(blank=True, verbose_name='text additional')),
                ('is_visible', models.BooleanField(default=True, verbose_name='visible', db_index=True)),
                ('shop_main_visible', models.BooleanField(help_text='visible on shop main', default=True, verbose_name='visible', db_index=True)),
                ('product_count', models.PositiveIntegerField(help_text='count of immediate visible products', editable=False, verbose_name='product count', default=0)),
                ('total_product_count', models.PositiveIntegerField(help_text='count of visible products', editable=False, verbose_name='total product count', default=0)),
                ('shop_line', models.PositiveIntegerField(help_text='shop main product line count', choices=[(1, '1'), (2, '2')], verbose_name='line count', default=1)),
                ('sort_order', models.PositiveIntegerField(verbose_name='sort order')),
                ('created', models.DateTimeField(editable=False, verbose_name='create date', default=django.utils.timezone.now)),
                ('updated', models.DateTimeField(verbose_name='change date', auto_now=True)),
                ('lft', models.PositiveIntegerField(editable=False, db_index=True)),
                ('rght', models.PositiveIntegerField(editable=False, db_index=True)),
                ('tree_id', models.PositiveIntegerField(editable=False, db_index=True)),
                ('level', models.PositiveIntegerField(editable=False, db_index=True)),
                ('discount', models.ForeignKey(blank=True, on_delete=django.db.models.deletion.SET_NULL, to='shop.Discount', verbose_name='discount', null=True)),
                ('parent', mptt.fields.TreeForeignKey(related_name='children', blank=True, to='shop.ShopCategory', verbose_name='parent category', null=True)),
            ],
            options={
                'verbose_name': 'category',
                'verbose_name_plural': 'categories',
            },
        ),
        migrations.CreateModel(
            name='ShopConfig',
            fields=[
                ('id', models.AutoField(primary_key=True, serialize=False, verbose_name='ID', auto_created=True)),
                ('header', models.CharField(verbose_name='header', max_length=128)),
                ('title', models.CharField(verbose_name='Title', max_length=128)),
                ('description', ckeditor.fields.CKEditorField(blank=True, verbose_name='description')),
                ('updated', models.DateTimeField(verbose_name='change date', auto_now=True)),
                ('online_payment_limit', libs.valute_field.fields.ValuteField(verbose_name='online payment limit')),
                ('payment_text', models.TextField(verbose_name='payment text')),
            ],
            options={
                'verbose_name': 'settings',
            },
        ),
        migrations.CreateModel(
            name='ShopOrder',
            fields=[
                ('id', models.AutoField(primary_key=True, serialize=False, verbose_name='ID', auto_created=True)),
                ('uuid', models.UUIDField(editable=False, unique=True, verbose_name='UUID', default=uuid.uuid4)),
                ('products_cost', libs.valute_field.fields.ValuteField(editable=False, verbose_name='products cost', validators=[django.core.validators.MinValueValidator(0)])),
                ('step', models.PositiveSmallIntegerField(verbose_name='current step', default=1)),
                ('order_type', models.PositiveSmallIntegerField(choices=[(1, 'Delivery'), (2, 'Pickup'), (3, 'Boxberry'), (4, 'Transport Company')], verbose_name='type', default=1)),
                ('comment', models.TextField(blank=True, verbose_name='comment')),
                ('payment_type', models.PositiveSmallIntegerField(choices=[(1, 'Cash payment'), (2, 'Cashless payment'), (3, 'Online payment')], verbose_name='payment type', default=3)),
                ('is_confirmed', models.BooleanField(help_text='Confirmed by the user', editable=False, verbose_name='confirmed', default=False)),
                ('confirm_date', models.DateTimeField(editable=False, verbose_name='confirm date', null=True)),
                ('is_cancelled', models.BooleanField(verbose_name='cancelled', default=False)),
                ('cancel_date', models.DateTimeField(editable=False, verbose_name='cancel date', null=True)),
                ('is_checked', models.BooleanField(verbose_name='checked', default=False)),
                ('check_date', models.DateTimeField(editable=False, verbose_name='check date', null=True)),
                ('is_paid', models.BooleanField(verbose_name='paid', default=False)),
                ('pay_date', models.DateTimeField(editable=False, verbose_name='pay date', null=True)),
                ('is_archived', models.BooleanField(verbose_name='archived', default=False)),
                ('archivation_date', models.DateTimeField(editable=False, verbose_name='archivation date', null=True)),
                ('date', models.DateTimeField(editable=False, verbose_name='create date')),
                ('address', models.ForeignKey(related_name='+', blank=True, on_delete=django.db.models.deletion.SET_NULL, to='address.Address', verbose_name='address', null=True)),
                ('recipient', models.ForeignKey(related_name='+', blank=True, on_delete=django.db.models.deletion.SET_NULL, to='recipient.Recipient', verbose_name='recipient', null=True)),
                ('user', models.ForeignKey(related_name='orders', blank=True, to=settings.AUTH_USER_MODEL, verbose_name='user', null=True)),
                ('user_address', models.ForeignKey(related_name='+', blank=True, on_delete=django.db.models.deletion.SET_NULL, to='users.UserAddress', null=True)),
                ('user_recipient', models.ForeignKey(related_name='+', blank=True, on_delete=django.db.models.deletion.SET_NULL, to='users.UserRecipient', null=True)),
            ],
            options={
                'verbose_name': 'order',
                'verbose_name_plural': 'orders',
                'ordering': ('-date',),
            },
        ),
        migrations.CreateModel(
            name='ShopProducerDescription',
            fields=[
                ('id', models.AutoField(primary_key=True, serialize=False, verbose_name='ID', auto_created=True)),
                ('description', ckeditor.fields.CKEditorField(blank=True, verbose_name='description')),
                ('meta_title', models.CharField(blank=True, verbose_name='title', max_length=128)),
                ('meta_description', models.TextField(blank=True, verbose_name='meta description', max_length=255)),
                ('category', models.ForeignKey(to='shop.ShopCategory', verbose_name='category')),
            ],
            options={
                'verbose_name': 'producer description',
                'verbose_name_plural': 'producers description',
            },
        ),
        migrations.CreateModel(
            name='ShopProduct',
            fields=[
                ('id', models.AutoField(primary_key=True, serialize=False, verbose_name='ID', auto_created=True)),
                ('title', models.CharField(verbose_name='title', max_length=128)),
                ('alias', libs.autoslug.AutoSlugField(unique=True, help_text='Leave it blank to auto generate it', populate_from=('title',), verbose_name='alias')),
                ('serial', models.SlugField(help_text='Unique identifier of the product', unique=True, verbose_name='serial number', max_length=64)),
                ('description', ckeditor.fields.CKEditorField(blank=True, verbose_name='description')),
                ('video_title', models.CharField(blank=True, verbose_name='video title', max_length=128)),
                ('video', libs.videolink_field.fields.VideoLinkField(blank=True, verbose_name='video', providers=set(['youtube']))),
                ('price', libs.valute_field.fields.ValuteField(validators=[django.core.validators.MinValueValidator(0)], verbose_name='price')),
                ('is_visible', models.BooleanField(verbose_name='visible', default=True)),
                ('is_hot', models.BooleanField(help_text='priority show this product on main page', verbose_name='hot', default=False)),
                ('created', models.DateTimeField(editable=False, verbose_name='create date', default=django.utils.timezone.now)),
                ('updated', models.DateTimeField(verbose_name='change date', auto_now=True)),
                ('category', models.ForeignKey(related_name='immediate_products', to='shop.ShopCategory', verbose_name='category')),
                ('discount', models.ForeignKey(blank=True, on_delete=django.db.models.deletion.SET_NULL, to='shop.Discount', verbose_name='discount', null=True)),
            ],
            options={
                'ordering': ('-is_hot', '-created'),
                'verbose_name_plural': 'products',
                'verbose_name': 'product',
            },
        ),
        migrations.CreateModel(
            name='ShopProductGallery',
            fields=[
                ('id', models.AutoField(primary_key=True, serialize=False, verbose_name='ID', auto_created=True)),
            ],
            options={
                'verbose_name_plural': 'galleries',
                'verbose_name': 'gallery',
                'abstract': False,
                'default_permissions': (),
            },
        ),
        migrations.CreateModel(
            name='ShopProductGalleryImageItem',
            fields=[
                ('galleryitembase_ptr', models.OneToOneField(primary_key=True, serialize=False, to='gallery.GalleryItemBase', auto_created=True, parent_link=True)),
                ('image', gallery.fields.GalleryImageField(storage=libs.storages.media_storage.MediaStorage(), verbose_name='image', upload_to=gallery.models.generate_filepath)),
                ('image_crop', models.CharField(blank=True, editable=False, verbose_name='stored_crop', max_length=32)),
                ('image_alt', models.CharField(blank=True, verbose_name='alt', max_length=255)),
            ],
            options={
                'verbose_name_plural': 'image items',
                'ordering': ('object_id', 'sort_order', 'created'),
                'verbose_name': 'image item',
                'abstract': False,
                'default_permissions': (),
            },
            bases=('gallery.galleryitembase',),
        ),
        migrations.CreateModel(
            name='ShopProductProducer',
            fields=[
                ('id', models.AutoField(primary_key=True, serialize=False, verbose_name='ID', auto_created=True)),
                ('title', models.CharField(unique=True, verbose_name='title', max_length=128)),
                ('alias', libs.autoslug.AutoSlugField(unique=True, help_text='Leave it blank to auto generate it', populate_from=('title',), verbose_name='alias')),
                ('discount', models.ForeignKey(blank=True, on_delete=django.db.models.deletion.SET_NULL, to='shop.Discount', verbose_name='discount', null=True)),
            ],
            options={
                'verbose_name': 'producer',
                'verbose_name_plural': 'producers',
                'ordering': ('title',),
            },
        ),
        migrations.CreateModel(
            name='ShopWarehouse',
            fields=[
                ('id', models.AutoField(primary_key=True, serialize=False, verbose_name='ID', auto_created=True)),
                ('city', models.CharField(verbose_name='city', max_length=128, db_index=True)),
                ('address', models.CharField(verbose_name='address', max_length=255)),
                ('active', models.BooleanField(verbose_name='active', default=False)),
            ],
            options={
                'verbose_name': 'warehouse address',
                'verbose_name_plural': 'warehouse addresses',
                'ordering': ('city', 'address'),
            },
        ),
        migrations.AddField(
            model_name='shopproduct',
            name='gallery',
            field=gallery.fields.GalleryField(blank=True, on_delete=django.db.models.deletion.SET_NULL, to='shop.ShopProductGallery', verbose_name='gallery', null=True),
        ),
        migrations.AddField(
            model_name='shopproduct',
            name='producer',
            field=models.ForeignKey(related_name='products', blank=True, on_delete=django.db.models.deletion.SET_NULL, to='shop.ShopProductProducer', verbose_name='producer', null=True),
        ),
        migrations.AddField(
            model_name='shopproducerdescription',
            name='producer',
            field=models.ForeignKey(to='shop.ShopProductProducer', verbose_name='producer'),
        ),
        migrations.AddField(
            model_name='shoporder',
            name='warehouse',
            field=models.ForeignKey(related_name='+', blank=True, to='shop.ShopWarehouse', verbose_name='warehouse', null=True),
        ),
        migrations.AddField(
            model_name='postcategory',
            name='category',
            field=models.ForeignKey(to='shop.ShopCategory'),
        ),
        migrations.AddField(
            model_name='postcategory',
            name='post',
            field=models.ForeignKey(to='blog.BlogPost', verbose_name='post'),
        ),
        migrations.AddField(
            model_name='ordertypeoption',
            name='config',
            field=models.ForeignKey(to='shop.ShopConfig'),
        ),
        migrations.AddField(
            model_name='orderproduct',
            name='order',
            field=models.ForeignKey(related_name='order_products', to='shop.ShopOrder', verbose_name='order'),
        ),
        migrations.AddField(
            model_name='orderproduct',
            name='product',
            field=models.ForeignKey(to='shop.ShopProduct', verbose_name='product'),
        ),
        migrations.AddField(
            model_name='manual',
            name='manual',
            field=models.ForeignKey(related_name='files', to='shop.ShopProduct'),
        ),
        migrations.AddField(
            model_name='emailreciever',
            name='config',
            field=models.ForeignKey(related_name='recievers', to='shop.ShopConfig'),
        ),
        migrations.AlterIndexTogether(
            name='shopproduct',
            index_together=set([('category', 'is_visible', 'producer')]),
        ),
        migrations.AlterUniqueTogether(
            name='shopproducerdescription',
            unique_together=set([('producer', 'category')]),
        ),
        migrations.AlterUniqueTogether(
            name='orderproduct',
            unique_together=set([('order', 'product')]),
        ),
    ]

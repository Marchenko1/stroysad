# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('users', '0001_initial'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='organization',
            name='user',
        ),
        migrations.RemoveField(
            model_name='useraddress',
            name='address',
        ),
        migrations.RemoveField(
            model_name='useraddress',
            name='user',
        ),
        migrations.RemoveField(
            model_name='userrecipient',
            name='recipient',
        ),
        migrations.RemoveField(
            model_name='userrecipient',
            name='user',
        ),
        migrations.AlterModelOptions(
            name='customuser',
            options={'verbose_name': 'user', 'verbose_name_plural': 'users', 'permissions': (('admin_menu', 'Can see hidden menu items'),)},
        ),
        migrations.RemoveField(
            model_name='customuser',
            name='birth_date',
        ),
        migrations.RemoveField(
            model_name='customuser',
            name='phone',
        ),
        migrations.DeleteModel(
            name='Organization',
        ),
        migrations.DeleteModel(
            name='UserAddress',
        ),
        migrations.DeleteModel(
            name='UserRecipient',
        ),
    ]

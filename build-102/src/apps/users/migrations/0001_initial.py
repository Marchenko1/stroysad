# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models
from django.conf import settings
import libs.storages
import django.utils.timezone
import django.contrib.auth.models
import django.core.validators
import libs.stdimage.fields


class Migration(migrations.Migration):

    dependencies = [
        ('address', '0001_initial'),
        ('recipient', '__first__'),
        ('auth', '0006_require_contenttypes_0002'),
    ]

    operations = [
        migrations.CreateModel(
            name='CustomUser',
            fields=[
                ('id', models.AutoField(primary_key=True, auto_created=True, verbose_name='ID', serialize=False)),
                ('password', models.CharField(max_length=128, verbose_name='password')),
                ('last_login', models.DateTimeField(blank=True, null=True, verbose_name='last login')),
                ('is_superuser', models.BooleanField(verbose_name='superuser status', default=False, help_text='Designates that this user has all permissions without explicitly assigning them.')),
                ('username', models.CharField(max_length=30, unique=True, validators=[django.core.validators.RegexValidator('^[\\w.@+-]+$', 'Enter a valid username. This value may contain only letters, numbers and @/./+/-/_ characters.', 'invalid')], help_text='Required. 30 characters or fewer. Letters, digits and @/./+/-/_ only.', verbose_name='username', error_messages={'unique': 'A user with that username already exists.'})),
                ('first_name', models.CharField(blank=True, verbose_name='first name', max_length=30)),
                ('last_name', models.CharField(blank=True, verbose_name='last name', max_length=30)),
                ('email', models.EmailField(blank=True, verbose_name='email address', max_length=254)),
                ('is_staff', models.BooleanField(verbose_name='staff status', default=False, help_text='Designates whether the user can log into this admin site.')),
                ('is_active', models.BooleanField(verbose_name='active', default=True, help_text='Designates whether this user should be treated as active. Unselect this instead of deleting accounts.')),
                ('date_joined', models.DateTimeField(verbose_name='date joined', default=django.utils.timezone.now)),
                ('birth_date', models.DateField(blank=True, null=True, verbose_name='birth date')),
                ('phone', models.CharField(blank=True, verbose_name='phone number', max_length=24)),
                ('avatar', libs.stdimage.fields.StdImageField(blank=True, storage=libs.storages.MediaStorage('users/avatar'), verbose_name='avatar', aspects='normal', upload_to='', variations={'small': {'size': (50, 50)}, 'micro': {'size': (32, 32)}, 'normal': {'size': (150, 150)}}, min_dimensions=(150, 150))),
                ('avatar_crop', models.CharField(blank=True, editable=False, verbose_name='stored_crop', max_length=32)),
                ('groups', models.ManyToManyField(blank=True, verbose_name='groups', to='auth.Group', help_text='The groups this user belongs to. A user will get all permissions granted to each of their groups.', related_query_name='user', related_name='user_set')),
                ('user_permissions', models.ManyToManyField(blank=True, verbose_name='user permissions', to='auth.Permission', help_text='Specific permissions for this user.', related_query_name='user', related_name='user_set')),
            ],
            options={
                'abstract': False,
                'verbose_name': 'user',
                'verbose_name_plural': 'users',
            },
            managers=[
                ('objects', django.contrib.auth.models.UserManager()),
            ],
        ),
        migrations.CreateModel(
            name='Organization',
            fields=[
                ('id', models.AutoField(primary_key=True, auto_created=True, verbose_name='ID', serialize=False)),
                ('short_name', models.CharField(max_length=128, verbose_name='short company name')),
                ('full_name', models.CharField(max_length=255, verbose_name='full company name')),
                ('itn', models.CharField(max_length=12, verbose_name='itn', validators=[django.core.validators.RegexValidator('\\d+'), django.core.validators.MinLengthValidator(12)], error_messages={'invalid': 'ITN should be a 12-digit number'})),
                ('zip', models.CharField(max_length=9, verbose_name='zip', validators=[django.core.validators.RegexValidator('\\d+')], error_messages={'invalid': 'ZIP must contain only digits'})),
                ('city', models.CharField(max_length=128, verbose_name='city')),
                ('address', models.TextField(verbose_name='address')),
                ('bank_name', models.CharField(max_length=128, verbose_name='bank')),
                ('bank_city', models.CharField(max_length=128, verbose_name='bank city')),
                ('bic', models.CharField(max_length=9, verbose_name='bic', validators=[django.core.validators.RegexValidator('\\d+'), django.core.validators.MinLengthValidator(9)], error_messages={'invalid': 'BIC should be a 9-digit number'})),
                ('correspondent', models.CharField(max_length=20, verbose_name='correspondent account', validators=[django.core.validators.RegexValidator('\\d+'), django.core.validators.MinLengthValidator(20)], error_messages={'invalid': 'Correspondent account should be a 20-digit number'})),
                ('giro', models.CharField(max_length=20, verbose_name='giro', validators=[django.core.validators.RegexValidator('\\d+'), django.core.validators.MinLengthValidator(20)], error_messages={'invalid': 'Giro should be a 20-digit number'})),
                ('phone', models.CharField(max_length=24, verbose_name='contact phone number')),
                ('email', models.EmailField(max_length=254, verbose_name='contact e-mail')),
                ('ceo', models.CharField(max_length=255, verbose_name='ceo')),
                ('accountant', models.CharField(max_length=255, verbose_name='accountant')),
                ('user', models.ForeignKey(verbose_name='user', to=settings.AUTH_USER_MODEL, related_name='organizations')),
            ],
            options={
                'ordering': ('short_name',),
                'verbose_name': 'organization',
                'verbose_name_plural': 'organizations',
            },
        ),
        migrations.CreateModel(
            name='UserAddress',
            fields=[
                ('id', models.AutoField(primary_key=True, auto_created=True, verbose_name='ID', serialize=False)),
                ('address', models.OneToOneField(to='address.Address', verbose_name='address', related_name='+')),
                ('user', models.ForeignKey(verbose_name='user', to=settings.AUTH_USER_MODEL, related_name='addresses')),
            ],
            options={
                'verbose_name': 'delivery address',
                'verbose_name_plural': 'delivery addresses',
            },
        ),
        migrations.CreateModel(
            name='UserRecipient',
            fields=[
                ('id', models.AutoField(primary_key=True, auto_created=True, verbose_name='ID', serialize=False)),
                ('recipient', models.ForeignKey(verbose_name='recipient', to='recipient.Recipient', related_name='+')),
                ('user', models.ForeignKey(verbose_name='user', to=settings.AUTH_USER_MODEL, related_name='recipients')),
            ],
            options={
                'verbose_name': 'user recipient',
                'verbose_name_plural': 'user recipient',
            },
        ),
    ]

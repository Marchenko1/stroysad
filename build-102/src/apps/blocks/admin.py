from django import forms
from django.contrib import admin
from django.utils.translation import ugettext_lazy as _
from suit.admin import SortableStackedInline
from project.admin import ModelAdminMixin, ModelAdminInlineMixin
from shop.models import ShopCategory, ShopProduct
from libs.autocomplete import AutocompleteWidget
from .models import SliderBlock, SliderSlide, SuppliersBlock, Supplier


class SliderSlideForm(forms.ModelForm):
    category = forms.ModelChoiceField(
        label=_('Category'),
        required=False,
        queryset=ShopCategory.objects.all(),
        widget=AutocompleteWidget(
            format_item=ShopCategory.autocomplete_item,
            attrs={
                'style': 'width:50%',
            }
        )
    )

    product = forms.ModelChoiceField(
        label=_('Product'),
        required=False,
        queryset=ShopProduct.objects.all().order_by('title'),
        widget=AutocompleteWidget(
            filters=(
                ('supercategory', '__prefix__-category', False),
            ),
            attrs={
                'style': 'width:50%',
            }
        )
    )

    class Meta:
        model = SliderSlide
        fields = '__all__'

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        if self.instance.pk and self.instance.product:
            self.initial['category'] = str(self.instance.product.category.pk)


class SliderSlideInline(ModelAdminInlineMixin, SortableStackedInline):
    fieldsets = (
        (None, {
            'fields': [
                'title', 'image', 'image_alt', 'price', 'category', 'product'
            ],
        }),
    )
    model = SliderSlide
    form = SliderSlideForm
    min = 1
    extra = 0
    sortable = 'sort_order'
    suit_classes = 'suit-tab suit-tab-general'


@admin.register(SliderBlock)
class SliderBlockAdmin(ModelAdminMixin, admin.ModelAdmin):
    fieldsets = (
        (_('Common'), {
            'classes': ('suit-tab', 'suit-tab-general'),
            'fields': ('label', 'visible'),
        }),
    )
    inlines = (SliderSlideInline, )
    list_display = ('label', 'visible', )
    suit_form_tabs = (
        ('general', _('General')),
    )


class SupplierInline(ModelAdminInlineMixin, SortableStackedInline):
    fieldsets = (
        (None, {
            'fields': [
                'title', 'logo', 'website',
            ],
        }),
    )
    model = Supplier
    min = 1
    extra = 0
    sortable = 'sort_order'
    suit_classes = 'suit-tab suit-tab-suppliers'


@admin.register(SuppliersBlock)
class SuppliersBlockAdmin(ModelAdminMixin, admin.ModelAdmin):
    fieldsets = (
        (_('Common'), {
            'classes': ('suit-tab', 'suit-tab-general'),
            'fields': ('label', 'visible'),
        }),
        (_('Private'), {
            'classes': ('suit-tab', 'suit-tab-general'),
            'fields': ('header', ),
        }),
    )
    inlines = (SupplierInline,)
    list_display = ('label', 'visible',)
    suit_form_tabs = (
        ('general', _('General')),
        ('suppliers', _('Suppliers')),
    )
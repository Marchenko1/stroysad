(function() {

    $(document).ready(function () {
        var $slider_blocks = $('#mainslider_block').find('.slider');
        $slider_blocks.each(function() {
            Slider(this, {
                adaptiveHeight: false
            }).attachPlugins([
                SliderFadeAnimation({
                    speed: 800
                }),
                SliderSideAnimation({
                    speed: 800
                }),
                SliderSideShortestAnimation({
                    speed: 800
                }),
                SliderControlsPlugin({
                    animationName: 'side-shortest'
                }),
                SliderNavigationPlugin({
                    animationName: 'side',
                    container: '.slider-list-wrapper'
                }),
                SliderDragPlugin({
                    speed: 800
                }),
                SliderAutoscrollPlugin({
                    animationName: 'fade',
                    interval: 5000
                })
            ]);
        });

        $.bgInspector.inspect($slider_blocks.find('img'));
    });

})(jQuery);
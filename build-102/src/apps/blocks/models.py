from django.db import models
from django.utils.translation import ugettext_lazy as _
from attachable_blocks import AttachableBlock
from shop.models import ShopProduct
from libs.valute_field import ValuteField
from libs.stdimage.fields import StdImageField
from libs.storages.media_storage import MediaStorage


class SliderBlock(AttachableBlock):
    """ Блок слайдера картинок на главной """
    BLOCK_VIEW = 'blocks.views.mainslider_render'

    class Meta:
        verbose_name = _('Slider block')
        verbose_name_plural = _('Slider blocks')

    def __str__(self):
        return _('Slider #%s') % self.pk


class SliderSlide(models.Model):
    block = models.ForeignKey(SliderBlock, verbose_name=_('block'), related_name='slides')
    image = StdImageField(_('image'),
        storage=MediaStorage('blocks/mainslider'),
        min_dimensions=(1220, 400),
        admin_variation='admin',
        crop_area=True,
        aspects=('desktop',),
        variations=dict(
            desktop=dict(
                size=(1220, 400),
            ),
            tablet=dict(
                size=(768, 400),
            ),
            mobile=dict(
                size=(360, 400),
            ),
            admin=dict(
                size=(320, 116),
            ),
        ),
    )
    image_alt = models.CharField(_('image alt'), max_length=255, blank=True)
    title = models.CharField(_('title'), max_length=128)
    price = ValuteField(_('price'))
    product = models.ForeignKey(ShopProduct, null=True, blank=True, verbose_name=_('product'))
    sort_order = models.PositiveIntegerField(_('sort order'))

    class Meta:
        verbose_name = _('slide')
        verbose_name_plural = _('slides')
        ordering = ('sort_order',)

    def __str__(self):
        return _('Slide #%s') % self.pk


class SuppliersBlock(AttachableBlock):
    """ Блок поставщиков на главной """
    BLOCK_VIEW = 'blocks.views.suppliers_render'

    header = models.CharField(_('header'), max_length=128)

    class Meta:
        verbose_name = _('Suppliers block')
        verbose_name_plural = _('Suppliers blocks')

    def __str__(self):
        return self.header


class Supplier(models.Model):
    block = models.ForeignKey(SuppliersBlock, verbose_name=_('block'), related_name='suppliers')
    title = models.CharField(_('title'), max_length=128)
    logo = StdImageField(_('logo'),
        storage=MediaStorage('blocks/suppliers'),
        min_dimensions=(80, 40),
        admin_variation='admin',
        crop_area=True,
        aspects=('admin',),
        variations=dict(
            normal=dict(
                size=(0, 100),
                crop=False,
                stretch=False,
                max_width=200,
            ),
            admin=dict(
                size=(200, 100),
                crop=False,
                stretch=False
            ),
        ),
    )
    website = models.URLField(_('website'), blank=True)
    sort_order = models.PositiveIntegerField(_('sort order'))

    class Meta:
        verbose_name = _('Supplier')
        verbose_name_plural = _('Suppliers')
        ordering = ('sort_order', )

    def __str__(self):
        return _('Supplier #%s') % self.pk

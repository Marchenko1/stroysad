from django.template import Library, loader
from libs.cache import cached

register = Library()


@cached()
def mainslider_render(context, block, **kwargs):
    return loader.render_to_string('blocks/mainslider.html', {
        'block': block,
    }, request=context.get('request'))


@cached()
def suppliers_render(context, block, **kwargs):
    return loader.render_to_string('blocks/suppliers.html', {
        'block': block,
    }, request=context.get('request'))

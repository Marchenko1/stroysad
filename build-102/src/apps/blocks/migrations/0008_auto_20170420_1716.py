# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models
import libs.storages.media_storage
import libs.stdimage.fields


class Migration(migrations.Migration):

    dependencies = [
        ('blocks', '0007_auto_20170420_1339'),
    ]

    operations = [
        migrations.AlterField(
            model_name='sliderslide',
            name='image',
            field=libs.stdimage.fields.StdImageField(variations={'admin': {'size': (320, 116), 'stretch': True}, 'tablet': {'size': (768, 400), 'stretch': True}, 'desktop': {'size': (1220, 400), 'stretch': True}, 'mobile': {'size': (360, 400), 'stretch': True}}, aspects=('desktop',), storage=libs.storages.media_storage.MediaStorage('blocks/mainslider'), verbose_name='image', upload_to='', min_dimensions=(1200, 400)),
        ),
    ]

# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models
import libs.storages.media_storage
import libs.stdimage.fields


class Migration(migrations.Migration):

    dependencies = [
        ('blocks', '0003_auto_20170419_1503'),
    ]

    operations = [
        migrations.AlterField(
            model_name='sliderslide',
            name='image',
            field=libs.stdimage.fields.StdImageField(variations={'admin': {'size': (320, 116)}, 'tablet': {'stretch': True, 'size': (1600, 600)}, 'mobile': {'stretch': True, 'size': (768, 400)}, 'desktop': {'stretch': True, 'size': (1920, 400)}}, verbose_name='image', min_dimensions=(1920, 400), upload_to='', storage=libs.storages.media_storage.MediaStorage('blocks/mainslider'), aspects=('desktop',)),
        ),
    ]

# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models
import libs.storages.media_storage
import libs.stdimage.fields


class Migration(migrations.Migration):

    dependencies = [
        ('blocks', '0010_auto_20170421_0922'),
    ]

    operations = [
        migrations.AlterField(
            model_name='sliderslide',
            name='image',
            field=libs.stdimage.fields.StdImageField(variations={'mobile': {'size': (360, 400)}, 'desktop': {'size': (1220, 400)}, 'admin': {'size': (320, 116)}, 'tablet': {'size': (768, 400)}}, storage=libs.storages.media_storage.MediaStorage('blocks/mainslider'), min_dimensions=(1220, 400), upload_to='', verbose_name='image', aspects=('desktop',)),
        ),
    ]

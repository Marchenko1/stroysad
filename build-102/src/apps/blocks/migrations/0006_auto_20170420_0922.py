# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models
import libs.stdimage.fields
import libs.storages.media_storage


class Migration(migrations.Migration):

    dependencies = [
        ('blocks', '0005_auto_20170420_0913'),
    ]

    operations = [
        migrations.AlterField(
            model_name='sliderslide',
            name='image',
            field=libs.stdimage.fields.StdImageField(min_dimensions=(1920, 400), storage=libs.storages.media_storage.MediaStorage('blocks/mainslider'), aspects=('desktop',), upload_to='', variations={'admin': {'stretch': True, 'size': (320, 116)}, 'mobile': {'stretch': True, 'size': (768, 400)}, 'desktop': {'stretch': True, 'size': (1920, 400)}, 'tablet': {'stretch': True, 'size': (1600, 700)}}, verbose_name='image'),
        ),
    ]

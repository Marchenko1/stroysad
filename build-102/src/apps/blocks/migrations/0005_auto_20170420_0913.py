# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models
import libs.stdimage.fields
import libs.storages.media_storage


class Migration(migrations.Migration):

    dependencies = [
        ('blocks', '0004_auto_20170419_1734'),
    ]

    operations = [
        migrations.AlterField(
            model_name='sliderslide',
            name='image',
            field=libs.stdimage.fields.StdImageField(variations={'mobile': {'size': (768, 400), 'stretch': True}, 'admin': {'size': (320, 116)}, 'desktop': {'size': (1920, 400), 'stretch': True}, 'tablet': {'size': (1600, 700), 'stretch': True}}, upload_to='', min_dimensions=(1920, 400), storage=libs.storages.media_storage.MediaStorage('blocks/mainslider'), verbose_name='image', aspects=('desktop',)),
        ),
    ]

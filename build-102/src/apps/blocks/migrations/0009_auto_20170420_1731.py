# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models
import libs.storages.media_storage
import libs.stdimage.fields


class Migration(migrations.Migration):

    dependencies = [
        ('blocks', '0008_auto_20170420_1716'),
    ]

    operations = [
        migrations.AlterField(
            model_name='sliderslide',
            name='image',
            field=libs.stdimage.fields.StdImageField(upload_to='', variations={'tablet': {'size': (768, 400)}, 'desktop': {'size': (1220, 400)}, 'admin': {'size': (320, 116)}, 'mobile': {'size': (360, 400)}}, verbose_name='image', min_dimensions=(1220, 400), aspects=('desktop',), storage=libs.storages.media_storage.MediaStorage('blocks/mainslider')),
        ),
    ]

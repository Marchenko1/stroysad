# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models
import libs.valute_field.fields
import libs.storages
import libs.stdimage.fields


class Migration(migrations.Migration):

    dependencies = [
        ('attachable_blocks', '0001_initial'),
        ('shop', '__first__'),
    ]

    operations = [
        migrations.CreateModel(
            name='SliderBlock',
            fields=[
                ('attachableblock_ptr', models.OneToOneField(parent_link=True, auto_created=True, serialize=False, to='attachable_blocks.AttachableBlock', primary_key=True)),
            ],
            options={
                'verbose_name': 'Slider block',
                'verbose_name_plural': 'Slider blocks',
            },
            bases=('attachable_blocks.attachableblock',),
        ),
        migrations.CreateModel(
            name='SliderSlide',
            fields=[
                ('id', models.AutoField(primary_key=True, auto_created=True, verbose_name='ID', serialize=False)),
                ('image', libs.stdimage.fields.StdImageField(storage=libs.storages.MediaStorage('blocks/mainslider'), verbose_name='image', aspects=('desktop',), upload_to='', variations={'admin': {'size': (320, 116)}, 'desktop': {'stretch': True, 'size': (1920, 700)}, 'tablet': {'size': (1024, 373)}, 'mobile': {'size': (768, 400)}}, min_dimensions=(1400, 510))),
                ('title', models.CharField(max_length=128, verbose_name='title')),
                ('price', libs.valute_field.fields.ValuteField(verbose_name='price')),
                ('sort_order', models.PositiveIntegerField(verbose_name='sort order')),
                ('block', models.ForeignKey(verbose_name='block', to='blocks.SliderBlock', related_name='slides')),
                ('product', models.ForeignKey(blank=True, verbose_name='product', null=True, to='shop.ShopProduct')),
            ],
            options={
                'ordering': ('sort_order',),
                'verbose_name': 'slide',
                'verbose_name_plural': 'slides',
            },
        ),
        migrations.CreateModel(
            name='Supplier',
            fields=[
                ('id', models.AutoField(primary_key=True, auto_created=True, verbose_name='ID', serialize=False)),
                ('title', models.CharField(max_length=128, verbose_name='title')),
                ('logo', libs.stdimage.fields.StdImageField(storage=libs.storages.MediaStorage('blocks/suppliers'), verbose_name='logo', aspects=('admin',), upload_to='', variations={'admin': {'stretch': False, 'crop': False, 'size': (200, 100)}, 'normal': {'stretch': False, 'crop': False, 'max_width': 200, 'size': (0, 100)}}, min_dimensions=(80, 40))),
                ('website', models.URLField(blank=True, verbose_name='website')),
                ('sort_order', models.PositiveIntegerField(verbose_name='sort order')),
            ],
            options={
                'ordering': ('sort_order',),
                'verbose_name': 'Supplier',
                'verbose_name_plural': 'Suppliers',
            },
        ),
        migrations.CreateModel(
            name='SuppliersBlock',
            fields=[
                ('attachableblock_ptr', models.OneToOneField(parent_link=True, auto_created=True, serialize=False, to='attachable_blocks.AttachableBlock', primary_key=True)),
                ('header', models.CharField(max_length=128, verbose_name='header')),
            ],
            options={
                'verbose_name': 'Suppliers block',
                'verbose_name_plural': 'Suppliers blocks',
            },
            bases=('attachable_blocks.attachableblock',),
        ),
        migrations.AddField(
            model_name='supplier',
            name='block',
            field=models.ForeignKey(verbose_name='block', to='blocks.SuppliersBlock', related_name='suppliers'),
        ),
    ]

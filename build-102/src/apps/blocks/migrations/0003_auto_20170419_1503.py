# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models
import libs.storages.media_storage
import libs.stdimage.fields


class Migration(migrations.Migration):

    dependencies = [
        ('blocks', '0002_sliderslide_image_alt'),
    ]

    operations = [
        migrations.AlterField(
            model_name='sliderslide',
            name='image',
            field=libs.stdimage.fields.StdImageField(upload_to='', variations={'admin': {'size': (320, 116)}, 'desktop': {'stretch': True, 'size': (1920, 400)}, 'mobile': {'size': (768, 400)}, 'tablet': {'size': (1024, 373)}}, aspects=('desktop',), storage=libs.storages.media_storage.MediaStorage('blocks/mainslider'), verbose_name='image', min_dimensions=(1400, 400)),
        ),
    ]

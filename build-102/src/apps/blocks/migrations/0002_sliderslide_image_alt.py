# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('blocks', '0001_initial'),
    ]

    operations = [
        migrations.AddField(
            model_name='sliderslide',
            name='image_alt',
            field=models.CharField(verbose_name='image alt', blank=True, max_length=255),
        ),
    ]

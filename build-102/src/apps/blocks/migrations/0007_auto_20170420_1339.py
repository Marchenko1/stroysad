# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models
import libs.stdimage.fields
import libs.storages.media_storage


class Migration(migrations.Migration):

    dependencies = [
        ('blocks', '0006_auto_20170420_0922'),
    ]

    operations = [
        migrations.AlterField(
            model_name='sliderslide',
            name='image',
            field=libs.stdimage.fields.StdImageField(variations={'tablet': {'stretch': True, 'size': (768, 400)}, 'admin': {'stretch': True, 'size': (320, 116)}, 'desktop': {'stretch': True, 'size': (1220, 230)}, 'mobile': {'stretch': True, 'size': (360, 400)}}, verbose_name='image', storage=libs.storages.media_storage.MediaStorage('blocks/mainslider'), upload_to='', min_dimensions=(1200, 400), aspects=('desktop',)),
        ),
    ]

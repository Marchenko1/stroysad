from ._pipeline import PIPELINE

PIPELINE['STYLESHEETS'].update({
    'critical': {
        'source_filenames': (
            'scss/grid.scss',
            'scss/layout.scss',
            'scss/forms.scss',
            'scss/text_styles.scss',

            'shop/scss/header_cart.scss',
            'menu/scss/main_menu.scss',
            'header/scss/header.scss',
            'shop/scss/header.scss',
            'shop/scss/products.scss',
        ),
        'output_filename': 'css_build/critical.css',
    },
    'core': {
        'source_filenames': (
            'css/jquery-ui/jquery-ui.css',
            'css/jquery-ui/jquery-ui.theme.css',

            'scss/custom_checkbox.scss',
            'scss/custom_radiobox.scss',
            'scss/custom_counter.scss',
            'scss/parallax.scss',
            'scss/popups/popups.scss',
            'scss/popups/preloader.scss',
            'scss/slider/slider.scss',
            'scss/slider/plugins/controls.scss',
            'scss/slider/plugins/navigation.scss',

            'blocks/scss/suppliers.scss',
            'contacts/scss/contacts.scss',
            'footer/scss/footer.scss',
            'paginator/scss/paginator.scss',
            'rating/scss/rating.scss',
            'seo/scss/block.scss',
            'users/scss/users.scss',
            'testimonials/scss/block.scss',
        ),
        'output_filename': 'css/core.css',
    },
    'fonts': {
        'source_filenames': (
            'fonts/Lato_Regular/stylesheet.css',
            'fonts/Lato_Light/stylesheet.css',
            'fonts/Lato_Bold/stylesheet.css',
        ),
        'output_filename': 'css_build/fonts.css',
        'template_name': 'pipeline/localstorage_css.html',
    },
    'main_critical': {
        'source_filenames': (
            'blocks/scss/mainslider.scss',
        ),
        'output_filename': 'css_build/main_critical.css',
    },
    'main_page': {
        'source_filenames': (
            'shop/scss/category_products.scss',
            'main/scss/index.scss',
        ),
        'output_filename': 'css/main_page.css',
    },
    'blog': {
        'source_filenames': (
            'blog/scss/index.scss',
        ),
        'output_filename': 'css_build/blog.css',
    },
    'blog_detail': {
        'source_filenames': (
            'blog/scss/detail.scss',
        ),
        'output_filename': 'css_build/blog_detail.css',
    },
    'shop_category_critical': {
        'source_filenames': (
            'breadcrumbs/scss/breadcrumbs.scss',
            'shop/scss/category_critical.scss',
        ),
        'output_filename': 'css/shop_category_critical.css',
    },
    'shop_category': {
        'source_filenames': (
            'shop/scss/category.scss',
        ),
        'output_filename': 'css/shop_category.css',
    },
    'shop_detail_critical': {
        'source_filenames': (
            'breadcrumbs/scss/breadcrumbs.scss',
            'shop/scss/detail_critical.scss',
        ),
        'output_filename': 'css/shop_detail_critical.css',
    },
    'shop_detail': {
        'source_filenames': (
            'shop/scss/category_products.scss',
            'shop/scss/detail.scss',
        ),
        'output_filename': 'css/shop_detail.css',
    },
    'shop_order_cart': {
        'source_filenames': (
            'breadcrumbs/scss/breadcrumbs.scss',
            'shop/scss/order_cart.scss',
        ),
        'output_filename': 'css/shop_order_cart.css',
    },
    'dealers_page': {
        'source_filenames': (
            'dealers/scss/index.scss',
        ),
        'output_filename': 'css/dealers_page.css',
    },
    'delivery_page': {
        'source_filenames': (
            'delivery/scss/index.scss',
        ),
        'output_filename': 'css/delivery_page.css',
    },
    'training_page': {
        'source_filenames': (
            'training/scss/index.scss',
        ),
        'output_filename': 'css/training_page.css',
    },
    'design_page': {
        'source_filenames': (
            'design/scss/index.scss',
        ),
        'output_filename': 'css/design_page.css',
    },
    'about_page': {
        'source_filenames': (
            'about/scss/index.scss',
        ),
        'output_filename': 'css/about_page.css',
    },
    'contacts_page': {
        'source_filenames': (
            'google_maps/scss/balloon.scss',
            'contacts/scss/index.scss',
        ),
        'output_filename': 'css/contacts_page.css',
    },
    'policy': {
        'source_filenames': (
            'policy/scss/index.scss',
        ),
        'output_filename': 'css/policy.css',
    },
    'agreement': {
        'source_filenames': (
            'agreement/scss/index.scss',
        ),
        'output_filename': 'css/agreement.css',
    },
    'search': {
        'source_filenames': (
            'search/scss/result.scss',
        ),
        'output_filename': 'css/search.css',
    },
    'error_page': {
        'source_filenames': (
            'scss/error_page.scss',
        ),
        'output_filename': 'css/error_page.css',
    },
})

PIPELINE['JAVASCRIPT'].update({
    'core': {
        'source_filenames': (
            'polyfills/modernizr.js',
            'polyfills/loader.js',

            'js/jquery-2.2.1.min.js',
            'js/jquery-ui.min.js',

            'common/js/jquery.cookie.js',
            'common/js/jquery.utils.js',
            'common/js/jquery.ajax_csrf.js',

            'js/jquery.inspectors.js',
            'js/datepicker-ru.js',
            'js/jquery.fitvids.js',
            'js/jquery.form.js',
            'js/jquery.scrollTo.js',
            'js/custom_checkbox.js',
            'js/custom_radiobox.js',
            'js/custom_counter.js',
            'js/drager.js',
            'js/parallax.js',
            'js/agreement.js',
            'js/sticky.js',
            'js/text_styles.js',
            'js/jquery.maskedinput.min.js',

            'js/popups/jquery.popups.js',
            'js/popups/preloader.js',

            'js/slider/slider.js',
            'js/slider/plugins/side_animation.js',
            'js/slider/plugins/fade_animation.js',
            'js/slider/plugins/autoscroll.js',
            'js/slider/plugins/navigation.js',
            'js/slider/plugins/controls.js',
            'js/slider/plugins/drag.js',

            'attachable_blocks/js/async_blocks.js',
            'blocks/js/mainslider.js',
            'contacts/js/contacts.js',
            'rating/js/rating.js',
            'menu/js/main_menu.js',
            'shop/js/cart.js',
            'shop/js/buttons.js',
            'users/js/users.js',
            'ajaxcache/js/ajaxcache.js',
        ),
        'output_filename': 'js/core.js',
    },
    'main_page': {
        'source_filenames': (
            'main/js/index.js',
        ),
        'output_filename': 'js/main_page.js',
    },
    'shop_category': {
        'source_filenames': (
            'shop/js/category.js',
        ),
        'output_filename': 'js/shop_category.js',
    },
    'shop_detail': {
        'source_filenames': (
            'shop/js/detail.js',
        ),
        'output_filename': 'js/shop_detail.js',
    },
    'shop_order_cart': {
        'source_filenames': (
            'shop/js/order_cart.js',
        ),
        'output_filename': 'js/shop_order_cart.js',
    },
    'dealers_page': {
        'source_filenames': (
            'dealers/js/index.js',
        ),
        'output_filename': 'js/dealers_page.js',
    },
    'delivery_page': {
        'source_filenames': (
            'delivery/js/index.js',
        ),
        'output_filename': 'js/delivery_page.js',
    },
    'training_page': {
        'source_filenames': (
            'training/js/index.js',
        ),
        'output_filename': 'js/training_page.js',
    },
    'design_page': {
        'source_filenames': (
            'design/js/index.js',
        ),
        'output_filename': 'js/design_page.js',
    },
    'about_page': {
        'source_filenames': (
            'about/js/index.js',
        ),
        'output_filename': 'js/about_page.js',
    },
    'contacts_page': {
        'source_filenames': (
            'google_maps/js/core.js',
            'google_maps/js/balloon.js',
            'contacts/js/index.js',
        ),
        'output_filename': 'js/contacts_page.js',
    },
})

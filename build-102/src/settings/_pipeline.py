PIPELINE = {
    'PIPELINE_ENABLED': True,
    'COMPILERS': (
        'libs.pipeline.sassc.SASSCCompiler',
    ),
    'SASS_ARGUMENTS': '-t compressed',
    'CSS_COMPRESSOR': 'libs.pipeline.cssmin.CSSCompressor',
    'JS_COMPRESSOR': 'pipeline.compressors.jsmin.JSMinCompressor',

    'STYLESHEETS': {
        'admin_customize': {
            'source_filenames': (
                'admin/css/jquery-ui/jquery-ui.min.css',
                'admin/scss/admin_fixes.scss',
                'admin/scss/admin_table.scss',
                'admin/scss/button_filter.scss',
                'admin/scss/hierarchy_filter.scss',
                'admin/scss/dl_core.scss',
                'admin/scss/dl_login.scss',
            ),
            'output_filename': 'admin/css/customize.css',
        },
    },

    'JAVASCRIPT': {
        'admin_customize': {
            'source_filenames': (
                'admin/js/jquery-ui.min.js',
                'common/js/jquery.cookie.js',
                'common/js/jquery.ajax_csrf.js',
                'common/js/jquery.mousewheel.js',
                'common/js/jquery.utils.js',
                'common/js/file_dropper.js',
                'admin/js/button_filter.js',
            ),
            'output_filename': 'admin/js/customize.js',
        },
    }
}
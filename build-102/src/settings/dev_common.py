from settings.common import *

VZ_DIRECTORY = '/home/webapp/domains/shop-dvor.ru'
# настройки статики
STATIC_ROOT = os.path.join(VZ_DIRECTORY, 'static')
MEDIA_ROOT = os.path.join(VZ_DIRECTORY, 'media')
BACKUP_ROOT = os.path.join(VZ_DIRECTORY, 'backup')
PUBLIC_DIR = os.path.join(VZ_DIRECTORY, 'public')

DOMAIN = '.shop-dvor.ru'
SESSION_COOKIE_DOMAIN = DOMAIN
CSRF_COOKIE_DOMAIN = DOMAIN
ALLOWED_HOSTS = (
    DOMAIN,
    'localhost',
    '127.0.0.1',
)

DEBUG = True

DATABASES.update({
    'default': {
        'ENGINE':       'django.db.backends.postgresql_psycopg2',
        'NAME':         'shop-dvor.ru',
        'USER':         'webapp',
        'PASSWORD':     'MJJjuNrUtguE4gex',
        'HOST':         'localhost',
        'CONN_MAX_AGE': 60,
        'TEST':         {
            'MIRROR': 'default',
        },
    }
})

# Отключение компрессии SASS (иначе теряется наглядность кода)
PIPELINE['SASS_ARGUMENTS'] = '-t nested'
# Pipeline
SASS_INCLUDE_DIR = os.path.join(BASE_DIR, 'static', 'scss')
PIPELINE['SASS_BINARY'] = '"C:/Program Files/libsass/sassc.exe" --load-path ' + SASS_INCLUDE_DIR

STATICFILES_FINDERS += (
    'libs.pipeline.debug_finder.PipelineFinder',
)


# # Media
# MEDIA_ROOT = os.path.abspath(os.path.join(BASE_DIR, 'media'))
# MEDIA_URL = '/media/'
#
# # Static files (CSS, JavaScript, Images)
# STATIC_ROOT = os.path.join(os.path.dirname(BASE_DIR), 'static')
# STATIC_URL = '/static/'

# STATICFILES_DIRS = (
#     os.path.join(BASE_DIR, "static"),
# )

# print(STATIC_ROOT)
# print(STATICFILES_DIRS)

# Отключение кэширования шаблонов
TEMPLATES[0]['OPTIONS']['loaders'] = (
    'django.template.loaders.filesystem.Loader',
    'django.template.loaders.app_directories.Loader',
)

INSTALLED_APPS = (
     # 'devserver',
     'api',
     'rest_framework',
 ) + INSTALLED_APPS

MIDDLEWARE_CLASSES = MIDDLEWARE_CLASSES + (
    'devserver.middleware.DevServerMiddleware',
)

# Вывод ошибок в консоль
LOGGING = {
    'version':                  1,
    'disable_existing_loggers': True,
    'handlers':                 {
        'null':    {
            'level': 'DEBUG',
            'class': 'logging.NullHandler',
        },
        'console': {
            'level': 'DEBUG',
            'class': 'logging.StreamHandler',
        },
    },
    'loggers':                  {
        'django':         {
            'handlers': ['null'],
            'level':    'INFO',
        },
        'django.request': {
            'handlers': ['console'],
            'level':    'ERROR',
        },
        'requests':       {
            'handlers':  ['null'],
            'propagate': False,
        },
        'sql':            {
            'handlers':  ['null'],
            'propagate': False,
            'level':     'DEBUG',
        },
        '':               {
            'handlers': ['console'],
            'level':    'INFO',
        }
    },
}

import os
import re
import sys
from django.utils.translation import ugettext_lazy as _
from .pipeline import PIPELINE

BASE_DIR = os.path.dirname(os.path.dirname(__file__))
sys.path.insert(0, os.path.join(BASE_DIR, 'apps'))
sys.path.insert(0, os.path.join(BASE_DIR, 'apps_common'))

SECRET_KEY = 'b&)w#20-y+c8b@bh4!57ene6^jj75ll_*%m9=d=+=watd+mhn='

DEBUG = True

LANGUAGE_CODE = 'ru'
LANGUAGES = (
    ('ru', _('Russian')),
    ('en', _('English')),
)

TIME_ZONE = 'Europe/Samara'
TIME_FORMAT = 'H:i'
DATE_FORMAT = 'j E Y'
USE_I18N = True
USE_L10N = True
USE_TZ = True

INSTALLED_APPS = (
    'suit',
    'django.contrib.admin',
    'django.contrib.auth',
    'django.contrib.contenttypes',
    'django.contrib.sessions',
    'django.contrib.messages',
    'django.contrib.sitemaps',
    'django.contrib.staticfiles',
    'django.contrib.humanize',
    'django.contrib.sites',

    'pipeline',
    'mptt',
    'solo',
    'suit_ckeditor',

    # Apps
    'about',
    'address',
    'agreement',
    'blocks',
    'blog',
    'config',
    'contacts',
    'dealers',
    'delivery',
    'design',
    'main',
    'policy',
    'recipient',
    'shop',
    'search',
    'testimonials',
    'training',
    'users',
    'yandex',

    # Apps common
    'admin_ctr',
    'admin_log',
    'admin_honeypot',
    'attachable_blocks',
    'backups',
    'boxberry',
    'breadcrumbs',
    'ckeditor',
    'footer',
    'gallery',
    'google_maps',
    'header',
    'menu',
    'paginator',
    'rating',
    'robokassa',
    'seo',
    'social_networks',
    'files',

    # Libs
    'libs.autocomplete',
    'libs.away',
    'libs.js_storage',
    'libs.form_helper',
    'libs.geocity',
    'libs.management',
    'libs.pipeline',
    'libs.sphinx',
    'libs.sprite_image',
    'libs.stdimage',
    'libs.templatetags',
    'libs.variation_field',
    'libs.ajaxcache',
)

# Suit
SUIT_CONFIG = {
    # header
    'ADMIN_NAME': 'СтройСад',
    'HEADER_DATE_FORMAT': 'l, j F Y',

    # search
    'SEARCH_URL': '',

    # menu
    'MENU': (
        {
            'app': 'main',
            'icon': 'icon-file',
            'models': (
                'mainpageconfig',
            )
        },
        {
            'app': 'shop',
            'icon': 'icon-shopping-cart',
            'models': (
                'shoporder',
                'shopproduct',
                'shopcategory',
                'discount',
                'discountconfig',
                'shopproductproducer',
                'shopwarehouse',
                'shopconfig',
            )
        },
        {
            'app': 'dealers',
            'icon': 'icon-file',
            'models': (
                'dealersconfig',
            )
        },
        {
            'app': 'delivery',
            'icon': 'icon-file',
            'models': (
                'deliveryconfig',
            )
        },
        {
            'app': 'training',
            'icon': 'icon-file',
            'models': (
                'trainingconfig',
            )
        },
        {
            'app': 'design',
            'icon': 'icon-file',
            'models': (
                'designconfig',
            )
        },
        {
            'app': 'about',
            'icon': 'icon-file',
            'models': (
                'aboutconfig',
            )
        },
        {
            'app': 'contacts',
            'icon': 'icon-file',
            'models': (
                'address',
                'contactsconfig',
            )
        },
        {
            'app': 'blog',
            'icon': 'icon-file',
            'models': (
                'BlogPost',
                'Tag',
                'BlogConfig',
            )
        },
        '-',
        {
            'app': 'policy',
            'icon': 'icon-file',
            'models': (
                'policyconfig',
            )
        },
        {
            'app': 'agreement',
            'icon': 'icon-file',
            'models': (
                'agreementconfig',
            )
        },
        {
            'app': 'testimonials',
            'icon': 'icon-file',
            'models': (
                'testimonials',
                'testimonialsconfig',
            )
        },
        {
            'app': 'config',
            'icon': 'icon-wrench',
        },
        {
            'app': 'social_networks',
            'icon': 'icon-bullhorn',
            'models': (
                'SocialLinks',
                'FeedPost',
            ),
        },
        '-',
        'admin',
        {
            'icon': 'icon-lock',
            'label': 'Authentication and Authorization',
            'permissions': 'users.change_customuser',
            'models': (
                'auth.group',
                'users.customuser',
            )
        },
        {
            'app': 'backups',
            'icon': 'icon-hdd',
        },
        {
            'app': 'robokassa',
            'icon': 'icon-shopping-cart',
            'models': (
                'log',
            )
        },
        'sites',
        {
            'app': 'seo',
            'icon': 'icon-tasks',
            'models': (
                'seoconfig',
                'redirect',
                'counter',
                'robots',
            ),
        },
        '-',
        '-',
    ),
}

# Pipeline
SASS_INCLUDE_DIR = BASE_DIR + '/static/scss/'
PIPELINE['SASS_BINARY'] = '/usr/bin/sassc --load-path ' + SASS_INCLUDE_DIR

MIDDLEWARE_CLASSES = (
    'pipeline.middleware.MinifyHTMLMiddleware',
    'django.contrib.sessions.middleware.SessionMiddleware',
    'django.middleware.common.CommonMiddleware',
    'django.middleware.csrf.CsrfViewMiddleware',
    'django.contrib.auth.middleware.AuthenticationMiddleware',
    'django.contrib.messages.middleware.MessageMiddleware',

    'shop.middleware.CartMiddleware',
    'libs.js_storage.middleware.JSStorageMiddleware',
    'libs.cache.middleware.SCCMiddleware',
    'libs.middleware.xss.XSSProtectionMiddleware',
    'libs.middleware.utm.UTMMiddleware',
    'breadcrumbs.middleware.BreadcrumbsMiddleware',
    'menu.middleware.MenuMiddleware',
    'seo.middleware.RedirectMiddleware',
)

ALLOWED_HOSTS = ()

ROOT_URLCONF = 'project.urls'

WSGI_APPLICATION = 'project.wsgi.application'

# Sites and users
SITE_ID = 1
ANONYMOUS_USER_ID = -1
AUTH_USER_MODEL = 'users.CustomUser'
AUTHENTICATION_BACKENDS = (
    'django.contrib.auth.backends.ModelBackend',
)

LOGIN_URL = 'users:login'
LOGIN_REDIRECT_URL = 'index'
RESET_PASSWORD_REDIRECT_URL = 'index'
LOGOUT_URL = 'index'

# Email
EMAIL_HOST = 'smtp.yandex.ru'
EMAIL_PORT = 25
EMAIL_HOST_USER = 'noreply@shop-dvor.ru'
EMAIL_HOST_PASSWORD = '21081989'
EMAIL_USE_TLS = True
DEFAULT_FROM_EMAIL = SERVER_EMAIL = 'noreply@shop-dvor.ru'
EMAIL_SUBJECT_PREFIX = '[%s] ' % (SUIT_CONFIG['ADMIN_NAME'],)

# ==================================================================
# ==================== APPS SETTINGS ===============================
# ==================================================================

# Admin Dump
BACKUP_ROOT = os.path.abspath(os.path.join(BASE_DIR, '..', 'backup'))

# Директория для robots.txt и других открытых файлов
PUBLIC_DIR = os.path.abspath(os.path.join(BASE_DIR, '..', 'public'))

# Google Map API
GOOGLE_APIKEY = 'AIzaSyDAx6URKWh7OAwJqKoiIkBq47mpZ0BGgR0'

# Youtube Data API
# для ckeditor, videolink_field, youtube
YOUTUBE_APIKEY = 'AIzaSyDAx6URKWh7OAwJqKoiIkBq47mpZ0BGgR0'

# Smart Cache-Control
SCC_DISABLED_URLS = [
    r'/admin/',
    r'/dladmin/',
    r'/order/cart/',
]

# Формат валют (RUB / USD / EUR / GBP)
# Для включения зависимости от языка сайта - задать None или удалить
VALUTE_FORMAT = None

# Автопостинг
TWITTER_APP_ID = 'usBDiKFC2nLKzqxbTnzo4n0zo'
TWITTER_SECRET = 'mEDp9YtzfwSktppj0V8jfuSj5A6BqsytIly1UiHEwaBJcxrr27'
TWITTER_TOKEN = '771354664419860480-Q4gvXq4ya4NumHqMA1yRmBqh0RJzP9e'
TWITTER_TOKEN_SECRET = 'QtP8Y3EK7EXMbAtfQc3i2pjeTQUv8vVdqaTbDQeU2pIGk'

FACEBOOK_TOKEN = 'EAAZA6jzmKGisBAEG7zv2bhGGmtZBw9vh2MmPWAoLppe0MBZCFfZC' \
                 'GbT1toddMZAHfegM0jNMPSOOSdeHf6AR80lMz4ZB0GQ5DwxwsXqGF' \
                 'nDlUXQAtAh54LIW3xtJY1Q7k5x06w5HT8sjmzuNS25c6QMnR22cNu' \
                 'wg4ZD'

# Robokassa
ROBOKASSA_LOGIN = 'Stroysad'
ROBOKASSA_PASSWORD1 = 'w3seux874pDZuGy3nCEe'
ROBOKASSA_PASSWORD2 = 'OF3DVfwMw1g1bIfM9q6T'
ROBOKASSA_SUCCESS_REDIRECT_URL = 'index'
ROBOKASSA_FAIL_REDIRECT_URL = 'index'
ROBOKASSA_TEST_MODE = False

# Sphinx
SPHINX_HOST = 'localhost'
SPHINX_PORT = 9312
SPHINX_SECRET = 'Q2PvrdkU2b'

# Boxberry
BOXBERRY_TEST = False
#BOXBERRY_KEY = 'UkfuVvL76/FNlHfh6s00uQ=='
BOXBERRY_KEY = '1$OYs2SH7fIVUE8xaGikqdHoV6nO34Wizm'
#BOXBERRY_API_TOKEN = '19052.rjpqdacf'
BOXBERRY_API_TOKEN = '36a39ee667aa022bcb736fdd21f49837'
BOXBERRY_API_TEST_TOKEN = '31705.rvpqcbfd'

# ==================================================================
# ==================== END APPS SETTINGS ===========================
# ==================================================================

# Домен для куки сессий (".example.com")
SESSION_COOKIE_DOMAIN = None
SESSION_ENGINE = 'django.contrib.sessions.backends.cached_db'
SESSION_CACHE_ALIAS = 'default'
SESSION_COOKIE_AGE = 30 * 24 * 3600

# Домен для куки CSRF (".example.com")
CSRF_COOKIE_DOMAIN = None

# Список скомпилированных регулярных выражений
# запретных юзер-агентов
DISALLOWED_USER_AGENTS = ()

# Получатели писем о ошибках при DEBUG = False
ADMINS = (
    ('pix', 'pix666@ya.ru'),
    ('n.koshelev', 'n.koshelev@directline.digital'),
    ('tester', 'tester.errors@ya.ru'),
)

# Получатели писем о битых ссылках при DEBUG=False
# Требуется подключить django.middleware.common.BrokenLinkEmailsMiddleware
MANAGERS = (
    ('pix', 'pix666@ya.ru'),
)

# Список скомпилированных регулярных выражений адресов страниц,
# сообщения о 404 на которых не должны отправляться на почту (MANAGERS)
IGNORABLE_404_URLS = (
    re.compile(r'^/apple-touch-icon.*\.png$'),
    re.compile(r'^/favicon\.ico$'),
    re.compile(r'^/robots\.txt$'),
)

# DB
DATABASES = {}

# Cache
CACHES = {
    'default': {
        "BACKEND": "django_redis.cache.RedisCache",
        "LOCATION": "127.0.0.1:6379:0",
        "KEY_PREFIX": LANGUAGE_CODE + SECRET_KEY,
        "OPTIONS": {
            "CLIENT_CLASS": 'django_redis.client.DefaultClient',
            "PASSWORD": "",
        }
    }
}

# Templates
TEMPLATES = [
    {
        'BACKEND': 'django.template.backends.django.DjangoTemplates',
        'DIRS': (
            os.path.join(BASE_DIR, 'templates'),
        ),
        'OPTIONS': {
            'context_processors': (
                'django.contrib.messages.context_processors.messages',
                'django.contrib.auth.context_processors.auth',
                'django.template.context_processors.i18n',
                'django.template.context_processors.request',
                'google_maps.context_processors.google_apikey',
            ),
            'loaders': (
                ('django.template.loaders.cached.Loader', (
                    'django.template.loaders.filesystem.Loader',
                    'django.template.loaders.app_directories.Loader',
                )),
            ),
        }
    },
]

# Locale
LOCALE_PATHS = (
    'locale',
)

# Media
MEDIA_ROOT = os.path.abspath(os.path.join(BASE_DIR, '..', 'media'))
MEDIA_URL = '/media/'

# Static files (CSS, JavaScript, Images)
STATIC_ROOT = os.path.abspath(os.path.join(BASE_DIR, '..', 'static'))
STATIC_URL = '/static/'
STATICFILES_DIRS = (
    BASE_DIR + "/static/",
)
STATICFILES_STORAGE = 'pipeline.storage.PipelineStorage'
STATICFILES_FINDERS = (
    'django.contrib.staticfiles.finders.FileSystemFinder',
    'django.contrib.staticfiles.finders.AppDirectoriesFinder',
)

# Ckeditor
CKEDITOR_CONFIG_MICRO = {
    'extraPlugins': 'textlen,enterfix,image_attrs',
    'toolbar': [
        {
            'name': 'basicstyles',
            'items': ['Bold', 'Italic', '-', 'RemoveFormat']
        },
        {
            'name': 'links',
            'items': ['Link', 'Unlink']
        },
        {
            'name': 'document',
            'items': ['Format', 'Source']
        },
    ]
}
CKEDITOR_CONFIG_MINI = {
    'extraPlugins': 'textlen,enterfix,image_attrs',
    'toolbar': [
        {
            'name': 'basicstyles',
            'items': ['Bold', 'Italic', '-', 'RemoveFormat']
        },
        {
            'name': 'links',
            'items': ['Link', 'Unlink']
        },
        {
            'name': 'document',
            'items': ['Source']
        },
    ]
}

SPRITE_ICONS = (
    ('timer', (-1, -135)),
    ('drops', (-53, -135)),
    ('gears', (-105, -135)),
    ('waves', (-157, -135)),
    ('carrot', (-209, -135)),
    ('lift', (-261, -135)),
    ('home', (-313, -135)),
    ('cards', (-365, -135)),
    ('blank', (-417, -135)),
    ('truck', (-469, -135)),
    ('cash', (-521, -135)),
    ('coins', (-573, -135)),
    ('operator', (-625, -135)),
    ('ruler', (-677, -135)),
    ('bulb', (-729, -135)),
    ('box', (-781, -135)),
)

YML_SHOP_NAME = 'ShopDvor'
YML_SHOP_COMPANY = 'ООО СТРОЙ САД'
YML_SHOP_URL = 'https://shop-dvor.ru'

REDIS_HOST = 'localhost'
REDIS_PORT = '6379'
BROKER_URL = 'redis://' + REDIS_HOST + ':' + REDIS_PORT + '/1'
BROKER_TRANSPORT_OPTIONS = {
    'visibility_timeout': 3600
}
CELERY_RESULT_BACKEND = 'redis://' + REDIS_HOST + ':' + REDIS_PORT + '/1'

API_ENTITIES = {}

from settings.common import *


DOMAIN = '.shop-dvor.ru'
VZ_DIRECTORY = '/home/webapp/domains/shop-dvor.ru'

SESSION_COOKIE_DOMAIN = DOMAIN
CSRF_COOKIE_DOMAIN = DOMAIN

# Метка %SECRET_KEY% при развёртывании заменяется на нужный секретный ключ
SECRET_KEY = 'b&)w#20-y+c8b@bh4!57ene6^jj75ll_*%m9=d=+=watd+mhn='

ALLOWED_HOSTS = (
    DOMAIN,
    '89.111.132.45'
)

DEBUG=True

# настройки статики
STATIC_ROOT = os.path.join(VZ_DIRECTORY, 'static')
MEDIA_ROOT = os.path.join(VZ_DIRECTORY, 'media')
BACKUP_ROOT = os.path.join(VZ_DIRECTORY, 'backup')
PUBLIC_DIR = os.path.join(VZ_DIRECTORY, 'public')

STATICFILES_DIRS = [
    os.path.join(BASE_DIR, 'static')
]

DATABASES.update({
    'default': {
        'ENGINE':       'django.db.backends.postgresql_psycopg2',
        'NAME':         'shop-dvor.ru',
        'USER':         'webapp',
        'PASSWORD':     'MJJjuNrUtguE4gex',
        'HOST':         'localhost',
        'CONN_MAX_AGE': 60,
        'TEST':         {
            'MIRROR': 'default',
        },
    }
})

LOGGING = {
    'version':                  1,
    'disable_existing_loggers': False,
    'formatters':               {
        'verbose': {
            'format': '%(levelname)s [%(asctime)s]: %(message)s'
        },
    },
    'handlers':                 {
        'null':        {
            'level': 'DEBUG',
            'class': 'logging.NullHandler',
        },
        'file':        {
            'level':     'INFO',
            'class':     'logging.FileHandler',
            'filename':  os.path.join(VZ_DIRECTORY, 'django_errors.log'),
            'formatter': 'verbose',
        },
        'mail_admins': {
            'level': 'ERROR',
            'class': 'django.utils.log.AdminEmailHandler'
        },
    },
    'loggers':                  {
        'django':         {
            'handlers':  ['null'],
            'propagate': True,
            'level':     'INFO',
        },
        'django.request': {
            'handlers':  ['mail_admins', 'file'],
            'propagate': False,
            'level':     'WARNING',
        },
        '':               {
            'handlers': ['file'],
            'level':    'ERROR',
        }
    },
}

